from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, AbstractBaseUser

class Customer(models.Model):
    id = models.IntegerField(primary_key=True)
    Customer_Identifier = models.CharField(max_length=10,unique=True)
    password = models.CharField(max_length=150)

    class Meta:
        db_table = 'SuperAdmin_customers'

    def __str__(self) -> str:
        return self.Customer_Identifier
    
    USERNAME_FIELD = 'Customer_Identifier'
    REQUIRED_FIELDS = ['password']

class Version(models.Model):
    version = models.CharField(max_length=50, unique = True)
    code = models.CharField(max_length = 3,  unique = True)
    sensors = models.CharField(max_length=200,default="")  
    # Json_File =  models.FileField(upload_to='files/jsonFiles', blank=False, validators=[validate_file_extension])
    configtabs = models.CharField(max_length=200,default="")  
    # Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", validators=[validate_file_extension])
    # ext_Json_File =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # ext_Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # alter_units_table =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    last_modified = models.DateTimeField(auto_now = True)
    def __str__(self):
        return u'{0}'.format(self.version)

    class Meta:
        db_table = 'SuperAdmin_version'

class gfr(models.Model):
    Code     = models.CharField(max_length=3)
    tracker = models.CharField(max_length=150, unique=True)
    Mfg = models.CharField(max_length=30)
    Device = models.CharField(max_length=15)
    SerialNo = models.CharField(max_length=15)
    version =  models.ForeignKey(Version, on_delete=models.CASCADE)
    SAMVer = models.CharField(max_length=150)
    SWver = models.CharField(max_length=150, default="")
    hardware_Id = models.CharField(max_length=150, unique=True)
    RECHARGE =  models.IntegerField(null=True)
    SQN =  models.IntegerField(null=True)
    config_ver = models.IntegerField(default=0)
    MOTIONDET =  models.CharField(max_length=50, default="off")
    TS = models.BigIntegerField(null=True)
    PowerSource = models.CharField(max_length=20, default="")
    PowerLineAlert = models.IntegerField(default="2")
    DataRecoInfo = models.CharField(max_length=250, default="")
    fullJSON = models.JSONField(null = True)
    fullJSON_date = models.DateTimeField(auto_now = True)
    gereq_flag = models.IntegerField(default=0)
    tracker_name = models.CharField(max_length=250, default="")  
    Encryptions = models.CharField(max_length=20, default="")  
    prefix_encryption_string = models.CharField(max_length=20, default="")   
    encryption_string = models.CharField(max_length=20, default="")   
    handshake_status = models.IntegerField(null=False,default=0)  
    capability_status = models.IntegerField(null=False,default=0)
    last_modified = models.DateTimeField(auto_now = True)

    class Meta:
        db_table = 'SuperAdmin_gfrid'
        # managed = True

class Tracker(models.Model):
    id = models.IntegerField(primary_key=True)
    customerID = models.IntegerField()
    batchId = models.IntegerField()
    mode = models.CharField(max_length=150)
    staffID_id = models.IntegerField()
    tracker = models.CharField(max_length=150)
    status = models.IntegerField()
    asset = models.CharField(max_length=150)
    application_domain = models.CharField(max_length=150)
    hardware_comments = models.CharField(max_length=500)
    software_comments = models.CharField(max_length=500)
    remarks = models.CharField(max_length=500)
    last_modified = models.DateTimeField()

    class Meta:
        db_table = 'SuperAdmin_cust_batches_trackers'

class login(models.Model):
    # id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=8)
    # tracker = models.CharField(max_length=40)
    login_at = models.DateTimeField(auto_now_add=True)

# class Version(models.Model):
#     id = models.IntegerField(primary_key=True)
#     version = models.CharField(max_length=50)
#     code = models.CharField(max_length=3)
#     sensors = models.CharField(max_length=200)
#     Json_File = models.CharField(max_length=100)
#     configtabs = models.CharField(max_length=200)
#     Json_File_Config = models.CharField(max_length=100)
#     last_modified = models.DateTimeField()

    # class Meta:
    #     db_table = 'SuperAdmin_version'

class Sensor(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    option = models.CharField(max_length=20)
    description = models.CharField(max_length=500)
    vendor = models.CharField(max_length=50)
    sensorClass = models.CharField(max_length=50)
    last_modified = models.DateTimeField()

    class Meta:
        db_table = 'SuperAdmin_sensors'

class s1_statusdata(models.Model):
    id = models.IntegerField(primary_key=True)
    gfrid = models.IntegerField()
    sensor = models.IntegerField()
    poststatus = models.CharField(max_length=10)
    geofence = models.CharField(max_length=10)
    last_modified = models.DateTimeField()

    class Meta:
        db_table = 's1_statusdata'

class ldv_statusdata(models.Model):
    id = models.IntegerField(primary_key=True)
    gfrid = models.IntegerField()
    sensor = models.IntegerField()
    poststatus = models.CharField(max_length=10)
    geofence = models.CharField(max_length=10)
    last_modified = models.DateTimeField()

    class Meta:
        db_table = 'ldv_statusdata'

class track_detail(models.Model):
    tracker = models.CharField(max_length=30)

class Loadcell(models.Model):
    id = models.IntegerField(primary_key=True)
    gfrid = models.IntegerField()
    # last_modified = models.DateTimeField()
    perwght = models.CharField(max_length=10)
    avgwght = models.CharField(max_length=10)
    wghtlist = models.CharField(max_length=10)
    ts = models.BigIntegerField()
    class Meta:
        db_table = 'ldv_lcell711'

class Gps(models.Model):
    id = models.IntegerField(primary_key=True)
    gfrid = models.IntegerField()
    last_modified = models.DateTimeField()
    lat = models.CharField(max_length=50)
    lon = models.CharField(max_length=50)
    ts = models.BigIntegerField()

    class Meta:
        db_table = 'ldv_gpsl86'

class ssub(models.Model):
    GFRID = models.IntegerField(null=False)
    sensorID = models.IntegerField(null=True)
    Sen_StartDate = models.BigIntegerField(null=True)
    Sen_EndDate = models.BigIntegerField(null=True)
    last_modified = models.DateTimeField(auto_now = True)

    class Meta:
        db_table = 'SuperAdmin_sensor_subscription'


class sub(models.Model):
    GFRID = models.IntegerField(null=False)
    custID = models.IntegerField(null=True)
    transaction = models.CharField(max_length=300)
    amount = models.BigIntegerField(null=True)
    duration = models.BigIntegerField(null=True)
    StartDate = models.BigIntegerField(null=True)
    EndDate = models.BigIntegerField(null=True)
    purchase_date = models.DateTimeField(auto_now = True)
    last_modified = models.DateTimeField(auto_now = True)

    class Meta:
        db_table = 'SuperAdmin_subscription'

class On(models.Model):
    gfrid = models.IntegerField(null=False)
    Status = models.IntegerField(null=False)
    TS = models.BigIntegerField(null=True)

    class Meta:
        db_table = 'v_on'

#-----------------------------------------------------------------------------------------------------
class AlertNotification(models.Model):
    alert_notice = models.CharField(max_length=200)
    hexVal = models.CharField(max_length=100)
    binaryVal = models.CharField(max_length=100)
    sensor = models.CharField(max_length=50)
    position = models.IntegerField(null=False,default=0)
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        db_table = 'SuperAdmin_alertnotification'

#-----------------------------------------------------------------------------------------------------
class Alerts(models.Model):
    alert = models.CharField(max_length=150)
    alertNotify = models.ForeignKey(AlertNotification, on_delete=models.CASCADE)
    status = models.IntegerField(null=True,default=2) # 2 means grey, 1 means red,0 means green
    GFRID = models.IntegerField(null=False)
    TS = models.DateTimeField(null=False, blank=False)
    TS_OFF = models.DateTimeField(null=True, blank=True)
    TS_BigInt = models.BigIntegerField(null = True)
    TS_OFF_BigInt = models.BigIntegerField(null=True)
    jsonFile = models.JSONField(null = True)
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        db_table = 'SuperAdmin_alerts'   

class System_Health(models.Model):
    GFRID = models.IntegerField(null=False)
    system_ontime = models.BigIntegerField(null=True)
    system_offtime = models.BigIntegerField(null=True)
    last_modified = models.DateTimeField(auto_now = True) 

#-----------------------------------------------------------------------
class TestCommands(models.Model):
    sensorID =  models.IntegerField(default=0)
    testcases = models.CharField(max_length=250)
    cmdjson = models.JSONField(null = True)
    admin_permission = models.CharField(max_length=150, default="")
    user_permission = models.CharField(max_length=150, default="")
    tester_permission = models.CharField(max_length=150, default="")    
    last_modified = models.DateTimeField(auto_now = True)  

    class Meta:
        db_table = 'superAdmin_testcommands'

#------------------------------------------------------------------------

class TrackerCommands(models.Model):
    tracker = models.CharField(max_length=150 )
    commandID = models.IntegerField(null=False)
    flag =  models.IntegerField(default=0)
    cmdOrder =  models.IntegerField(default=0)
    tester_flag =  models.IntegerField(default=0)
    tester_cmdOrder =  models.IntegerField(default=0)
    cust_flag =  models.IntegerField(default=0)
    cust_cmdOrder =  models.IntegerField(default=0)   
    process_flag = models.IntegerField(default=0)
    process_time = models.DateTimeField(auto_now = True)  
    last_modified = models.DateTimeField(auto_now = True)    

    class Meta:
        db_table = 'SuperAdmin_trackercommands'

class GSMSQL(models.Model):
    GFRID = models.IntegerField(null=False)
    SQValue =  models.IntegerField(default=0)
    TS = models.BigIntegerField(null=True)
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        db_table = 'SuperAdmin_gsmsql'