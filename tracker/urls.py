from django.urls import path,re_path
from . import views

urlpatterns = [
    path('',views.home,name='home'),
    path('login_auth/',views.login_auth,name='login_auth'),
    path('login/',views.login,name='login'),
    path('Machines/',views.Machine,name='Machine'),
    path('logout/',views.logout,name='logout'),
    # path('viewlive/<tracker>',views.ViewLive,name='viewlive'),
    re_path(r'^viewdetail/(?P<tracker>\w+)',views.ViewLive,name='viewdetail'),
    # re_path(r'^viewhistory/(?P<tracker>\w+)',views.ViewHist,name='viewhist'),
    # re_path(r'^Location/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewGps,name='location'),
    # re_path(r'^Histo/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewHisto,name='histo'),
    # re_path(r'^Statuson/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewOn,name='statuson'),
    # re_path(r'^viewjson/(?P<tracker>\w+)',views.ViewJson.as_view(),name='viewjson'),
    # re_path(r'^viewlive/(?P<tracker>\w+)/pastdata',views.ViewHist,name='viewhist')
    path('live_map/',views.Live_map, name='live_map'),
    path('pie_chart/',views.Pie_chart, name='pie_chart'),

    re_path(r'^Lock/',views.Lock,name="Lock"),
    re_path(r'^UnLock/',views.Unlock,name="UnLock"),
    re_path(r'^Lock1/',views.Lock1,name="Lock1"),
    re_path(r'^UnLock1/',views.Unlock1,name="UnLock1"),
    re_path(r'^Lock2/',views.Lock2,name="Lock2"),
    re_path(r'^UnLock2/',views.Unlock2,name="UnLock2"),
]
