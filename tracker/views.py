from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import gfr,Version,Sensor,On, AlertNotification,Alerts, TrackerCommands,GSMSQL
from dateutil import tz
from django.db import connection
from django.conf import settings
from datetime import datetime,timezone
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderServiceError
from plotly import graph_objs as go
from plotly import offline
# from rest_framework import status
from datetime import timedelta,datetime as dtime,timezone
from dateutil import tz as tzz
# from zoneinfo import ZoneInfo
from backports.zoneinfo import ZoneInfo
from itertools import islice
from dateutil.parser import parse
# from chart_studio import plotly as py

import matplotlib.pyplot as plt
import plotly.express as px
import plotly.offline as opy,pandas as pd
import plotly.graph_objects as go
import json,pytz,timeago,itertools,numpy as np
import folium,datetime as dte, time
import io,urllib,base64,plotly
import plotly.tools as tls
# import mpldatacursor
# import mplcursors

class create_dict(dict):
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        self[key] = value

# Create your views here.
@login_required(login_url='/Admin/login_auth/')
def home(request):
    return render(request, 'tracker/index1.html')

def login_auth(request):
    return render(request, 'tracker/login.html')

@csrf_exempt
def login(request):      
    global ver_user
    ver_user = request.POST.get('username')
    ver_pass = request.POST.get('password')
    vals = "vermeer"
    pwd = "Ver@2024"

    request.session.clear_expired()
    if ver_user == vals and ver_pass == pwd :
        request.session['ver_user'] = request.POST.get('username')

        return Machine(request)
    else :
        messages = "Invalid username/password"
        return render(request, 'tracker/login.html',{'messages':messages})

def Machine(request):
    session_timeout_seconds = request.session.get_expiry_age()
    if 'ver_user' not in request.session:    
        return redirect('/Admin/login_auth/')
    else:
        print(session_timeout_seconds)
        # vals = "a"
        # pwd = "a"
        data = []
        lu = []
        lua = []
        l_on = []
        l_com = []
        bn = []
        o_f = []
        f_f = []
        last_7 = []
        last_ts = []
        t_of = []
        t_of_m = []
        m_t = []
        
        # Get today's date
        end_date = dtime.now() - timedelta(days=1)

        # Generate a list of dates for the previous 7 days
        previous_7_days = [end_date - timedelta(days=i) for i in range(7)]
        previous_7_days = previous_7_days[::-1]
        #print(previous_7_days)

        # #print the dates
        for date in previous_7_days:
            last_7.append(date.strftime('%-d %b'))
            # last_7 = last_7[::-1]
            f = date.strftime('%Y %m %d')
            l = dte.datetime.strptime(f,'%Y %m %d')
            # # #print(f)
            last_ts.append(round(dte.datetime.timestamp(l)))
            # last_ts= last_ts[::-1]
        # #print(last_ts)
    
        c_id ="clinohealth67"
        c_pass ="onilc@112022"
        # ls = ['JIVSJILDVLCS3086','JIVSJILDVLCS8842','JIVSJILDVLCS3703','JIVSJILDVLCS5221']
        # ,'JIVSJIVERVME7124'
        ls = ['JIVSJIVERVME3693','JIVSJIVERVME9140']
        v_id = ['JIVMOT0001B0001','JIVMOT0002B0002']
        # veh_id = ['BL0183','SC1823']
        veh_id = ['test1','D130x150']
        mac_type = ['Balers','Horizontal Directional Drill']

        mydict = create_dict()

        for e in range(len(ls)):
            m_t.append([])
        
        for i in range(len(ls)):
            for j in range(7):
                m_t[i].append([])
        
        for t in range(len(ls)):                
            cust_tracker = ls[t]
        
            global subs_start
            global subs_end
            
            subs_start = 1669899600
            subs_end = 1809040092

            exst = gfr.objects.filter(tracker=cust_tracker).exists()
            g_id = gfr.objects.get(tracker=cust_tracker).id

            o_f.append(TrackerCommands.objects.get(tracker=cust_tracker,commandID=5).tester_flag)
            f_f.append(TrackerCommands.objects.get(tracker=cust_tracker,commandID=6).tester_flag)
            
            if TrackerCommands.objects.get(tracker=cust_tracker,commandID=3).process_flag == 0:
                bn.append(TrackerCommands.objects.get(tracker=cust_tracker,commandID=5).process_flag)

            else:
                on_t = TrackerCommands.objects.get(tracker=cust_tracker,commandID=5).process_time
                off_t = TrackerCommands.objects.get(tracker=cust_tracker,commandID=6).process_time

                if on_t > off_t:
                    bn.append(1)
                else:
                    bn.append(0)

            last_on = Alerts.objects.filter(alert="0x00000800",GFRID=g_id).last()
            last_com = GSMSQL.objects.filter(GFRID=g_id).last()
            cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

            r = 0
            o = 0
            while r<=6:
                for i in range(len(last_ts)):
                    s_date = last_ts[i]
                    e_date = last_ts[i] + 86399
                    #print(s_date,e_date)
                    t_o = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt__range=[s_date,e_date])
                    
                    for n in t_o:
                        m_t[t][i].append(n.TS_BigInt)
                        dt_ef = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M:%S')                
                        dt_e = dtime.strptime(dt_ef, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

                        if Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']:
                            of_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                            on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                            if on_z != of_z:
                                rr_t = dtime.strptime(dtime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                                # #print("ttttt",rr_t)
                                rr_p = pd.to_datetime(dtime.strftime(dte.datetime.now(),'%Y %m %d'))
                                num_days = (rr_p - rr_t).days
                                print("num111111",num_days)

                                s = i
                                for a in range(num_days):                              
                                    if s != i:
                                        # print(of_z,datetime.fromtimestamp(last_ts[s]).strftime("%Y %m %d") + " 00:00:00")
                                        if of_z == datetime.fromtimestamp(last_ts[s]).strftime("%Y %m %d") + " 00:00:00":
                                            break                        
                                        elif(last_ts[s] not in m_t[t][s]): 
                                            m_t[t][s].append(last_ts[s])
                                        r_tt = datetime.fromtimestamp(m_t[t][s][-1]).strftime("%Y %m %d") + " 23:59:59"
                                        r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                        if round(dte.datetime.timestamp(r_pp)) not in m_t[t][s]:
                                            m_t[t][s].append(round(dte.datetime.timestamp(r_pp))) 
                                    s = s+1

                            else:
                                rr_t = dtime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                                # #print("ttttt",rr_t)
                                rr_p = pd.to_datetime(dtime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                                num_days = (rr_p - rr_t).days
                                # print("num111111",num_days)

                                for i in range(num_days):
                                    # #print(i,ts_list[i])
                                    if last_ts[i] not in m_t[t][i]:
                                        m_t[t][i].append(last_ts[i])
                                    r_tt = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                                    r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                    if round(dte.datetime.timestamp(r_pp)) not in m_t[t][i]:
                                        m_t[t][i].append(round(dte.datetime.timestamp(r_pp)))
                                    current_date = rr_t + timedelta(days=i)
                                    day = current_date.day
                                    month = current_date.strftime('%m')
                                    year = current_date.year
                                    # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                        else:
                            #print("currrr",dte.datetime.now().timestamp())
                            m_t[t][i].append(e_date)
                            on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d")
                            rr_t = dtime.strptime(dtime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                            # #print("ttttt",rr_t)
                            rr_p = dtime.strptime(dtime.fromtimestamp(last_ts[6]).strftime("%Y %m %d"),'%Y %m %d')
                            num_days = (rr_p - rr_t).days
                            # #print("num111111",num_days)
                            s = i
                            for a in range(num_days): 
                                # if s != i:
                                s = s +1
                                print(s,len(last_ts))
                                if s <= len(last_ts)-1:
                                    if on_z == last_ts[s]:
                                        break                                              
                                    elif(last_ts[s] not in m_t[t][s]): 
                                        m_t[t][s].append(last_ts[s])
                                    # r_tt = datetime.fromtimestamp(r_p,tz=timezone.utc)
                                    # r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                    if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[t][s]:
                                        m_t[t][s].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp())) 
                                else:
                                    m_t[t][i].append(last_ts[i])
                                    if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[t][i]:
                                        m_t[t][i].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp()))

                    t_f = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_OFF_BigInt__range=[s_date,e_date])   

                    # #print("t_f",t_f)
                    for m in t_f:
                        # #print(m.TS_BigInt)
                        rr_t = dtime.strptime(datetime.fromtimestamp(m.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                        rr_p = dtime.strptime(datetime.fromtimestamp(m.TS_OFF_BigInt).strftime("%Y %m %d"),'%Y %m %d')

                        num_days = (rr_p - rr_t).days
                        # #print("num",num_days)
                        for j in range(num_days + 1):
                            current_date = rr_t + timedelta(days=j)
                            day = current_date.day
                            month = current_date.strftime('%m')
                            year = current_date.year
                            # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                                
                            if m.TS_OFF_BigInt not in m_t[t][i]:
                                # #print("ooooofff",i,m.TS_OFF_BigInt)
                                m_t[t][i].append(m.TS_OFF_BigInt)
                    r = r+1  

            if last_on:
                if last_on.TS_OFF_BigInt:
                    lt_t = datetime.fromtimestamp(last_on.TS_OFF_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                    lt = timeago.format(lt_t,cur)
                    l_on.append(lt)
                else:
                    lt_t = datetime.fromtimestamp(last_on.TS_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                    lt = timeago.format(lt_t,cur)
                    l_on.append("Running")
            else:
                l_on.append("no status")

            if last_com:
                lt_c = datetime.fromtimestamp(last_com.TS).strftime("%Y-%m-%d %H:%M:%S")
                lc = timeago.format(lt_c,cur)
                l_com.append(lc)
            else:
                l_com.append("no status")

            if exst is True:
                ts_now = round(datetime.now().replace(tzinfo=timezone.utc).timestamp())
                if ts_now < subs_end:
                    ver = gfr.objects.get(tracker=cust_tracker).version_id
                    ver_code = (Version.objects.get(id=ver).code).lower()
                    sensor_code = "gpsl86"
                    cursor1 = connection.cursor()
                    cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                    da = cursor1.fetchall()
                    print(da)
                    data.append(da[0])    
                    mydict.add("Customer",({"name": f"{c_id}","tracker":cust_tracker,"l_on":l_on,"bn":bn,"o_f":o_f,"f_f":f_f,'l_com':l_com,"veh_id":veh_id,"mac_type":mac_type,"v_id":v_id,"len":len(veh_id)+1}))
                    # print(mydict)
            print(o_f,f_f,bn)
            
            a_id = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9)
            for t in range(len(ls)):            
                for i in range(len(m_t[t])):                    
                    if len(m_t[t][i]) == 0:
                        print("empty")

                    elif(sorted(m_t[t][i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_t[t][i])[-1] in [i.TS_BigInt for i in a_id]):                
                        r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 00:00:00"
                        r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                        if round(dte.datetime.timestamp(r_p)) not in m_t[t][i]:
                            m_t[t][i].append(round(dte.datetime.timestamp(r_p)))

                        r_tt = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                        r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                        if round(dte.datetime.timestamp(r_pp)) not in m_t[t][i]:
                            m_t[t][i].append(round(dte.datetime.timestamp(r_pp)))

                    elif(sorted(m_t[t][i])[0] in [i.TS_OFF_BigInt for i in a_id]):
                        # #print(m_t[t][i][0])
                        r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 00:00:00"
                        r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                        if round(dte.datetime.timestamp(r_p)) not in m_t[t][i]:
                            m_t[t][i].append(round(dte.datetime.timestamp(r_p)))

                    elif(sorted(m_t[t][i])[-1] in [i.TS_BigInt for i in a_id]):
                        print(m_t[t][i][-1],[i.TS_BigInt for i in a_id])                
                        r_t = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                        r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                        if round(dte.datetime.timestamp(r_p)) not in m_t[t][i]:
                            m_t[t][i].append(round(dte.datetime.timestamp(r_p)))
                        
                    elif(m_t[t][i][0] in last_ts):                
                        r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 23:59:59"
                        r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                        if round(dte.datetime.timestamp(r_p)) not in m_t[t][i]:
                            m_t[t][i].append(round(dte.datetime.timestamp(r_p)))

        main_ts_list = []
        # main_ts_l = []
        for i in range(len(ls)):
            main_ts_list.append([])
            # main_ts_l.append([])
            for j in range(len(m_t[i])):
                main_ts_list[i].append([])
                
        for i in range(len(ls)):
            for j in range(len(m_t[i])):
                main_ts_list[i][j].extend(sorted(set((m_t[i][j]))))
        
        print(main_ts_list)

        m_list = []
        for i in range(len(ls)):
            m_list.append([])
            for j in range(len(main_ts_list[i])):
                m_list[i].append([])

        for i in range(len(ls)):
            for j in range(len(main_ts_list[i])):
                timestamp = main_ts_list[i][j]
                datetime_objects = [datetime.fromtimestamp(timestamp) for timestamp in timestamp]

                # Calculate time differences in hours
                time_differences_hours = []
                for k in range(1, len(datetime_objects)):
                    time_diff = (datetime_objects[k] - datetime_objects[k - 1]).total_seconds() / 3600
                    time_differences_hours.append(time_diff)

                # #print the hour-wise time differences
                for k, diff in enumerate(time_differences_hours, start=1):
                    m_list[i][j].append(float(f"{diff:.2f}"))
                    # #print(f"Hour {k}: {diff:.2f} hours")
        # #print(m_list)

        mn_list = []
        for i in range(len(ls)):
            mn_list.append([])
            for j in range(len(m_list[i])):
                mn_list[i].append([])

        for i in range(len(ls)):
            for j in range(len(m_list[i])):
                mn_list[i][j].append(sum(m_list[i][j][::2]))
        # #print(mn_list)

        main_list = []
        total_list = []
        for i in range(len(ls)):
            main_list.append([])

        for i in range(len(mn_list)):
            for j in range(len(mn_list[i])):
                main_list[i].append(round(mn_list[i][j][0]))
            total_list.append(sum(main_list[i]))
        # #print(main_list)

        # print(main_list)
        # print(last_7[::-1],main_list[0][::-1])
        mydict.add("lst",({"lst":main_list}))

        for i in range(len(veh_id)):
            colors1 = ['blue',] * 7
            for j in range(len(main_list[i])):
                if main_list[i][j] > 8:                
                    colors1[j] = 'red'
            colors1 = colors1[::-1]

            # text = [f"{main_list[i][::-1][j]}" for j in range(len(main_list[i]))]

            trace = {
                    "x": last_7[::-1],
                    "y": main_list[i][::-1],
                    # "line": {"shape": 'hv'},
                    # "mode": 'lines',
                    # "text" : text,
                    # "textposition": 'auto',
                    "name": 'ASSET_ON_STATUS',
                    "type": 'bar',
                    
                    "marker_color" : colors1
                    }
            data1 = [trace]
            layout = go.Layout(margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
            figure = go.Figure(data = data1, layout = layout) 
            figure.update_xaxes(showticklabels=False)
            figure.update_yaxes(showticklabels=False,range = [0,24])
            figure.update_layout(
                yaxis_title="Hours",
                yaxis = dict(
                    # tickmode = 'linear',
                    tick0 = 2,
                    dtick = 2
                )
            )
            plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False,'responsive': True})
            mydict.add(f"bar{i+1}",({f"plot{i+1}":plot_div}))

        # colors2 = ['blue',] * 7
        # for j in range(len(main_list[1])):
        #     if main_list[1][j] > 8:
        #         colors2[j] = 'red'
        # colors2 = colors2[::-1]

        # trace1 = {
        #                 "x": last_7[::-1],
        #                 "y": main_list[1][::-1],
        #                 # "line": {"shape": 'hv'},
        #                 # "mode": 'lines',
        #                 "name": 'ASSET_ON_STATUS',
        #                 "type": 'bar',
        #                 "marker_color" : colors2
        #                 }
        # data1 = [trace1]
        # layout1 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
        # figure1 = go.Figure(data = data1, layout = layout1)
        # figure1.update_xaxes(showticklabels=False)
        # figure1.update_yaxes(showticklabels=False,range = [0,24])
        # figure1.update_layout(
        #     yaxis_title="Hours",
        #     yaxis = dict(
        #         # tickmode = 'linear',
        #         tick0 = 2,
        #         dtick = 2
        #     )
        # )           
        # plot_div = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False,'responsive': True})
        # mydict.add("bar1",({"plot1":plot_div}))

        # colors3 = ['blue',] * 7
        # for j in range(len(main_list[2])):
        #     if main_list[2][j] > 8:
        #         colors3[j] = 'red'
        # colors3 = colors3[::-1]

        # trace2 = {
        #                 "x": last_7[::-1],
        #                 "y": main_list[2][::-1],
        #                 # "line": {"shape": 'hv'},
        #                 # "mode": 'lines',
        #                 "name": 'ASSET_ON_STATUS',
        #                 "type": 'bar',     
        #                 "marker_color" : colors3                 
        #                 }
        # data2 = [trace2]
        # layout2 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
        # figure2 = go.Figure(data = data2, layout = layout2)
        # figure2.update_xaxes(showticklabels=False)
        # figure2.update_yaxes(showticklabels=False,range = [0,24])
        # figure2.update_layout(
        #     yaxis_title="Hours",
        #     yaxis = dict(
        #         # tickmode = 'linear',
        #         tick0 = 2,
        #         dtick = 2
        #     )
        # )
        # plot_div = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
        # mydict.add("bar2",({"plot2":plot_div}))   

        # colors4 = ['blue',] * 7
        # # colors4[0] = 'red'
        # # colors4[1] = 'red'
        # # colors4[2] = 'red'
        # # colors4[4] = 'red'
        # # colors4[j] = 'red'

        # for j in range(len(main_list[3])):
        #     if main_list[3][j] > 8:
        #         colors4[j] = 'red'
        # colors4 = colors4[::-1]


        # trace3 = {
        #                 "x": last_7[::-1],
        #                 "y":main_list[3][::-1],
        #                 # "y": [24,24,24,7,9,6,0],
        #                 # "line": {"shape": 'hv'},
        #                 # "mode": 'lines',
        #                 "name": 'ASSET_ON_STATUS',
        #                 "type": 'bar',     
        #                 "marker_color" : colors4                 
        #                 }
        # data3 = [trace3]
        # layout3 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
        # figure3 = go.Figure(data = data3, layout = layout3)
        # figure3.update_xaxes(showticklabels=False)
        # figure3.update_yaxes(showticklabels=False,range = [0,24])
        # figure3.update_layout(
        #     yaxis_title="Hours",
        #     yaxis = dict(
        #         # tickmode = 'linear',
        #         tick0 = 2,
        #         dtick = 2
        #     )
        # )
        # plot_div = opy.plot(figure3, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
        # mydict.add("bar3",({"plot3":plot_div}))

        m = folium.Map(location=[13.0827,80.2707], width="%100", height="100%",zoom_start=10, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
        for row in data:
            trac = gfr.objects.get(id=row[1]).tracker
            if trac == "JIVSJIVERVME3693":
                cust_trac = 'JIVMOT0001B0001'
                veh = "BL0183"
            elif trac == "JIVSJIVERVME9140":
                cust_trac = 'JIVMOT0002B0002'
                veh = "D130x150"
            # elif trac == "JIVSJIVERVME7124":
            #     cust_trac = 'JIVMOT0003B0003'
            #     veh = "PT1945"
            # elif trac == "JIVSJILDVLCS6123":
            #     cust_trac = 'JIVMOT0004B0004'
            #     veh = "MS0123"
 
            lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
            cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
            cur = dte.datetime.strptime(cur,'%Y-%m-%d %H:%M:%S')
            lc_ts = dte.datetime.strptime(lc_ts,'%Y-%m-%d %H:%M:%S')
            loc_ts = timeago.format(lc_ts,cur)
            lu.append(loc_ts)
            iframe = folium.IFrame("Lat : "+str(cust_trac))
            try:
                geolocator = Nominatim(user_agent="my_geocoder")
                location = geolocator.reverse((row[3].strip()[:-1],row[4].strip()[:-1]), exactly_one=True)
                address = location.raw.get("address", {})
                loc_city = address.get('state','')
                loc_pin = address.get('postcode','')
                l_cp = str(loc_city)+"," +str(loc_pin)
                lua.append(l_cp)
            except GeocoderServiceError as e:
                #print("Geocoder Service Error:", e)
                return None, None
            # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
            tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Vehicle :"+str(veh)
            folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),tooltip=tooltip).add_to(m)

        mydict.add("lu",({"TS":lu,"tracker":ls}))    
        mydict.add("lua",({"loc":lua}))
        mydict.add("total",({"total":total_list}))

        return render(request, 'tracker/index1.html',context=mydict)

# def Pie_chart(request):        
#     labels = ['Balers','Mini Skid Steers','Pipeline Trenchers','Horizontal Directional Drill','Pedestrian Trenchers','Pile Driver']
#     values = [4,6,5,2,7,6]

#     fig = go.Figure(data=[go.Pie(labels=labels, values=values,
#                                 insidetextorientation='radial'
#                                 )])

#     fig.update_layout(autosize=True,  # Automatically adjust the size to fit the content
#     margin=dict(l=0, r=0, b=0, t=40),  # Adjust margin for better mobile display
#     )
    
#     fig.update_traces(textinfo='none')
#     # fig = fig.update_layout(uniformtext_minsize=12, uniformtext_mode='hide',plot_bgcolor = "rgb(0,0,0,0)",font={"color":"black","family": "Times, serif"})
#     plot_div = opy.plot(fig, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
         
#     # ms=m._repr_html_()
#     mydict = create_dict()
#     mydict.add("piechart",({"pie":plot_div}))
#     return render(request, 'tracker/pie_chart.html',context=mydict)

def Pie_chart(request):        
    labels = ['Balers', 'Mini Skid Steers', 'Pipeline Trenchers', 'Horizontal Directional Drill', 'Pedestrian Trenchers', 'Pile Driver']
    values = [4, 6, 5, 2, 7, 6]

    
    colors = [
    'rgb(78, 200, 111)',    # Light Green
    'rgb(146, 228, 124)',   # Lighter Green
    'rgb(255, 247, 124)',   # Light Yellow
    'rgb(255, 222, 143)',   # Lighter Yellow
    'rgb(255, 179, 102)',   # Light Orange
    'rgb(255, 152, 109)'    # Lighter Orange
]

    fig = go.Figure(data=[go.Pie(labels=labels, values=values, marker=dict(colors=colors), insidetextorientation='radial')])

    fig.update_layout(
        autosize=True,  # Automatically adjust the size to fit the content
        margin=dict(l=0, r=0, b=0, t=40),  # Adjust margin for better mobile display
    )
    
    fig.update_traces(textinfo='none')

    # Plotly configuration
    plot_div = opy.plot(fig, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False, 'displayModeBar': False})

    mydict = create_dict()
    mydict.add("piechart", {"pie": plot_div})

    return render(request, 'tracker/pie_chart.html', context=mydict)


def Live_map(request):
    ls = ['JIVSJIVERVME3693','JIVSJIVERVME9140']
    data = []
    for t in range(len(ls)):                
        cust_tracker = ls[t]
        g_id = gfr.objects.get(tracker=cust_tracker).id
        ver = gfr.objects.get(tracker=cust_tracker).version_id
        ver_code = (Version.objects.get(id=ver).code).lower()
        sensor_code = "gpsl86"
        cursor1 = connection.cursor()
        cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
        da = cursor1.fetchall()
        print(da)
        data.append(da[0])

    m = folium.Map(location=[13.0827,80.2707], width="%100", height="100%",zoom_start=10, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
    for row in data:
        trac = gfr.objects.get(id=row[1]).tracker
        if trac == "JIVSJIVERVME3693":
            cust_trac = 'JIVMOT0001B0001'
            veh = "BL0183"
        elif trac == "JIVSJIVERVME9140":
            cust_trac = 'JIVMOT0002B0002'
            veh = "D130x150"
        # elif trac == "JIVSJIVERVME7124":
        #     cust_trac = 'JIVMOT0003B0003'
        #     veh = "PT1945"
        # elif trac == "JIVSJILDVLCS6123":
        #     cust_trac = 'JIVMOT0004B0004'
        #     veh = "MS0123"

        # lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
        # cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        # cur = dte.datetime.strptime(cur,'%Y-%m-%d %H:%M:%S')
        # lc_ts = dte.datetime.strptime(lc_ts,'%Y-%m-%d %H:%M:%S')
        # loc_ts = timeago.format(lc_ts,cur)
        # lu.append(loc_ts)
        iframe = folium.IFrame("Lat : "+str(cust_trac))
        try:
            geolocator = Nominatim(user_agent="my_geocoder")
            location = geolocator.reverse((row[3].strip()[:-1],row[4].strip()[:-1]), exactly_one=True)
            address = location.raw.get("address", {})
            loc_city = address.get('state','')
            loc_pin = address.get('postcode','')
            l_cp = str(loc_city)+"," +str(loc_pin)
            # lua.append(l_cp)
        except GeocoderServiceError as e:
            #print("Geocoder Service Error:", e)
            return None, None
        # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Vehicle :"+str(veh)
        folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),tooltip=tooltip).add_to(m)

    ms=m._repr_html_()
    mydict = create_dict()
    mydict.add("folium",({"map":ms}))
    return render(request, 'tracker/map.html',context=mydict)
    
def logout(request):
    if 'ver_user' in request.session: del request.session['ver_user']
    return HttpResponseRedirect('/Admin/')

def split(list_a, chunk_size):
  for i in range(0, len(list_a), chunk_size):
    yield list_a[i:i + chunk_size]

def ViewLive(request,tracker):
    global veh
    if 'ver_user' not in request.session:    
        return redirect('/Admin/login_auth/?next=/Admin/')
    else:
        c1 = []
        x_ts = []
        sm_d = []
        sm_e = []
        j = 1

        if tracker == "JIVMOT0001B0001":
            cust_trac = 'JIVSJIVERVME3693'
            veh = "BL0183"
            ty = "Balers"
        elif tracker == "JIVMOT0002B0002":
            cust_trac = 'JIVSJIVERVME9140'
            veh = "D130x150"
            ty = "Horizontal Directional Drill"
        # elif tracker == "JIVMOT0003B0003":
        #     cust_trac = 'JIVSJIVERVME7124'
        #     veh = "PT1945"
        #     ty = "Pipeline Trenchers"
        # elif tracker == "JIVMOT0004B0004":
        #     cust_trac = 'JIVSJILDVLCS6123'
        #     veh = "MS0123"
        #     ty = "Mini Skid Steers"
        
        sensorname = 'gpsl86'

        ver =  gfr.objects.get(tracker=cust_trac).version_id
        verCode = (Version.objects.get(id=ver).code).lower()

        tblename = verCode.lower()+"_"+sensorname.lower()

        Gfr = gfr.objects.get(tracker = cust_trac)
        gfrid = Gfr.id
        dt_range = request.POST.get('datetimes')

        last_on = Alerts.objects.filter(alert="0x00000800",GFRID=gfrid).last()
        last_com = GSMSQL.objects.filter(GFRID=gfrid).last()
        cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

        if last_on:
            if last_on.TS_OFF_BigInt:
                lt_t = datetime.fromtimestamp(last_on.TS_OFF_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                lt = timeago.format(lt_t,cur)
                l_on=lt
            else:
                lt_t = datetime.fromtimestamp(last_on.TS_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                lt = timeago.format(lt_t,cur)
                l_on="running"
        else:
            l_on="no status"

        if last_com:
            lt_c = datetime.fromtimestamp(last_com.TS).strftime("%Y-%m-%d %H:%M:%S")
            lc = timeago.format(lt_c,cur)
            l_com=lc
        else:
            l_com = "no status"

        cursor1 = connection.cursor()
        cursor1.execute(f"SELECT * FROM {verCode}_{sensorname} WHERE gfrid = {gfrid} ORDER BY last_modified DESC LIMIT 1")
        da = cursor1.fetchall()[0]
        # data.append(da[0])

        try:
            geolocator = Nominatim(user_agent="my_geocoder")
            location = geolocator.reverse((da[3].strip()[:-1],da[4].strip()[:-1]), exactly_one=True)
            address = location.raw.get("address", {})
            loc_city = address.get('state','')
            loc_pin = address.get('postcode','')
            l_cp = str(loc_city)+"," +str(loc_pin)
    
        except GeocoderServiceError as e:
            #print("Geocoder Service Error:", e)
            return None, None

        print(l_on,l_cp)

        if not dt_range:
            dt_string = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y %b %d')
            dt_string3 = (dtime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=7)).strftime('%Y %b %d')
            
            dt_range = f"{dt_string3} - {dt_string}"
            dates = dt_range.split("-")
            
            dt_ef = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d')
            dt_sf = (dtime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=7)).strftime('%Y-%m-%d')

            dt_e = dtime.strptime(dt_ef, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))
            dt_s = dtime.strptime(dt_sf, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))

            start_date = round(dt_s.replace(tzinfo=timezone.utc).timestamp())
            end_date = round(dt_e.replace(tzinfo=timezone.utc).timestamp())

            # #print("1",start_date,end_date)

        else:
            dates = dt_range.split("-")
            # #print("DATERANGE:: ",dates)
            
            s = dates[0]
            t = dates[0].strip()

            start = t.replace("/","-")
            # drs = datetime.strftime(t, '%Y %b %d %H:%M:%S')
            last = dtime.strptime(start, '%Y %b %d').astimezone(pytz.timezone("UTC"))
            
            start_date = round(last.replace(tzinfo=timezone.utc).timestamp())
            # # #print(int(start_date))
            e = dates[1]
            nw = dates[1].strip()
            end = nw.replace("/","-")
            # dre = dtime.strftime(nw, '%Y %b %d %H:%M:%S')
            now = dtime.strptime(end, '%Y %b %d').astimezone(pytz.timezone("UTC"))
            
            end_date = round(now.replace(tzinfo=timezone.utc).timestamp())

            # #print("2",start_date,end_date)

        current_day = datetime.strptime(dtime.fromtimestamp(start_date).strftime("%Y-%m-%d"), "%Y-%m-%d")
        day_list = []
        while current_day <= datetime.strptime(dtime.fromtimestamp(end_date).strftime("%Y-%m-%d"), "%Y-%m-%d"):
            day_list.append(current_day.strftime("%Y-%m-%d"))            
            current_day += timedelta(days=1)

        ts_list = []
        for i in day_list:
            ts_list.append(round(pd.to_datetime(i).timestamp()))
        
        # #print(type(current_day))
        # #print(dtime.strftime(pd.to_datetime(dtime.fromtimestamp(end_date).strftime("%Y-%m-%d")),"%Y-%m-%d"))
        #print(day_list)
        # #print(ts_list)

        m_t = []
        for e in range(len(ts_list)):
            m_t.append([])

        for i in range(len(ts_list)):
            s_date = ts_list[i]
            e_date = ts_list[i] + 86399
            #print(s_date,e_date)
            t_o = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt__range=[s_date,e_date])
            
            for n in t_o:
                m_t[i].append(n.TS_BigInt)
                dt_ef = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M:%S')                
                dt_e = dtime.strptime(dt_ef, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

                if Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']:
                    of_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                    on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                    if on_z != of_z:   
                        rr_t = dtime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                        # #print("ttttt",rr_t)
                        rr_p = pd.to_datetime(dtime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                        num_days = (rr_p - rr_t).days
                        # #print("num111111",num_days)

                        r = i
                        for a in range(num_days):  
                            #print("lllllllllll",r,len(ts_list))
                            if r != i:
                                if r == len(ts_list):
                                    break                        
                                elif(ts_list[r] not in m_t[r]): 
                                    m_t[r].append(ts_list[r])
                                r_tt = datetime.fromtimestamp(m_t[r][-1]).strftime("%Y %m %d") + " 23:59:59"
                                r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                if round(dte.datetime.timestamp(r_pp)) not in m_t[r]:
                                    m_t[r].append(round(dte.datetime.timestamp(r_pp))) 
                            r = r+1

                    else:
                        rr_t = dtime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                        # #print("ttttt",rr_t)
                        rr_p = pd.to_datetime(dtime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                        num_days = (rr_p - rr_t).days
                        # #print("num111111",num_days)

                        for i in range(num_days):
                            print(i,ts_list[i])
                            if ts_list[i] not in m_t[i]:
                                m_t[i].append(ts_list[i])
                            r_tt = datetime.fromtimestamp(m_t[i][-1]).strftime("%Y %m %d") + " 23:59:59"
                            r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                            if round(dte.datetime.timestamp(r_pp)) not in m_t[i]:
                                m_t[i].append(round(dte.datetime.timestamp(r_pp)))
                            current_date = rr_t + timedelta(days=i)
                            day = current_date.day
                            month = current_date.strftime('%m')
                            year = current_date.year
                            # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                else:
                    # m_t[i].append(e_date)
                    on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d")
                    rr_t = dtime.strptime(dtime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                    # #print("ttttt",rr_t)
                    rr_p = pd.to_datetime(dtime.strftime(dte.datetime.now(),'%Y %m %d'))
                    # r_p = dte.datetime.now(timezone.utc)
                    print(dt_e)
                    num_days = (rr_p - rr_t).days
                    print("num111111",num_days)
                    s = i
                    for a in range(num_days): 
                        # if s != i:
                        s = s +1

                        if s <= len(ts_list)-1:
                            if on_z == ts_list[s]:
                                break                                              
                            elif(ts_list[s] not in m_t[s]): 
                                m_t[s].append(ts_list[s])
                            # r_tt = datetime.fromtimestamp(r_p,tz=timezone.utc)
                            # r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                            if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[s]:
                                m_t[s].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp())) 
                        else:
                            m_t[i].append(ts_list[s-1])
                            if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[s-1]:
                                m_t[i].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp()))
                        # s = s+1

            t_f = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_OFF_BigInt__range=[s_date,e_date])   
        
            # #print("t_f",t_f)
            for m in t_f:
                # #print(m.TS_BigInt)
                rr_t = dtime.strptime(datetime.fromtimestamp(m.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                rr_p = dtime.strptime(datetime.fromtimestamp(m.TS_OFF_BigInt).strftime("%Y %m %d"),'%Y %m %d')

                num_days = (rr_p - rr_t).days
                # #print("num",num_days)
                for j in range(num_days + 1):
                    current_date = rr_t + timedelta(days=j)
                    day = current_date.day
                    month = current_date.strftime('%m')
                    year = current_date.year
                    # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                        
                    if m.TS_OFF_BigInt not in m_t[i]:
                        # #print("ooooofff",i,m.TS_OFF_BigInt)
                        m_t[i].append(m.TS_OFF_BigInt)
                
        print(m_t)
        m_tt = []
        for i in range(len(ts_list)):
            m_tt.append([])
        for i in range(len(m_t)):
            if len(m_t[i])!=0:
                m_tt[i].extend(sorted(m_t[i]))
            else:
                m_tt[i].extend(m_t[i])

        a_id = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9)
        for i in range(len(m_tt)):
            if len(m_tt[i]) == 0:
                print("empty")

            elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]) and Alerts.objects.get(GFRID=gfrid,alertNotify_id=9,TS_BigInt=m_tt[i][-1]).TS_OFF_BigInt:          
                r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
                r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                if round(dte.datetime.timestamp(r_p)) not in m_tt[i]:
                    m_tt[i].append(round(dte.datetime.timestamp(r_p)))

                r_tt = datetime.fromtimestamp(m_tt[i][-1]).strftime("%Y %m %d") + " 23:59:59"
                r_pp = dte.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                if round(dte.datetime.timestamp(r_pp)) not in m_tt[i]:
                    m_tt[i].append(round(dte.datetime.timestamp(r_pp)))
                print("mtt",m_tt[i])

            # elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]) and not Alerts.objects.get(GFRID=gfrid,alertNotify_id=9,TS_BigInt=m_tt[i][-1]).TS_OFF_BigInt:                
            #     r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
            #     r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
            #     if round(dte.datetime.timestamp(r_p)) not in m_tt[i]:
            #         m_tt[i].append(round(dte.datetime.timestamp(r_p)))

            #     if round(round(datetime.now().replace(tzinfo=timezone.utc).timestamp())) not in m_tt[i]:
            #         m_tt[i].append(round(datetime.now().replace(tzinfo=timezone.utc).timestamp()))

            elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]):
                # #print(m_t[t][i][0])
                r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
                r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                if round(dte.datetime.timestamp(r_p)) not in m_tt[i]:
                    m_tt[i].append(round(dte.datetime.timestamp(r_p)))

            elif(sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]):  
                print(i,m_tt[i][-1])           
                r_t = datetime.fromtimestamp(m_tt[i][-1]).strftime("%Y %m %d") + " 23:59:59"
                r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                if round(dte.datetime.timestamp(r_p)) not in m_tt[i]:
                    m_tt[i].append(round(dte.datetime.timestamp(r_p)))

            elif(sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]):
                print(i,len(m_tt)-1) 
                if round(round(datetime.now().replace(tzinfo=timezone.utc).timestamp())) not in m_tt[i]:
                    m_tt[i].append(round(datetime.now().replace(tzinfo=timezone.utc).timestamp()))
                
            elif(m_tt[i][0] in ts_list):                
                r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 23:59:59"
                r_p = dte.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                if round(dte.datetime.timestamp(r_p)) not in m_tt[i]:
                    m_tt[i].append(round(dte.datetime.timestamp(r_p)))
        print("mtt",m_tt)
        main_t = []
        main_tt = []
        main_y = []
        for i in range(len(ts_list)):
            main_t.append([])
            main_tt.append([])
            main_y.append([])

        for i in range(len(m_tt)):
            if len(m_tt[i])!=0:
                main_t[i].extend(sorted(m_tt[i]))
            else:
                main_t[i].extend(m_tt[i])

        # Iterate through the list and create pairs
        for i in range(len(main_t)):
            for j in range(0, len(main_t[i]), 2):
                if len(main_t[i]) == 0:
                    main_tt[i].append([0,0])
                if j + 1 < len(main_t[i]):
                    gmt_timezone = pytz.timezone('GMT')
                    if float(datetime.fromtimestamp(main_t[i][j], gmt_timezone).strftime("%-H.%M")) != float(datetime.fromtimestamp(main_t[i][j+1], gmt_timezone).strftime("%-H.%M")):
                        main_tt[i].append([float(datetime.fromtimestamp(main_t[i][j], gmt_timezone).strftime("%-H.%M")),float(datetime.fromtimestamp(main_t[i][j+1], gmt_timezone).strftime("%-H.%M"))])

        for i in range(len(main_tt)):
            if len(main_tt[i]) == 0:
                main_y[i].append([0,0])
            for j in range(len(main_tt[i])):
                main_y[i].append(main_tt[i][j]) 

        print(main_y)

        main_yy = main_y

        res = {}
        for key in day_list:
            for value in main_yy:
                res[key] = value
                main_yy.remove(value)
                break
        print(res)

        bar_traces = []

        # Iterate through each day's usage time ranges
        for day, ranges in res.items():
            for start_hour, end_hour in ranges:
                usage_duration = end_hour - start_hour

                hover_text = f"Day: {day} Start: {start_hour} End: {end_hour} Duration: {round(usage_duration,2)} hrs"

                # Create a bar trace for the specified usage hours
                bar_trace = go.Bar(
                    x=[day],
                    y=[usage_duration],
                    base=start_hour,
                    hoverinfo='text',
                    text=hover_text,
                    marker=dict(color='blue'),
                    showlegend=False,                   
                )

                bar_traces.append(bar_trace)

        # Create a Plotly layout
        layout = go.Layout(
            margin=dict(l=0, r=20, b=0, t=20, pad=0),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(showgrid=True, showticklabels=True),
        )

        # Create a Plotly figure with the bar traces and layout
        
        # type='category',
        # categoryorder='array',
        # categoryarray=list(res.keys()),

        fig = go.Figure(data=bar_traces, layout=layout)
        fig.update_xaxes(title="Days",tickmode='linear', ticklabelmode="period")
        fig.update_yaxes(fixedrange=False,automargin=True,range=[0,24],title="Hours")
        # fig.show()

        # Show the Plotly figure
        plot_div = opy.plot(fig, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
        # Create a figure and axis
        # fig, ax = plt.subplots()

        # # Iterate through each day's usage time ranges
        # for day, ranges in res.items():
        #     for start_hour, end_hour in ranges:
        #         usage_duration = end_hour - start_hour
        #     # #print(usage_duration)
            
        #         # Plot a bar for the specified usage hours
        #         bar = ax.bar(day, usage_duration, bottom=start_hour, color='blue')

        #         tooltip_text = f"{round(usage_duration,2)} hrs"
        #         if tooltip_text != "0 hrs":
        #             ax.annotate(tooltip_text, xy=(day, end_hour),fontsize=4)

        # # Set y-axis range to 24 hours
        # ax.set_ylim(0, 24)
        # ax.xaxis.set_major_locator(plt.MaxNLocator(10))
        # plt.xticks(rotation=45)

        # # buf = io.BytesIO()
        # # fig.tight_layout()
        # # fig.savefig(buf,format='png')
        # # buf.seek(0)
        # # string = base64.b64encode(buf.read())
        # # uri = "data:image/png;base64," + urllib.parse.quote(string)

        # # x = day_list
        # # y = main_y

        # # # Create a Plotly Image trace (Not recommended for images; for illustration purposes only)
        # # trace = go.Image(
        # #     source=uri,
        # #     hoverinfo='none'
        # # )

        # trace = go.Bar(
        #     source=plt,
        #     hoverinfo='none'
        # )

        # # # Create a Plotly layout with an image added to it
        # layout = go.Layout(
        #     margin=dict(l=0, r=0, b=0, t=0, pad=0),
        #     images=[dict(
        #         source=uri,
        #         x=0,  # X-coordinate (left)
        #         y=1,  # Y-coordinate (top)
        #         xref='paper',  # Reference relative to the paper
        #         yref='paper',
        #         opacity=1,  # Opacity of the image
        #         layer='below'
        #     )],
        #     autosize=True,
        #     xaxis=dict(showgrid=False, zeroline=False,showticklabels=False),
        #     yaxis=dict(showgrid=False, zeroline=False,showticklabels=False),
        # )
        
        # # Create a Plotly figure with the Image trace and layout
        # figg = go.Figure(data=[trace], layout=layout)
        # # figg = px.imshow(uri)

        # # # Show the Plotly figure
        # plot_div = opy.plot(figg, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
    return render(request,'tracker/viewdetail.html',{'grph':plot_div,'vehicle':veh,'l_on':l_on,'l_com':l_com,'l_cp':l_cp,'ty':ty,'dt':f"Selected date range  :  {dt_range}"})
    
def Unlock(request):
    if TrackerCommands.objects.get(id=89).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=89).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=133).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect(Machine)

def Lock(request):
    if TrackerCommands.objects.get(id=90).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=90).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=134).process_flag == 0:
        # return HttpResponse("In process")
    return redirect(Machine)

def Unlock1(request):
    if TrackerCommands.objects.get(id=95).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=95).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=155).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect(Machine)

def Lock1(request):
    if TrackerCommands.objects.get(id=96).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=96).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=156).process_flag == 0:
        # return HttpResponse("In process")
    return redirect(Machine)

def Unlock2(request):
    if TrackerCommands.objects.get(id=83).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=83).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=155).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect(Machine)

def Lock2(request):
    if TrackerCommands.objects.get(id=84).process_flag == 0:
        tc = TrackerCommands.objects.filter(id=84).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=156).process_flag == 0:
        # return HttpResponse("In process")
    return redirect(Machine)