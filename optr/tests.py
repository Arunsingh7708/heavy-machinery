from json import encoder
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, response
from numpy.lib.twodim_base import tri
from SuperAdmin.models import Config_Permission, Staff, Batches_Trackers, Cust_Batches_Trackers, Customers, GFRID, SimInfo, Notification, Version, Sensors, SensorVersion, ConfigTab, TestCommands, SensorCommands, AlertNotification, AlertsLink, Alerts, TrackerCommands, System_Health,GSMSQL, SensorShow
from SuperAdmin.views import createNewDB, getTableFields, version
from .forms import UpdateForm
import numpy as np
from django.db import connection
import folium
from django.db.models import  Avg
from geopy.geocoders import Nominatim
from django.http import JsonResponse
from .forms import TestTrackerDataForm
from datetime import datetime, timedelta,timezone
import pytz, time
from django.views.decorators.csrf import csrf_exempt
import logging
from logging.handlers import RotatingFileHandler
import haversine as hs
from pathlib import Path
from itertools import islice
from dateutil.parser import parse
import calendar

# from dateutil.tz import tzutc

from dateutil import tz as tzz

import datetime as datet

from haversine import Unit

import plotly.graph_objects as go
from plotly import offline

import haversine as hs
import os
import psycopg2
from django.db.models import Q
from django.db import close_old_connections
from plotly.offline import plot
from plotly.graph_objs import Scatter
import plotly.express as px
from plotly import graph_objs as go
import pandas as pd
import folium
import plotly.offline as opy
import math
from math import pi,atan2,sqrt
from django.conf import settings
import itertools


# Create your views here.
@login_required(login_url='/Staff/login_auth/')
def home(request):
    return render(request, 'Staff/index.html')

def login_auth(request):
        return render(request, 'Staff/login.html')

#authentication

def login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    screenwidth = request.POST.get('screenwidth')

    obj = Staff.objects.filter(email = username, password = password)
    ct = obj.count()
    #print("yest"+str(ct))
    for i in obj:
        vals = i.email
        pwd = i.password
        name=i.name
        staff_ID = i.id
        custid = i.customer_id
        objC = Customers.objects.get(id = custid)
        company = objC.Company

    if ct == 0 :
        messages = "Invalid username/password"
        return render(request, 'Staff/login.html',{'messages':messages})
    else:
        request.session.clear_expired()
        if username == vals and password == pwd:
            request.session['staff_username'] = name +" ("+i.staff_category+")"
            request.session['staff_ID'] = staff_ID
            request.session['custid'] = custid
            request.session['company'] = company   
            request.session['staff_category'] = i.staff_category       
            request.session['screenwidth'] = screenwidth              
            
            return trackers(request)
        else :
            messages = "Invalid username/password"
            return render(request, 'Staff/login.html',{'messages':messages})
#-----------------------------------------------------------------------------------------

tz = pytz.timezone('Asia/Calcutta')
timestamps = datetime.now(tz)

def logout(request):
    tz = pytz.timezone("Asia/Calcutta")
    now = datetime.now(tz)
    if 'staff_username' in request.session:
        
        del request.session['staff_username']
    return HttpResponseRedirect('/Staff/')
#-----------------------------------------------------------------------------------------

def setup_logger(request, logger_name, log_file, level=logging.DEBUG):
    tz = pytz.timezone("Asia/Calcutta")
    now = datetime.now(tz)
    log_setup = logging.getLogger(logger_name)
    while log_setup.hasHandlers():
    	log_setup.removeHandler(log_setup.handlers[0])

    formatter = logging.Formatter(str(now)+'- [%(name)s] - Path :-['+request.get_full_path()+'] - [%(levelname)s] - %(message)s', datefmt='%d-%m-%Y %H:%M:%S %p %Z')
    logging.Formatter.converter = time.localtime
    fileHandler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=1*1024*1024, backupCount=1)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    log_setup.setLevel(level)
    log_setup.addHandler(fileHandler)
    log_setup.addHandler(streamHandler)
#-----------------------------------------------------------------------------------------

def logger(msg, level, logfile):
    print(logfile)
    if logfile == '1'   : log = logging.getLogger('1')
    if logfile == '0'   : log = logging.getLogger('0') 
    print(log.handlers)
    if level == 'info'    : log.info(msg) 
    if level == 'warning' : log.warning(msg)
    if level == 'error'   : log.error(msg)
    if level == 'debug'   : log.error(msg)    	
#------------------------------JSON DATA-----------------------------------------------------------
   

import json, time
from django.utils import timezone

@csrf_exempt
def insertData(request):
   
    path = "logs/SAMlog.log"
    isFile = os.path.isfile(path)
    #print("File exitsts", isFile)
    setup_logger(request, "1", path)
    #print(request.method,"Method")
    stf = 0
    
    json_body = json.loads(request.body.decode("utf-8"))
    #print("***Capability: ",json_body)
    capability_body = json_body.get('Capabilities')
    #print("***JSON_BODY:::::::::",capability_body)
    
    #capability message
    if "Capabilities" in str(json_body):
        if "GFRID" in str(capability_body):
            GFRIDs = capability_body['GFRID']
            code = GFRIDs['Code']
            if code =="GFR":
                if 'Id' in GFRIDs:
                    HardwareId = GFRIDs['Id']
                    hwSelect = GFRID.objects.filter(hardware_Id = HardwareId)
                    for i in hwSelect:
                        print("TRACKER:::::::",i.tracker)
                        tracker = i.tracker
                        recordInfo = i.DataRecoInfo
                else:           
                    mess = "Please verify the JSON keys and try again"
                    logger(mess, 'warning', "1")
                    Notification.objects.create(tracker="Error", message = mess, priority="High")
                    logger(json_body, 'info', "1")
                    return JsonResponse(mess, safe=False)
            else:           
                    mess = "Please check the code"
                    logger(mess, 'warning', "1")
                    Notification.objects.create(tracker="Error", message = mess, priority="High")
                    logger(json_body, 'info', "1")
                    return JsonResponse(mess, safe=False)
           
            if tracker: 
                path = "logs/"+tracker+".log"
                isFile = os.path.isfile(path)
                setup_logger(request, str(stf), path)
                logger('Capability '+" ("+str(json_body)+")", 'info', str(stf))

                try:
                    my_record = GFRID.objects.get(tracker = tracker)
                    GFRIDID  = my_record.id
                    vid = my_record.version_id

                    myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
                    MyCustomer = Customers.objects.get(id= myCust.customerID)
                    new_DB = MyCustomer.new_database
                    custcode = MyCustomer.Customer_Identifier
                    conn1_new = ""
                    if new_DB == True:
                        conn = createNewDB(connection, custcode)
                        conn1_new = conn
                    else:
                        conn1_new = connection  
                       
                except GFRID.DoesNotExist:
                    GFRIDID  = 0
                                              
                if GFRIDID >0:

                    #version
                    ver = Version.objects.get(id = vid)
                    vcod = ver.code
                    slst = ver.sensors
                    SimInfos = capability_body['SimInfo']
                    IMEA = SimInfos['IMEA']
                    simdet = SimInfo.objects.filter(GFRIDID = GFRIDID, IMEA = IMEA)
                    objcount = simdet.count()

                    if objcount >0  or IMEA == "None":
                           
                        SAMVer = GFRIDs['SAMver']
                        SWver = GFRIDs['SWver']     
                        LRT = capability_body['LRT']
                        tbleLTName = vcod.lower()+"_dataacqcfg"
                        tbleLT = checkTableExists(conn1_new, tbleLTName.lower())
                                           
                        Encryption = capability_body['Encryption']
                        SQN = capability_body['SQN']
                        TS = capability_body['TS']            
                        PS = capability_body['PowerSource']  
                        Source = PS['Source']
                        GFRSWCAPS_T = capability_body['GFRSWCAPS']  
                        GPSGF_T = GFRSWCAPS_T['GPSGF']
                        
                        # checking system health
                        if tbleLT == True:
                            with conn1_new.cursor() as cursor:
                                                            
                                qryLT ="select DATAINTR from "+tbleLTName+" where gfrid = "+str(GFRIDID)
                                cursor.execute(qryLT)
                                conn1_new.commit()
                                if cursor.rowcount >0:
                                    Dint = cursor.fetchone()
                                    DATAINTR = Dint[0]
                                else:
                                    DATAINTR = 0
                                print(DATAINTR)
                                conn1_new.close()

                        sysHth = System_Health.objects.filter(GFRID = GFRIDID, system_offtime__isnull = False, system_ontime__isnull = True )
                        sysH = sysHth.first()
                        print(sysHth.count(),"SYSTEM")
                        tz = pytz.timezone("Asia/Calcutta")
                        if sysHth.count() > 0:
                            #latest = System_Health.objects.latest(GFRID = GFRIDID, system_offtime__isnull = False, system_ontime__isnull = True )
                            latest =sysHth.first()
                            print("OFFTIME , ONTIME::: ",latest.system_offtime,latest.system_ontime)
                            if latest.system_offtime > int(LRT):
                                System_Health.objects.filter(id = sysH.id).update(system_ontime = TS, last_modified = datetime.now(tz))
                                print("tetstets",datetime.now(tz))
                            else:
                                System_Health.objects.filter(id = sysH.id).update(system_ontime = TS, system_offtime = LRT, last_modified = datetime.now(tz))
                                print("tetstets222222222", datetime.now(tz))
                            
                        else:
                            #valid LRT
                            ontime = time.strftime('%Y-%m-%d', time.localtime(int(LRT)))
                            datetime_object = datetime.strptime(ontime, '%Y-%m-%d')
                            todays_date = datetime.today()
                            print(datetime_object.year, todays_date.year)
                            if datetime_object.year >= todays_date.year: 
                                System_Health.objects.create(GFRID = GFRIDID, system_ontime = TS, system_offtime = LRT)
                                Alerts.objects.filter(GFRID = GFRIDID,status=1).update(status=0,TS_OFF_BigInt = LRT)
                            else:
                                System_Health.objects.create(GFRID = GFRIDID, system_ontime = TS)   
                           
                        
                        # checking system health
                        
			
                        GFRID.objects.filter(id = GFRIDID).update(SAMVer= SAMVer, SWver= SWver, hardware_Id= HardwareId, Encryption= Encryption, SQN = SQN, TS= TS, PowerSource=Source)
                      
                        #Geofence
                        GCaps = capability_body['GFRSWCAPS']
                        if GCaps:
                            GPSGF = GCaps['GPSGF']
                        
                       
                        #GFRSENSORCAPS
                        if SQN == str(1):
                            sendet = Sensors.objects.all()
                            #sensors
                            SenCaps = capability_body['GFRSENSORCAPS']
                            if len(SenCaps) > 0:
                                for s in sendet:
                                    s_id = s.id
                                     
                                    ff = list(eval(slst))
                                     
                                    tblename = vcod+"_"+ s.code
                                    statusData = vcod+"_statusdata"
                                    if s_id in ff:
                                        
                                        sid = SenCaps[s.code]['SID']
                                         
				
                                        #status = SenCaps[s.code]['Status']
                                        poststat = SenCaps[s.code]['POST']
                                        
                                           
                                        SVDatas = SensorVersion.objects.get(sensors = s.id, version = vid)
                                        print("SVDatas.sid", SVDatas.sid)
		
                                        if str(SVDatas.sid) == str(sid):
                                            # updating the sis, status and poststatus from JSON
                                            print("Testing vid")
                                            testV = SensorVersion.objects.get(sensors = s.id, version = vid)
                                            testVStatus = testV.status

                                            if testVStatus == "ENABLED":
                                                sensors = slst.strip("]").strip("[").split(",")
                                                conn1_new = ""
                                                if new_DB == True:
                                                    conn = createNewDB(connection, custcode)
                                                    conn1_new = conn
                                                else:
                                                    conn1_new = connection

                                                with conn1_new.cursor() as cursor:
                                                    
                                                        qry ="UPDATE "+statusData+" SET  poststatus = '"+str(poststat)+"' where gfrid = "+str(GFRIDID)+" AND sensor = "+str(s_id)
                                                        print(qry)
                                                        cursor.execute(qry)
                                                        conn1_new.commit()
                                                tble = checkTableExists(conn1_new, tblename.lower())

                                                if tble == False:
                                                    
                                                    mess = "Database table '"+tblename+"' is not available with this version ("+vcod+"). Please add and try again"
                                                    Notification.objects.create(tracker=tracker, message = mess, priority="High")
                                                    #logger(mess+" ("+tracker+")", 'info', str(stf))

                                                    return JsonResponse(mess, safe=False)
                                                    break
                                                else:
                                                    tblefields = checkTableFieldExists(conn1_new, tblename.lower(), SenCaps)
                                                    if tblefields == "0":
                                                        mess = "Some fields are missing in database table "+tblename.lower()+". Please verify and try again"
                                                        Notification.objects.create(tracker=tracker, message = mess, priority="Medium")
                                                        #logger(mess+" ("+tracker+")", 'info', str(stf))                    
                                                        return JsonResponse(mess, safe=False)
                                                        break
                                                    else:

                                                        if "GPS" in tblename:
                                                                if GPSGF_T == "Disabled":
                                                                    vl = "DISABLED"
                                                                else:
                                                                    vl = "ENABLED"
                                                                with conn1_new.cursor() as cursor:
                                                        
                                                                    #print(s.id)
                                                                    qry ="UPDATE "+statusData+" SET  geofence = '"+str(vl)+"' where gfrid = "+str(GFRIDID)+" AND sensor = "+str(s.id)
                                                                    #print(qry)
                                                                    cursor.execute(qry)   
                                                                    conn1_new.commit()

                                                                SensorVersion.objects.filter(sensors = s.id).update(geofence = vl)
                                                        elif "SIM" in tblename:
                                                            #print(tblename)
                                                            tz = pytz.timezone("Asia/Calcutta")
                                                            with conn1_new.cursor() as cursor:
                                                                sel = "select * from "+tblename.lower()+" where gfrid = "+str(GFRIDID)
                                                                cursor.execute(sel)  
                                                                conn1_new.commit()
                                                                if cursor.rowcount>0:
                                                                    qry ="UPDATE "+tblename.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(GFRIDID) 
                                                                    #print(qry)
                                                                    cursor.execute(qry)   
                                                                    conn1_new.commit()
                                                                else:
                                                                    inst1 = "insert into "+tblename.lower()+"(gfrid, last_modified) values("+str(GFRIDID)+",'"+str(datetime.now(tz))+"')"
                                                                    #print(inst1)
                                                                    cursor.execute(inst1) 
                                                                    conn1_new.commit()  
      
                                        else:
                                               
                                            emess = "Stored Sensor id not matching with JSON sensor id. Ref Sensor code  is :"+s.code+" (Tracker :- "+tracker+") and version "+vcod
                                            #logger(emess, 'info', str(stf))
                                            Notification.objects.create(tracker=tracker, message = emess, priority="High")
                           
                                            return JsonResponse(emess, safe=False)
                                            break

                        msgdict = {"Code": "GFR", "Id": HardwareId, "MSGTYPE":"DATA", "STATUS":"SUCCESS"}
                        msgmagicinfo = { "CODE": "GFR", "ID": HardwareId, "CMD": "DATARECINFO", "STRING" : recordInfo}
                        logger("All messages are verfied and waiting for the data"+" ("+tracker+")", 'info', str(stf))                    
                        return JsonResponse(msgmagicinfo, safe=False)
                                               
                        #return JsonResponse("Updated", safe=False)
                    else:
                        mess = "Sim Information is not available in Database. Please update the database and try to insert data"
                        Notification.objects.create(tracker=tracker, message = mess, priority="Medium")
                        #print(stf)
                        logger(mess +" ("+tracker+")", 'info', str(stf))
                        return JsonResponse(mess, safe=False)
                       
                else:
                    mess = "Tracker Information is not available in Database. Please update the database and try to insert data"
                    Notification.objects.create(tracker=tracker, message = mess, priority="Medium")
                    logger('Tracker Information is not in Database. Please update database and try to insert data '+" ("+tracker+")", 'info', str(stf))
                    return JsonResponse("Tracker Information is not in Database. Please update database and try to insert data", safe=False)
            else:
                mess = "Sofware Details are not created in server for this Hardware ID"
                Notification.objects.create(tracker="Error", message = mess, priority="Medium")
                logger(mess, 'warning',"1")
                return JsonResponse(mess, safe=False)
        else:
            mess = "Please verify the JSON keys and try again"
            Notification.objects.create(tracker="Error", message = mess, priority="Medium")
            logger(mess, 'warning', "1")
            return JsonResponse(mess, safe=False)

    #-----------------------------------------------------------------------------------------------------------------------------------------------------                                    
    elif "SYSCONFIG" in str(json_body):
        #SysConfig records
        #print("!!!!!!JSON_BODY FROM DATA: ",json_body)
        records = json_body['SYSCONFIG']   
            
        #checking the hardware id 
        if 'Id' in records:
            HardwareId = records['Id']
            #print("HarwareId:::::: ",HardwareId)
            configVer = records['VER']
            hwSelect = GFRID.objects.get(hardware_Id = HardwareId)
            #print("hwselect:::::::",hwSelect.tracker)
            tracker = hwSelect.tracker
        else:
            mess = "Please verify the JSON keys and try again"
            #print(mess)
            Notification.objects.create(tracker="Error", message = mess, priority="High")
            logger(mess, 'warning', "1")
            logger(json_body, 'info', "1")
            return JsonResponse(mess, safe=False)

        if tracker:
            path = "logs/"+tracker+".log"
            setup_logger(request, str(stf), path)
                           
            logger('SysConfig '+" ("+str(json_body)+")", 'info', str(stf))
            try:
                my_record = GFRID.objects.get(tracker = tracker)
                GFRIDID  = my_record.id
                con_VER = my_record.config_ver
                vid = my_record.version_id

                myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
                MyCustomer = Customers.objects.get(id= myCust.customerID)
                new_DB = MyCustomer.new_database
                custcode = MyCustomer.Customer_Identifier
                #version
                ver = Version.objects.get(id = vid)
                vcod = ver.code
                configtablst = ver.configtabs
                slst = ver.sensors
                slst = slst.replace ("[","")
                slst = slst.replace ("]","")
                slst = slst.replace (",","")
                conn1_new = ""
                if new_DB == True:
                    conn = createNewDB(connection, custcode)
                    conn1_new = conn
                else:
                    conn1_new = connection

                slstStr = list(slst.split(" "))
                
                for s in slstStr:
                    #print(s)
                    snes = Sensors.objects.get(id = s)
                    if "SIM" in snes.code:
                        SIMTB = vcod+"_"+snes.code
                        tz = pytz.timezone("Asia/Calcutta")
                        with conn1_new.cursor() as cursor:
                            sel = "select * from "+SIMTB.lower()+" where gfrid = "+str(GFRIDID)
                            cursor.execute(sel)  
                            conn1_new.commit()

                            if cursor.rowcount>0:
                                qry ="UPDATE "+SIMTB.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(GFRIDID) 
                                #print(qry)
                                cursor.execute(qry)   
                                conn1_new.commit()
                            else:
                                inst1 = "insert into "+SIMTB.lower()+"(gfrid, last_modified) values("+str(GFRIDID)+",'"+str(datetime.now(tz))+"')"
                                #print(inst1)
                                cursor.execute(inst1) 
                                conn1_new.commit()  

            except GFRID.DoesNotExist:
                GFRIDID  = 0     


            contabs = ConfigTab.objects.all()

            if str(con_VER) != str(configVer):
                if GFRIDID>0:
                    TS = records['TS']
                    jsfld = None
                    GFRID.objects.filter(id = GFRIDID).update(config_ver = configVer, fullJSON = jsfld)
                    ConfigData = records['HATCFG']
                    ConfigData = lower_keys(ConfigData)
                    
                    for tbs in contabs:
                        ids = tbs.id
                        tblename = vcod+"_"+ tbs.configtab
                        if str(ids) in configtablst:
                            #print("Test")
                            flushDatas(conn1_new, tblename.lower(), GFRIDID)
                            kval = tbs.configtab.upper()
                            #print(kval, len(ConfigData[kval]))
                            if kval in ConfigData:
                                #print(kval)
                                insertConfigValues(conn1_new, tblename.lower(), ConfigData[kval], GFRIDID)  
                                
                    logger("Version is different and Updated in database", 'info', str(stf))                                                         
                    return JsonResponse("Updated", safe=False)    
            else:
                #for tbs in contabs:
                    #ids = tbs.id
                    #tblename = vcod+"_"+ tbs.configtab
                    #if str(ids) in configtablst:
                        #print("Test")
                        #flushUpdates(conn1_new, tblename.lower(), GFRIDID)

                
                mess = "No changes in Configuration"
                logger(mess, 'info', str(stf))                                                         
                return JsonResponse(mess, safe=False)
        else:
            mess = "Sofware Details are not created in server for this Hardware ID"
            Notification.objects.create(tracker="Error", message = mess, priority="Medium")
            logger(mess, 'warning',"1")
            return JsonResponse(mess, safe=False)
    #-------------------------------------------------------------------------------------------------------------------------
    elif "ALERTMSG" in str(json_body):
            alert_body = json_body.get('ALERTMSG')

            #Alert records
            #print("!!!!!!JSON_BODY FROM DATA: ",alert_body)
            HardwareId = alert_body['Id']
            #print("HarwareId:::::: ",HardwareId)

            hwSelect = GFRID.objects.get(hardware_Id = HardwareId)
            #print("hwselect:::::::",hwSelect.tracker)
            
            tracker = hwSelect.tracker
            if tracker:
                path = "logs/"+tracker+".log"
                setup_logger(request, str(stf), path)
                logger('Alert '+" ("+str(json_body)+")", 'info', str(stf))                                 
            my_record = GFRID.objects.get(tracker = tracker)
            GFRIDID  = my_record.id
            DATARECINFO = alert_body['DATARECINFO']
            GFRID.objects.filter(id = GFRIDID).update(DataRecoInfo= DATARECINFO)
            alertrecords = alert_body['ALERTINFO']    #need to check if the name of the array is coming as 'Alert'
            alertVal = alertrecords['ALERT']
            timestamp = alertrecords['TS']
            status = alertrecords['STATUS']
            jsonFile = alertrecords
            print("!!!!!!!ALERTS:::::",alertVal,GFRIDID)             

            # timestamp and today's datetime for checking if TS in json is valid
            ontime = time.strftime('%Y-%m-%d', time.localtime(int(timestamp)))
            datetime_object = datetime.strptime(ontime, '%Y-%m-%d')
            todays_date = datetime.today()
            print(datetime_object.year, todays_date.year)
            #-------------------------------------------------------------------

            tz = pytz.timezone('Asia/Calcutta')
            times = datetime.fromtimestamp(int(timestamp)).astimezone(tz).strftime('%Y-%m-%d %H:%M:%S.%f')      
            print("HexVal: ", str(alertVal), type(str(alertVal)))
            if alertVal != '0xFFFFFFFF':
                records = AlertNotification.objects.get(hexVal = str(alertVal))
                if records:
                    print("got something!")
                    
                else:
                    print("got nothing") 
            if alertVal == '0x00001000':
                GFRID.objects.filter(id=GFRIDID).update(PowerLineAlert=status)    
            print(GFRID,str(alertVal),timestamp,jsonFile)
            if alertVal == '0xFFFFFFFF':
                if status == '0':
                    print("Clear message! ")    
                    Alerts.objects.filter(GFRID=GFRIDID,status=1).update(status=0,TS_OFF_BigInt=timestamp)
                if status == "1":
                    print("Ignore this message for now!")    
            else:    
                alertExists = Alerts.objects.filter(GFRID=GFRIDID, alert=str(alertVal), status=1) 
                if alertExists: 
                        if status == '1':  #if alert with status 1 is in the json message
                            print("This Alert "+alertVal+" already exists!")
                        else: #if alert with status 0 is in the json message, reset the alert in db 
                            
                            if datetime_object.year >= todays_date.year: 
                                Alerts.objects.filter(GFRID=GFRIDID,alert=str(alertVal),status=1).update(status=status,TS_OFF_BigInt=timestamp)
                                print("This Alert "+alertVal+" is reset in DB with status 0!")

                else:  
                        if datetime_object.year >= todays_date.year: 
                            Alerts.objects.create(GFRID=GFRIDID, alert=str(alertVal), TS=times, jsonFile=jsonFile, TS_OFF=None, alertNotify_id=records.id, status=status, TS_BigInt=timestamp, TS_OFF_BigInt=None)
                            
          
            return JsonResponse("Alert message coming in and updated in DB!!!", safe=False)
    #----------------------------------------------------------------------------------------------------------------------------  
    elif "SensorDataRecord" in str(json_body):

                #data record
                print("!!!!!!JSON_BODY FROM DATA: ",json_body)
           
                records = json_body['SensorDataRecord']
             
                 #  tracker = GFRIDs['Id']
                if 'Id' in records:
                    HardwareId = records['Id']
                    print("HarwareId:::::: ",HardwareId)

                    hwSelect = GFRID.objects.get(hardware_Id = HardwareId)
                    print("hwselect:::::::",hwSelect.tracker)
                    tracker = hwSelect.tracker
                else:
                    mess = "Please verify the JSON keys and try again"
                    print(mess)
                    Notification.objects.create(tracker="Error", message = mess, priority="High")
                    logger(mess, 'warning', "1")
                    return JsonResponse(mess, safe=False)
                    
                if tracker:
                    path = "logs/"+tracker+".log"

                    setup_logger(request, str(stf), path)
                           
                    logger('Sensor Data '+" ("+str(json_body)+")", 'info', str(stf))
                    try:
                        my_record = GFRID.objects.get(tracker = tracker)
                        GFRIDID  = my_record.id
                        vid = my_record.version_id

                        myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
                        MyCustomer = Customers.objects.get(id= myCust.customerID)
                        new_DB = MyCustomer.new_database
                        custcode = MyCustomer.Customer_Identifier

                        #version
                        ver = Version.objects.get(id = vid)
                        vcod = ver.code
                        slst = ver.sensors

                    except GFRID.DoesNotExist:
                        GFRIDID  = 0
                       
                    if GFRIDID>0:
                        SQN = records['SQN']
                        TS = records['TS']
                        DATARECINFO = records['DATARECINFO']
                        RECHARGE = records['RECHARGE']
                        SQValue = records['GSMSQ']
                        GFRID.objects.filter(id = GFRIDID).update(DataRecoInfo= DATARECINFO, RECHARGE=RECHARGE)
                        GSMSQL.objects.create(GFRID= GFRIDID, SQValue=SQValue, TS = TS)
                                                
                        senDetail = Sensors.objects.all()
                        #sensors
                        senData = records['DataRecord']
                        for t in range(0, len(senData)):
                            for s in senDetail:
                                    ids = s.id
                                    tblename = vcod+"_"+ s.code
                                    conn1_new = ""
                                    if new_DB == True:
                                        conn = createNewDB(connection, custcode)
                                        conn1_new = conn
                                    else:
                                        conn1_new = connection

                                    if "SIM" in tblename:
                                        tz = pytz.timezone("Asia/Calcutta")
                                        with conn1_new.cursor() as cursor:
                                            sel = "select * from "+tblename.lower()+" where gfrid = "+str(GFRIDID)
                                            cursor.execute(sel)  
                                            conn1_new.commit()
                                            if cursor.rowcount>0:
                                                qry ="UPDATE "+tblename.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(GFRIDID) 
                                                print(qry)
                                                cursor.execute(qry)  
                                                conn1_new.commit() 
                                            else:
                                                inst1 = "insert into "+tblename.lower()+"(gfrid, last_modified) values("+str(GFRIDID)+",'"+str(datetime.now(tz))+"')"
                                                print(inst1)
                                                cursor.execute(inst1)   
                                                conn1_new.commit()

                                    if str(ids) in slst:
                                        sid = senData[t].get('SID')
                                        print("TESTESTE", sid, s.id, t, senData)
                                        SVDatass = SensorVersion.objects.filter(sensors = s.id, version = vid)  
                                        if  SVDatass.count()>0:  
                                        	SVDatas  =   SVDatass.first()           
                                        	testVStatus = SVDatas.status
                                        else:
                                        	continue
                                        print(testVStatus)

                                        if str(SVDatas.sid) == str(sid):    

                                            if testVStatus == "ENABLED":
                                                statusData = vcod+"_statusdata"
                                                with conn1_new.cursor() as cursor:
                                                    
                                                        qry ='SELECT poststatus FROM '+statusData+' WHERE gfrid = '+str(GFRIDID)+' and sensor = '+str(ids)
                                                        cursor.execute(qry)
                                                        conn1_new.commit()

                                                        testStatus = cursor.fetchone()
                                                        testS = str(testStatus).strip("(").strip(")").strip("'").strip("',")

                                                        if testS == 'ACTIVE':
                                                                insertFieldValues(conn1_new, tblename.lower(), senData[t], GFRIDID)                                                                    
                                                        else:
                                                                print("Sensor ",ids ," IS INACTIVE")
                                            

                                        else:
                                            mess = "Sensor("+s.code+") are not linked with this Tracker"+tracker+"."
                                            Notification.objects.create(tracker=tracker, message = mess, priority="Low")
                                            #logger(mess +" ("+tracker+")", 'info', str(stf))
                                            
                    logger("Sensor Data Updated" +" ("+tracker+")", 'info', str(stf))                       
                    return JsonResponse("Updated", safe=False)   
                else:
                    mess = "Sofware Details are not created in server for this Hardware ID"
                    Notification.objects.create(tracker="Error", message = mess, priority="Medium")
                    logger(mess, 'warning',"1")
                    return JsonResponse(mess, safe=False)                     
 #-------------------------------------------------------------------------------------------------------------------------    
    #---------------------------------------------------------------------------------------------------------------------------
    elif "CFGRESP" in str(json_body):
        records = json_body['CFGRESP']   
            
        #checking the hardware id 
        if 'Id' in records:
            HardwareId = records['Id']
            #print("HarwareId:::::: ",HardwareId)
            MSGTYPE = records['MSGTYPE']
            STATUS = records['STATUS']
            hwSelect = GFRID.objects.get(hardware_Id = HardwareId)
            tracker = hwSelect.tracker
        else:
            mess = "Please verify the JSON keys and try again"
            print(mess)
            Notification.objects.create(tracker="Error", message = mess, priority="High")
            logger(mess, 'warning', "1")
            logger(json_body, 'info', "1")
            return JsonResponse(mess, safe=False)

        if tracker:
            path = "logs/"+tracker+".log"
            setup_logger(request, str(stf), path)
                           
            logger('CFGRESP '+" ("+str(json_body)+")", 'info', str(stf))
            

            GFRIDID = hwSelect.id
            vid = hwSelect.version_id
            fulljson = hwSelect.fullJSON
            if STATUS == "SUCCESS":
                logger('Updation - SUCCESS'+" (json"+str(fulljson)+")", 'info', str(stf))
            else:
                logger('Updation - Failed'+" (json"+str(fulljson)+")", 'info', str(stf))

            jsfld = None
            GFRID.objects.filter(id = GFRIDID).update(fullJSON = jsfld, gereq_flag = 0)

            #customer
            myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
            MyCustomer = Customers.objects.get(id= myCust.customerID)
            new_DB = MyCustomer.new_database
            custcode = MyCustomer.Customer_Identifier
            #version
            ver = Version.objects.get(id = vid)
            vcod = ver.code
            configtablst = ver.configtabs
            slst = ver.sensors

            conn1_new = ""
            if new_DB == True:
                conn = createNewDB(connection, custcode)
                conn1_new = conn
            else:
                conn1_new = connection

            senDetail = Sensors.objects.all()
            for s in senDetail:
                if str(s.id) in slst:
                    if "SIM" in s.code:
                        SIMTB = vcod+"_"+s.code
                        tz = pytz.timezone("Asia/Calcutta")
                        with conn1_new.cursor() as cursor:
                            sel = "select * from "+SIMTB.lower()+" where gfrid = "+str(GFRIDID)
                            cursor.execute(sel)  
                            conn1_new.commit()

                            if cursor.rowcount>0:
                                qry ="UPDATE "+SIMTB.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(GFRIDID) 
                                #print(qry)
                                cursor.execute(qry)   
                                conn1_new.commit()
                            else:
                                inst1 = "insert into "+SIMTB.lower()+"(gfrid, last_modified) values("+str(GFRIDID)+",'"+str(datetime.now(tz))+"')"
                                #print(inst1)
                                cursor.execute(inst1) 
                                conn1_new.commit()  

            
            configtabs = ConfigTab.objects.all()
            for c in configtabs:
                if str(c.id) in configtablst:
                    print(c.configtab)
                    tblename = vcod+"_"+c.configtab
                    flushUpdates(conn1_new, tblename.lower(), GFRIDID)
	    
            return JsonResponse("CFG Response Updated", safe=False)    

        else:
            mess = "Sofware Details are not created in server for this Hardware ID"
            Notification.objects.create(tracker="Error", message = mess, priority="Medium")
            logger(mess, 'warning',"1")
            return JsonResponse(mess, safe=False)

#---------------------------------------------------------------------------------------------------------------------------
    elif "CMDRESP" in str(json_body):
        records = json_body['CMDRESP']   
        stf = 0
        #checking the hardware id 
        if 'Id' in records:
            HardwareId = records['Id']
            #print("HarwareId:::::: ",HardwareId)
            STATUS = records['STATUS']
            CMD = records['CMD']
            hwSelect = GFRID.objects.get(hardware_Id = HardwareId)
            tracker = hwSelect.tracker
            GFRIDID = hwSelect.id
        else:
            mess = "Please verify the JSON keys and try again"
            print(mess)
            Notification.objects.create(tracker="Error", message = mess, priority="High")
            logger(mess, 'warning', "1")
            logger(json_body, 'info', "1")
            return JsonResponse(mess, safe=False)

        if tracker:
            path = "logs/"+tracker+".log"
            setup_logger(request, str(stf), path)
                           
            logger('CMDRESP '+" ("+str(json_body)+")", 'info', str(stf))
            
            #customer
            myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
            MyCustomer = Customers.objects.get(id= myCust.customerID)
            new_DB = MyCustomer.new_database
            custcode = MyCustomer.Customer_Identifier
            #version
            bv = Batches_Trackers.objects.get(id = myCust.batchId)
            vid = bv.version.id
            ver = Version.objects.get(id = vid)
            vcod = ver.code
            configtablst = ver.configtabs
            slst = ver.sensors

            conn1_new = ""
            if new_DB == True:
                conn = createNewDB(connection, custcode)
                conn1_new = conn
            else:
                conn1_new = connection

            senDetail = Sensors.objects.all()
            for s in senDetail:
                if str(s.id) in slst:
                    if "SIM" in s.code:
                        SIMTB = vcod+"_"+s.code
                        tz = pytz.timezone("Asia/Calcutta")
                        with conn1_new.cursor() as cursor:
                            sel = "select * from "+SIMTB.lower()+" where gfrid = "+str(GFRIDID)
                            cursor.execute(sel)  
                            conn1_new.commit()

                            if cursor.rowcount>0:
                                qry ="UPDATE "+SIMTB.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(GFRIDID) 
                                #print(qry)
                                cursor.execute(qry)   
                                conn1_new.commit()
                            else:
                                inst1 = "insert into "+SIMTB.lower()+"(gfrid, last_modified) values("+str(GFRIDID)+",'"+str(datetime.now(tz))+"')"
                                #print(inst1)
                                cursor.execute(inst1) 
                                conn1_new.commit()  
            
		
            GFRIDID = hwSelect.id
            vid = hwSelect.version_id
            fulljson = hwSelect.fullJSON
            if STATUS == "SUCCESS":
                logger('Updation - SUCCESS'+" (json"+str(records)+")", 'info', str(stf))
            else:
                logger('Updation - Failed'+" (json"+str(records)+")", 'info', str(stf))
            if CMD :
                objs = TestCommands.objects.all()
                print("CMD", CMD)
                for r in objs:
                    print("type", type(r.cmdjson))
                    cmddict = json.dumps(r.cmdjson)
                    if CMD in cmddict:
                        cmdid = r.id
                        TrackerCommands.objects.filter(commandID=cmdid, tracker=tracker).update(flag = 0, tester_flag = 0, cust_flag = 0, process_flag =1, process_time = str(datetime.now(tz)), last_modified = str(datetime.now(tz)))
            return JsonResponse("CMD - Updated", safe=False)    

        else:
            mess = "Sofware Details are not created in server for this Hardware ID"
            Notification.objects.create(tracker="Error", message = mess, priority="Medium")
            logger(mess, 'warning',"1")
            return JsonResponse(mess, safe=False)



#-------------------------------------------------------------------------------------------------------------

    else:
            #No capabilities tag in json
            print("!!!!!!JSON_BODY FROM DATA: ",json_body)
            mess = "Please verify json keys and try again"
            Notification.objects.create(tracker="Error", message = mess, priority="Medium")
            logger(mess, 'warning', "1")
            logger(json_body, 'warning', "1")
            return JsonResponse("Please verify json keys and try again", safe=False)        
   

#-------------------------------------------------------------------------------------
def insertConfigValues(dbcon, tablename, SenCaps, GFRIDID):
    tabl = tablename.split("_")
    print("Table Name",tablename)
    dbcur = dbcon.cursor()
    dbcur.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tablename+"' and table_schema = 'public'")
    dbcon.commit()
    LIST = dbcur.fetchall()
    LIST =  " ".join( repr(e) for e in LIST  )
    LIST = LIST.replace("(","")
    LIST = LIST.replace(")","")
    LIST = LIST.replace("'","")
    LIST = LIST.replace(" ","")
    LIST = LIST.rstrip(LIST[-1])
    LIS = LIST.split(",")
    LIS.remove("id")
    LIS.remove("jsonfield")
    flds = ", ".join(str(x) for x in LIS)
    gf  = GFRID.objects.get(id=GFRIDID)
    slst = gf.version.sensors
    
    if "sen" in tablename:

        print("Coonfigtaaaabsss",gf.version.sensors)
        sen =  Sensors.objects.all()
        for s in sen:
            ids = s.id      
            scod = s.code   
            if str(ids) in slst:
                sid = SenCaps.get(scod)
                vals1 = ""
                qry1 = "Select * from "+tablename+" where gfrid = "+str(GFRIDID) +" and  sensorid ="+str(ids)
                dbcur.execute(qry1)
                dbcon.commit()


                if dbcur.rowcount == 0:
                    for i in range(0, len(LIS)):
                        lstItem = LIS[i].upper()
                        print(lstItem)
                        if sid :
                            if lstItem =="GFRID":
                                if vals1:
                                    vals1 = vals1+ ", '"+str(GFRIDID)+"'"
                                else:
                                    vals1="'"+str(GFRIDID)+"'"
                                    
                            elif lstItem =="LAST_MODIFIED":
                                if vals1:
                                    vals1 = vals1+ ", '"+str(datetime.now())+"'"
                                else:
                                    vals1="'"+str(datetime.now())+"'"

                            elif lstItem =="SENSORID":
                                if vals1:
                                    vals1 = vals1+ ", '"+str(ids)+"'"
                                else:
                                    vals1="'"+str(ids)+"'"
                            
                            elif sid.get(lstItem):
                                if vals1:
                                    vals1 = vals1+", '"+sid.get(lstItem)+"'"
                                else:
                                    vals1 = "'"+sid.get(lstItem)+"'"
                    print("vals1",vals1, lstItem)
                    if vals1:
                        inst1 = "insert into "+tablename+"("+str(flds)+") values("+vals1+")"
                        print(inst1)
                        dbcur.execute(inst1)
                        dbcon.commit()
                else:
                    upval = ""
                    for i in range(0, len(LIS)):
                        lstItem = LIS[i].upper()
                        print(lstItem,"LSTItem")
                        if sid :
                            if lstItem =="LAST_MODIFIED":
                                if upval:
                                    upval = upval+ ", "+lstItem+ "='"+str(datetime.now())+"'"
                                else:
                                    upval = lstItem+"='"+str(datetime.now())+"'"
                            elif sid.get(lstItem):
                                if upval:
                                    upval = upval+", "+lstItem+ "='"+sid.get(lstItem)+"'"
                                else:
                                    upval = lstItem+"='"+sid.get(lstItem)+"'"
                    print("upval",upval)
                    if upval:
                        qry = "UPDATE "+tablename+" set "+upval+" WHERE gfrid = "+str(GFRIDID)+" and sensorid ="+str(ids)
                        print(qry)
                        dbcur.execute(qry)
                        dbcon.commit()
                        
            #statusdata
            ststSDqry = "select sensor from "+gf.version.code+"_statusdata where gfrid = "+str(GFRIDID)
            dbcur.execute(ststSDqry)
            ststSDlist = dbcur.fetchall()
            for i in ststSDlist:
                sensor = i[0]
                selSDqry = "select status from "+tablename +" WHERE gfrid = "+str(GFRIDID)+" and sensorid ="+str(sensor)
                dbcur.execute(selSDqry)
                ststSDstat = dbcur.fetchone()
                if ststSDstat:
                    stSTAT = ststSDstat[0]
                    if stSTAT.strip() == "ENABLED":
                        stSTAT1 = "ACTIVE"
                    else:
                        stSTAT1 = "INACTIVE"
                    print("last", sensor, ststSDstat[0])
                    upqry = "UPDATE "+gf.version.code+"_statusdata SET poststatus = '"+str(stSTAT1)+"' WHERE gfrid = "+str(GFRIDID)+" and sensor = "+str(sensor)  
                    print(upqry)
                    dbcur.execute(upqry)
                    dbcon.commit()

        return True
    else:
        #print(LIS)

        val = ""
        for i in range(1, len(LIS)):
            lstItem = LIS[i].upper()

            if "LAST_MODIFIED" in lstItem:
                if val:
                    val = val+ "'"+str(datetime.now())+"'"
                else:
                    val="'"+str(datetime.now())+"'"

            elif SenCaps.get(lstItem):
                
                if val:
                    if type(SenCaps.get(lstItem)) == list:
                    	listvalss = ','.join(str(x) for x in SenCaps.get(lstItem)) 
                    	val = val+", '["+listvalss+"]'"
                    else:
                    	val = val+", '"+SenCaps.get(lstItem)+"'"
                else:
                    if type(SenCaps.get(lstItem)) == list:
                    	listvalss = ','.join(str(x) for x in SenCaps.get(lstItem)) 
                    	val = "'["+listvalss+"]'"
                    else:
                    	val = "'"+SenCaps.get(lstItem)+"'"
            else:
                if val:
                    val = val+", Null"
                else:
                    val = "Null"
        val = str(GFRIDID)+", "+val
        #print("****************LIST::: ",LIST)
        #print("**************VAL::::",val)
        inst = ""
        dbcur.execute("SELECT * FROM public."+tablename+" where gfrid = "+str(GFRIDID))
        ct = dbcur.rowcount
        if ct >0:
            dbcur.execute("DELETE FROM public."+tablename+" where gfrid = "+str(GFRIDID))
        
        inst = "insert into "+tablename+"("+str(flds)+") values("+val+")"
        print("RESULT::::::",inst)
        ch = insertTable(dbcon, inst)
        if ch == True:
            #GFConfig- update geofenceupdate
            if "gf" in tablename:
                sen =  Sensors.objects.filter(code__contains='GPS')
                for s in sen:
                    ids = s.id      
                    print("sensor", ids)

                dbcur.execute("SELECT gfstatus FROM public."+tablename+" where gfrid = "+str(GFRIDID))
                gfstatus = dbcur.fetchone()[0]
                upqry = "UPDATE "+gf.version.code+"_statusdata SET geofence = '"+str(gfstatus)+"' WHERE gfrid = "+str(GFRIDID)+" and sensor = "+str(ids)  
                dbcur.execute(upqry)
                dbcon.commit()
                print(gfstatus, "GEOFENCWEEEEEEE")

        return ch
        #return ch
    
#-----------------------------------------------------------------------------------
def lower_keys(x):
    if isinstance(x, list):
        return [lower_keys(v) for v in x]
    elif isinstance(x, dict):
        return dict((k.upper(), lower_keys(v)) for k, v in x.items())
    else:
        return x
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
def checkTableExists(dbcon, tablename):
    dbcur = dbcon.cursor()
    dbcur.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format(tablename.replace('\'', '\'\'')))
    if dbcur.fetchone()[0] == 1:
        #print("hello")
        dbcur.close()
        return True

    dbcur.close()
    return False

#-----------------------------------------------------------------------------------------
def checkTableFieldExists(dbcon, tablename, SenCaps):
    tabl = tablename.split("_")
    print(len(tabl))
    if len(tabl) > 2:
    	ttab = tabl[1]+"_"+tabl[2]
    else:
    	ttab = tabl[1]
 
   
    dbcur = dbcon.cursor()
    dbcur.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tablename+"'")
    LIST = dbcur.fetchall()
    LIST =  " ".join( repr(e) for e in LIST  )
    LIST = LIST.replace("(","")
    LIST = LIST.replace(")","")
    LIST = LIST.replace("'","")
    LIST = LIST.rstrip(LIST[-1])
    LIS = LIST.split(",")
   
    if "units" not in tablename: 

        for i in range(3, len(LIS)):
            lt = LIS[i].lstrip().upper()
            if lt in str(SenCaps[ttab.upper()]) or lt=="TS" or lt=="ID":
                return 1
            else:    
                return 0
#-----------------------------------------------------------------------------------------                      
def insertFieldValues(dbcon, tablename, SenCaps, GFRIDID):
    tabl = tablename.split("_")
   
    dbcur = dbcon.cursor()
    dbcur.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tablename+"' ORDER BY ORDINAL_POSITION")
    LIST = dbcur.fetchall()
    LIST =  " ".join( repr(e) for e in LIST  )
    LIST = LIST.replace("(","")
    LIST = LIST.replace(")","")
    LIST = LIST.replace("'","")
    LIST = LIST.rstrip(LIST[-1])
    LIS = LIST.split(",")
    LIST = LIST[LIST.startswith("id, ") and len("id, "):]
    print(LIST, "LISTSTSTSTSS", LIS, type(LIS))
    print("TABLE NAMES*******: ",tablename)
    val = ""
    lstmod = ""
    for i in range(2, len(LIS)):
        na = LIS[i].upper().lstrip()
        print(na, "LISSSS")
        if "mpu" in tablename:
                acc = SenCaps.get('Accelerometer')
                gcc = SenCaps.get('Gyroscope')
                mag = SenCaps.get('Magnetometer')
                accX = acc.get('ACC_X')
                accY = acc.get('ACC_Y')
                accZ = acc.get('ACC_Z')
                print("VALUES FROM JSON: ",na)
                if "LAST_MODIFIED" in na:
                    if val:
                        val = val+"'"+str(datetime.now())+"'"
                    else:
                        val="'"+str(datetime.now())+"'"

                elif na in acc:
                    #print(acc.get(na))
                    accVal = mpuVals(acc.get(na), na)
                   
                    if val:
                        val = val+", '"+str(accVal)+"'"
                    else:
                        val = "'"+str(accVal)+"'"

                elif na in gcc:
                    #print(gcc.get(na))
                    gccVal = mpuVals(gcc.get(na), na)
                   
                    if val:
                        val = val+", '"+str(gccVal)+"'"
                    else:
                        val = "'"+str(gccVal)+"'"

                elif na in mag:
                    #print(mag.get(na))
                    magVal = mpuVals(mag.get(na), na)
                   
                    if val:
                        val = val+", '"+str(magVal)+"'"
                    else:
                        val = "'"+str(magVal)+"'"
                       
                else:
                    print(SenCaps.get(na))
                    if "TEMP" in na:
                        tempVal = mpuVals(SenCaps.get(na), na)
                       
                        if val:
                            val = val+", '"+str(tempVal)+"'"
                        else:
                            val = "'"+str(tempVal)+"'"

                    elif "TS" in na:
                        if val:
                            val = val+", '"+SenCaps.get(na)+"'"
                        else:
                            val = "'"+SenCaps.get(na)+"'"
       
                    elif 'ROLL' in na:
                        print("VALSSS:",accX,accY,accZ)
                       
                        roll = 180 * atan2(float(accY), sqrt(float(accX)*float(accX)+ float(accZ)*float(accZ)))/pi
                        print("VALS TILL NOW:::",val)
                       
                        if val:
                            val = val+", '"+str(roll)+"'"
                        else:
                            val="'"+str(roll)+"'"         
                        print("VAL:::",val)     

                    elif 'PITCH' in na:
                        print("For pitch,VALSSS:",accX,accY,accZ)
                       
                        pitch = 180 * atan2( float(accX), sqrt( float(accY)*float(accY) + float(accZ)*float(accZ) ))/pi
                        print("VALS TILL NOW:::",val)
                       
                        if val:
                            val = val+", '"+str(pitch)+"'"
                        else:
                            val="'"+str(pitch)+"'"         
                        print("VAL:::",val)     
 
                    else:
                        if val:
                            val = val+", Null"
                        else:
                            val = "Null"

        elif "gps" in tablename:
            lat = SenCaps.get('LAT')
            lon = SenCaps.get('LON')
            print("VALUES FROM JSON: ",lat,lon)

               

            if "LAST_MODIFIED" in na:
                if val:
                    val = val+ "'"+str(datetime.now())+"'"
                else:
                    val="'"+str(datetime.now())+"'"
            elif "TS" in na:
                        if val:
                            val = val+", '"+SenCaps.get(na)+"'"
                        else:
                            val = "'"+SenCaps.get(na)+"'"
                           
            elif "LAT" in na:
                before, after = lat.split('.')
                direction = after[-1]
                print("LATT: ",before[:-2],before[-2:],after[:-1],direction)
                lat1 = before[-2:]+"."+after[:-1]
                print(type(lat1))
                print(float(before[:-2]))
                lat = (float(before[:-2])) + (float(lat1)/60)
                lat = '%.6f'%lat
                lat = str(lat)+direction
                print("LATTTTT:",lat)
                if val:
                    val = val + ", '"+str(lat)+"'"    
                else:
                    val="'"+lat+"'"

            elif "LON" in na:
                before, after = lon.split('.')
                direction = after[-1]
                print("LON: ",before[-2:],after[:-1],direction)
                lon1 = before[-2:]+"."+after[:-1]
                print(type(lon1))
                lon = (float(before[:-2])) + (float(lon1)/60)
                lon = '%.6f'%lon
                lon = str(lon)+direction
                print("LONNNNN:",lon)
                if val:
                    val = val + ", '"+str(lon)+"'"    
                else:
                    val="'"+lon+"'"                        
            else:
                        if val:
                            val = val+", Null"
                        else:
                            val = "Null"        
                            
        elif "lce" in tablename:
            wght = SenCaps.get('WGHT')
            print(wght, "WEIGTHSSSS")
            FW =  100000
            EW = 0
            estwegt = FW - EW


            if "LAST_MODIFIED" in na:
                if val:
                    val = val+ "'"+str(datetime.now())+"'"
                else:
                    val="'"+str(datetime.now())+"'"
            elif "TS" in na:
                        if val:
                            val = val+", '"+SenCaps.get(na)+"'"
                        else:
                            val = "'"+SenCaps.get(na)+"'"
                           
            elif "WGHTLIST" in na:
                if val:
                    val = val + ", '"+json.dumps(wght)+"'"    
                else:
                    val="'"+wght+"'"
            elif "PERWGHT" in na:
		
                sum_of_list = 0
                for i in range(len(wght)):
                    listval = float(wght[i])-EW
                    sum_of_list = sum_of_list + listval
                    
                avg = sum_of_list/len(wght)
                perwght = (avg /estwegt) * 100
                perwght = round(perwght, 2)
                print(perwght, "PERWGHT11111111111111")

                if val:
                    val = val + ", '"+str(perwght)+"'"    
                else:
                    val="'"+perwght+"'"

            elif "AVGWGHT" in na:

                sum_of_list = 0
                for i in range(len(wght)):
                    listval = float(wght[i])-EW                
                    sum_of_list = sum_of_list + listval
                avg = sum_of_list/len(wght) 
                avg = avg / 1000

               
                if val:
                    val = val + ", '"+str(avg)+"'"    
                else:
                    val="'"+avg+"'"
	                  
        else:
            if "LAST_MODIFIED" in na:
                if val:
                    val = val+ "'"+str(datetime.now())+"'"
                else:
                    val="'"+str(datetime.now())+"'"

            elif SenCaps.get(na):
                if val:
                    val = val+", '"+SenCaps.get(na)+"'"
                else:
                    val = "'"+SenCaps.get(na)+"'"
            else:
                if val:
                    val = val+", Null"
                else:
                    val = "Null"
    print("VAL:::::::::::::::",val)    
    val = str(GFRIDID)+", "+val
    print("****************LIST::: ",LIST)
    print("**************VAL::::",val)
    inst = "insert into "+tablename+"("+str(LIST)+") values("+val+")"
    print("RESULT::::::",inst)
    ch = insertTable(dbcon, inst)
    #return ch

def insertTable(dbcon, inst):

    with dbcon.cursor() as cursor:
        if inst:
            result = cursor.execute(inst)
            print("DB insert Result: ",result)
           
            dbcon.commit()
            #dbcon.close()
            return True

   
#------------------------------------------------------------------------------------------------------
#
# ------------------------------------------------------------------------------------------------------                    
def comments_update(request, tracker, custBTID):
    if 'staff_username' not in request.session:    
        return redirect('/Staff/login_auth/')
    else:    
        my_record = Cust_Batches_Trackers.objects.get(id=custBTID)
        #print (custBTID)
        if request.method == 'POST':
            form1 = UpdateForm(request.POST,instance=my_record)
            if form1.is_valid():
                form1.save()
                return redirect ('/Staff/trackers/')
            else:
                return render(request,
                   'Staff/update.html',
                    {
                        'form1': form1
                    })
        else:
            form1 = UpdateForm(instance=my_record)
            return render(request,
                        'Staff/update.html',
                        {
                            'form1': form1
                        })
#-----------------------------------------------------------------------------------------

#listing trackers assigned to logged in staff
#-----------------------------------------------------------------------------------------

def trackers(request):
    if 'staff_username' not in request.session:    
        return redirect('/Staff/login_auth/')
    else:        
        trackers = Cust_Batches_Trackers.objects.filter(staffID_id=request.session['staff_ID'])
        batches = Batches_Trackers.objects.all()
        customers = Customers.objects.all()
        GFRIDObj = GFRID.objects.all()
        return render(request, 'Staff/trackers.html',{'obj':trackers, 'batches':batches, 'customers':customers, 'GFRIDObj':GFRIDObj})
#-----------------------------------------------------------------------------------------

def SingleMapView(request, tracker, custBTID, custid):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId
        batches = Batches_Trackers.objects.get(id = batchID)
        veicode = batches.version.code
        gfrids = GFRID.objects.get(tracker=tracker)
        gfrid = gfrids.id
        SamVer = gfrids.SAMVer
        SWver = gfrids.SWver
        trkName = gfrids.tracker_name
                        
        tz = pytz.timezone("Asia/Calcutta")
        now = datetime.now(tz)
        now = str(now).replace("-","/")
        last3days = datetime.now(tz) - timedelta(days=3)
        todate = 0.0
        fromdate = 0.0
        print(now, last3days)
         
        datetimerange = None
        print("TIMEDATAAAAA: ",datetimerange)
        #return ch
        sens = Sensors.objects.filter(code__contains='SIM')
        sensLM =  sens.first()
        simatble = veicode+"_"+sensLM.code

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection
        
        with conn1_new.cursor() as cursor:
            qrys = "select last_modified from "+simatble+" where gfrid = "+str(gfrid)
            print(qrys)
            cursor.execute(qrys)
            conn1_new.commit()

            if cursor.rowcount>0:
                curDt = cursor.fetchone()[0]
            else :
                curDt = ""
    
            if curDt:
                LM_SIM = curDt
            else:
                LM_SIM = "Not available"

        sensL = Sensors.objects.filter(code__contains='LocalTerm')
        sensVal = []
        sensLVal = []
        for x in sens:
            sensVal = x.id
        for x in sensL:
            sensLVal = x.id
         
        LIS = []
        LIS1 = []
        LISTSss = SensorVersion.objects.filter(version=batches.version.id).order_by('ordering')
        for t in LISTSss:
            LIS.append(t.sensors)
            print("t.sensors", t.sensors)
        print(LIS, "LIS", len(LIS))
        for j in range(len(LIS)):
            print(j, LIS[j])
            sel1 = SensorShow.objects.get(tracker = tracker, sensorID = LIS[j])
            
            if sel1.status == 0:
               LIS1.append(LIS[j])
       
        
        LIS = [fruit for fruit in LIS if fruit not in LIS1]  
        print(LIS, "LIS NEW", len(LIS))  
        LIST = batches.version.sensors
        #print(type(LIST))

      
        SensNam = ["" for k in range(len(LIS))]
        SensTitle = ["" for k in range(len(LIS))]

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            objectsAll = GFRID.objects.filter(tracker = tracker)
            trackerData = objectsAll.last()
            objcount = objectsAll.count()
            gfrid = trackerData.id
            hwId = trackerData.hardware_Id
            #print("GFRID" +str(gfrid))

            for i in range(0, len(LIS)):
                #print(LIS[i].lstrip())
                ssv = Sensors.objects.get(id = LIS[i])
                code = veicode+"_"+ssv.code
                SensNam[i] = ssv.code
                SensTitle[i] = ssv.title
             
            if objcount > 0 :
                siminfo = SimInfo.objects.get(tracker = tracker)
                sensors = Sensors.objects.all()
                senVersions = SensorVersion.objects.filter(version=batches.version.id)                
                #print("objCount is greater than > 0")
                return render(request,
                            'Staff/maps.html',
                            {
                                'custid':custid, 'custBTID': custBTID, 'tracker':tracker,'hwId':hwId,'SamVer':SamVer, 'SWver':SWver, 'objects' :objects, 'batches':batches, 'trackerData':trackerData, 'sensors':sensors,
                                'siminfo':siminfo, 'range' : range(len(LIS)), 'lst': LIS, 'nam':SensNam, 'title':SensTitle, 'senVersions':senVersions, "SIM":LM_SIM, 'todate':todate,'fromdate':fromdate,
                                'version':veicode, 'scrwid':int(request.session['screenwidth']), 'gfrids':gfrids, 'trkName':trkName
                            })
            else:
                #print("objcount is 0")
                return redirect ('/Staff/trackers/')
        else:
            #print("GFRID table not exists!!")
            return redirect ('/Staff/trackers/')

#-----------------------------------------------------------------------------------------
def selectLastRecord(connection, inst):

    with connection.cursor() as cursor:
        if inst:
            result = cursor.execute(inst)
            print("DB Selected Result: ",result)
           
            connection.commit()
            connection.close()
            return true

   
#------------------------------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------------------
                     

def viewDatalist(request,version, tracker, custBTID, custid):
    print("VERSION: ",version)
    senVer = Version.objects.get(code=version)
    print(senVer.id)
    objects = SensorVersion.objects.filter(version = senVer.id).order_by('id')
    sensLst = []
    for each in objects:
        sensLst.append(each.sensors)
    print(sensLst)    
    return render(request, 'Staff/viewData.html',{
                            'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'objects' :objects
                        })

#-----------------------------------------------------------------------------------------


def maps(request, tracker, custBTID, custid, sensorname, sensorid):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        custObjts = Cust_Batches_Trackers.objects.get(id=custBTID)

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            objectsAll = GFRID.objects.filter(tracker = tracker)
            objects = objectsAll.last()
            vcode = objects.version.code
            objcount = objectsAll.count()
            values = ["" for i in range(4)]

            if objcount > 0 :
                #customer lat long
                objCust = Customers.objects.get(id = custid)
                new_DB = objCust.new_database
                custcode = objCust.Customer_Identifier

                conn1_new = ""
                if new_DB == True:
                    conn = createNewDB(connection, custcode)
                    conn1_new = conn
                else:
                    conn1_new = connection

                custlat = objCust.Lat
                custlon = objCust.Lon
                values[0] =  [custlat, custlon, "customer", "GeoFence"]
	
                if "gps" in sensorname.lower():
                    selectqry = "Select * from "+sensorname.lower()+" where gfrid="+str(objects.id)+" ORDER BY id DESC"
                    selectFDqry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+sensorname.lower()+"'  and table_schema = 'public'"
                    dbf= GetFieldValues(conn1_new,selectFDqry, sensorname.lower())
                    dbv= selectDBValues(conn1_new,selectqry, sensorname.lower())
                    out_arr1 = np.asarray(dbf)
                    out_arr2 = np.asarray(dbv)
                    if dbv and dbf:
                        for i in range(len(out_arr1)):
                            print(out_arr1[i])
                            if 'lat' in out_arr1[i]:
                                lat1 = out_arr2[i].rstrip()
                                lat = lat1[:-1]

                            elif 'lon' in out_arr1[i]:
                                lon1 = out_arr2[i].rstrip()
                                lon = lon1[:-1]
                            elif 'last_modified' in out_arr1[i]:
                                print("Hi")
                                lastmod = out_arr2[i]

                        tdet = tracker+" Reach here at " +str(lastmod)
                        values[1] = [lat, lon, tdet, "GeoFence"]
                        senV = sensorid.split("_")
                        cursor = conn1_new.cursor()
                        
                        cursor.execute("Select geofence from "+vcode.lower()+"_statusdata where gfrid="+str(objects.id)+" and sensor="+str(senV[0])+" ORDER BY id DESC")
                        gfBool = cursor.fetchone()
                        print("Gefence Val",gfBool[0])
                        if gfBool[0].strip() == "ENABLED":
                            vers = Version.objects.get(code = vcode)
                            strng = vers.configtabs
                            ctabList1 = strng.strip("]").strip("[").split(",")
                            ctabList = ["" for k in range(len(ctabList1))] 
                            k = 0
                            for item in ctabList1:
                                print(item.lstrip())
                                cf = ConfigTab.objects.get(id=item.lstrip())
                                if "GF" in cf.configtab.upper():
                                    print("Yes its there")
                                    cursor.execute("Select * from "+vcode.lower()+"_"+cf.configtab+" where gfrid="+str(objects.id))
                                    records = cursor.fetchone()
                                    if cursor.rowcount>0:
                                        columnNames = [column[0] for column in cursor.description]
                                        #print(type(columnNames )  , columnNames) 
                                        #print(type(records), records)
                                        zip_iterator = zip(columnNames, records)
                                        a_dictionary = dict(zip_iterator)
                                        slat = str(a_dictionary.get('slat')).strip()
                                        slat = slat[:-1]
                                        slong = str(a_dictionary.get('slong')).strip()
                                        slong = slong[:-1]
                                        sgfr = str(a_dictionary.get('sgfr')).strip()

                                        wlat = str(a_dictionary.get('wlat')).strip()
                                        wlat = wlat[:-1]
                                        wlong = str(a_dictionary.get('wlong')).strip()
                                        wlong = wlong[:-1]
                                        wgfr = str(a_dictionary.get('wgfr')).strip()
                                        if str(a_dictionary.get('gfstatus')).strip()=="ENABLED":
                                            values[2] = [slat, slong , " Source Location", sgfr ]
                                            values[3] = [wlat, wlong , " Work Location", wgfr ]
                                        #print(values[2])
                    if  values[1]:
                        print(values)
                        return JsonResponse(values, safe=False)
                    else:
                        return JsonResponse("", safe=False)
                    #end of customer lat long
                #end of customer lat long
            else:
                return redirect ('/Staff/trackers/')
        else:
            return redirect ('/Staff/trackers/')                
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

def mapsview(request):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:

        objects1 = Cust_Batches_Trackers.objects.filter(staffID_id=request.session['staff_ID'])
        values = ["" for i in range(objects1.count() +1)]
        objCust = Customers.objects.get(id = request.session['custid'])
        custlat = objCust.Lat
        custlon = objCust.Lon
        values[0] =  [custlat, custlon, "test"]
       
        j=1
        for i in objects1:
            tble = checkTableExists(connection, "GPS")
            if tble == True:
                objectsAll = GPS.objects.filter(custBTID=i.id)
                objects = objectsAll.last()
                if(objects):
                    lat = objects.LatCL
                    lon = objects.LonCL
                    tracker = objects.tracker
                    custBTID = str(i.id)
                    sim_no = objects.sim_no
                    custid =  str(request.session['custid'])
                    tdet = objects.tracker+" Reach here at " +str(objects.time_stamp)+"  <a href='/Staff/viewmap/"+tracker+"/"+custBTID+"/"+"/"+custid+"/'>Click Here to View</a>"
                    values[j] = [lat, lon, tdet]
                    j =j+1
             
        return JsonResponse(values, safe=False)

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
def filter_map(request, tracker, sensor, custBTID, custid):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:

        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)    
     
       
        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            objectsAll = GFRID.objects.filter(tracker = tracker)
            trackerData = objectsAll.last()
            objcount = objectsAll.count()
            if objcount > 0 :
                return render(request,
                            'Staff/filter_map.html',
                            {
                                'custBTID': custBTID,'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'trackerData':trackerData, 'datetimerange':"None"
                            })
            else:
                return redirect ('/Staff/trackers/')
        else:
                return redirect ('/Staff/trackers/')

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

@csrf_exempt
def filter_histo(request,tracker, sensor, custBTID, custid):

    tracker = tracker
    sensor = sensor
    custBTID = custBTID
    custid =  custid
    ver = GFRID.objects.get(tracker=tracker).version_id
    verCode = (Version.objects.get(id=ver).code).lower()
    
    sensorname = sensor
    tblename = verCode.lower()+"_"+sensorname.lower()

    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id
    
    j =1
    
    dt_range = request.POST.get('datetimes')

    global yr
    global mn
    global hr
    if not dt_range:
        dt_string = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y %b %d %H:%M:%S')
        dt_string3 = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=3)).strftime('%Y %b %d %H:%M:%S')
        
        dt_range = f"{dt_string} - {dt_string3}"
        dates = dt_range.split("-")
        # print("DATERANGE:: ",dates)
        s = dates[0]
        
        e = dates[1]
        
        dt_ef = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M:%S')
        dt_sf = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=3)).strftime('%Y-%m-%d %H:%M:%S')

        dt_e = datetime.strptime(dt_ef, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))
        dt_s = datetime.strptime(dt_sf, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

        start_date = dt_s.replace(tzinfo=timezone.utc).timestamp()
        end_date = dt_e.replace(tzinfo=timezone.utc).timestamp()

        cursor = connection.cursor()

        cursor.execute(f"SELECT * FROM {tblename} WHERE gfrid = {str(gfrid)} AND  (ts BETWEEN  {int(start_date)} AND {int(end_date)}) ORDER BY id")
        dbv = cursor.fetchall()
        
        values = ["" for i in range(len(dbv) +1)]

        x_ts = []
        n=1

        coord = []
        ts = []

        x_year = []
        x_month = []
        x_hour = []

        histoSum = []

        distance = []
        y_dist = []
        m_dist = []
        h_dist = []

        yr1 = []
        yr_ll = [] 

        mn1 = []
        mn_ll = []

        hr1 = []
        hr_ll = []

        l_t_s = []
        yl_t_s = []
        hl_t_s = []

        for p in dbv:
            lat = p[3].rstrip()
            lat = lat[:-1]
            lon = p[4].rstrip()
            lon = lon[:-1]
            
            time = p[5]
        
            timestamp = datetime.fromtimestamp(int(time),tzz.tzutc()).strftime('%Y %b %d %H:%M:%S')

            x_ts.append(str(timestamp))
            tdet = tracker+" Reach here at " +(str(timestamp))
            values[j] = [lat, lon, tdet,timestamp]
            j =j+1

        # print(x_ts)
        
        for val in values[1:]:
            if val != "":
                coord.append((float(val[0]),float(val[1])))

                ts.append(val[3])
                n = n+1
            
    ############### Datetime_graph #################
        
        i = 0

        for i in range(len(coord)-1):
            
            distance.append(hs.haversine(coord[i],coord[i+1],unit = Unit.METERS))
            i = i+1
            histoSum.append(sum(distance))
        print(distance)
        # print(x_ts)
        # print(len(x_ts))

    ############### Year_graph #################
        ztc = zip(x_ts,coord)
        l_ztc = list(ztc)

        for tc in range(len(l_ztc)):
            if datetime.strptime(l_ztc[tc][0],'%Y %b %d %H:%M:%S').year not in yr1:
                yr1.append(datetime.strptime(l_ztc[tc][0],'%Y %b %d %H:%M:%S').year)
        
        yr = list(zip(*(iter(yr1),)))

        for yr_l in range(len(yr)):
            for y_tc in range(len(yr[yr_l])):
                for yy_tc in range(len(l_ztc)):
                    if f"{datetime.strptime(l_ztc[yy_tc][0],'%Y %b %d %H:%M:%S').year}":
                        yr_ll.append(l_ztc[yy_tc][1])
            yr[yr_l] += tuple(yr_ll)
            yr_ll.clear()
        # print(yr)

        for j_y in range(len(yr)):
            yl_t_s.append(len(yr[j_y]))
            for j_yy in range(1,len(yr[j_y])-1,1):
                y_dist.append(hs.haversine(yr[j_y][j_yy],yr[j_y][j_yy+1]))
        Input_y = iter(y_dist)
        ylength_to_split = yl_t_s
        yOutput = [list(islice(Input_y, elem))for elem in ylength_to_split]

        yres = list(map(sum, yOutput))
        # print(yres)
        
        for y_l in range(len(yr)):
            x_year.append(yr[y_l][0])
        # print(x_year)

    # ########### Month_graph ################

        for tc_m in range(len(l_ztc)):
            if f"{calendar.month_abbr[datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}" not in mn1:
                mn1.append(f"{calendar.month_abbr[datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}")
                
        mn = list(zip(*(iter(mn1),)))
        # print(mn)
        # print(len(l_ztc))

        for mn_l in range(len(mn)):
            for m_tc in range(len(mn[mn_l])):
                for mm_tc in range(len(l_ztc)):
                    if f"{calendar.month_abbr[datetime.strptime(l_ztc[mm_tc][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}" == mn[mn_l][m_tc]:
                        mn_ll.append(l_ztc[mm_tc][1])
            mn[mn_l] += tuple(mn_ll)
            mn_ll.clear()

        for j_m in range(len(mn)):
            l_t_s.append(len(mn[j_m]))
            for j_mm in range(1,len(mn[j_m])-1,1):
                m_dist.append(hs.haversine(mn[j_m][j_mm],mn[j_m][j_mm+1]))
        Input = iter(m_dist)
        length_to_split = l_t_s
        Output = [list(islice(Input, elem))for elem in length_to_split]

        print(l_t_s)
        # print(len(m_dist))
        # print(len(mn))
        # print(mn_ll)
        # print(mn)

        res = list(map(sum, Output))
        print(res)
        
        for m_l in range(len(mn)):
            x_month.append(mn[m_l][0])
        # print(x_month)
            
    # ########### Hour_graph ################

        for tc_h in range(len(l_ztc)):
            if f"{datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00" not in hr1:
                hr1.append(f"{datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00")
        # print(len(hr1))

        hr = list(zip(*(iter(hr1),)))

        for hr_l in range(len(hr)):
            # print(hr[hr_l])
            for h_tc in range(len(hr[hr_l])):
                for hh_tc in range(len(l_ztc)):
                    if f"{datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00" == hr[hr_l][h_tc]:
                        hr_ll.append(l_ztc[hh_tc][1])

            hr[hr_l] += tuple(hr_ll)
            hr_ll.clear()

        # print(l_ztc)
        # print(len(hr))

        for j_h in range(len(hr)):
            hl_t_s.append(len(hr[j_h])-1)
            for j_hh in range(1,len(hr[j_h])-1,1):
                h_dist.append(hs.haversine(hr[j_h][j_hh],hr[j_h][j_hh+1],unit = Unit.METERS))
        hInput = iter(distance)
        hlength_to_split = hl_t_s
        hOutput = [list(islice(hInput, helem))for helem in hlength_to_split]

        hres = list(map(sum, hOutput))
        print(hres)
        
        for h_l in range(len(hr)):
            x_hour.append(hr[h_l][0])
        # print(x_hour)

    ######################################

        traceA = go.Bar(x=x_month,y=res,width=0.3)
        traceB = go.Bar(x=x_year,y=yres,visible=False,width=0.3)
        traceC = go.Bar(x=x_hour,y=hres,visible=False,width=0.3)
        traceD = go.Bar(x=x_ts,y=distance,visible=False)
        data = [traceA,traceB,traceC,traceD]

    # Add dropdown
        updatemenus=list([
            dict(
                type = "buttons",
                direction = "down",
                yanchor = "bottom",
                buttons=list([dict(label='Month', method='update',
                                                args=[{'visible': [True,False,False,False]},
                                                    {'title': 'Month chart', 'yaxis': {'title':'Distance(kilometer)'}}]), # add layout to args
                                        
                                        dict(label='Year', method='update',
                                                args=[{'visible': [False,True,False,False]},
                                                    {'title': 'Year chart', 'yaxis': {'title':'Distance(kilometer)'}}]), # add layout to args

                                        dict(label='Hour', method='update',
                                                args=[{'visible': [False,False,True,False,]},
                                                    {'title': 'Hour chart', 'yaxis': {'title':'Distance(meter)'}}]), # add layout to args
                                        
                                        dict(label='Datetime', method='update',
                                                args=[{'visible': [False,False,False,True]},
                                                    {'title': 'Datetime chart', 'yaxis': {'title':'Distance(meter)'}}]), # add layout to args
                                        ]),)
        ])

        layout = dict(title='Month Chart', title_x=.5,
                yaxis_title='Distance(kilometer)', 
                updatemenus=updatemenus)
        if (len(x_ts)==0):
            tr = True
        else:
            tr = False

    # # plot
        fig = dict(data=data,layout=layout)
        fig1 = go.Figure(fig)
        fig1.update_xaxes(tickangle=-90)  
        return render(request, "Staff/plotly.html",context={'empty': offline.plot((fig1),output_type='div'),'alert_flag': tr,'dt':f"Date Range Selected : {dt_string3} - {dt_string}"})
    
    dates = dt_range.split("-")
    print("DATERANGE:: ",dates)
    s = dates[0]
    t =  dates[0].strip()
    start = t.replace("/","-")
    # drs = datetime.strftime(t, '%Y %b %d %H:%M:%S')
    last = datetime.strptime(start, '%Y %b %d %H:%M:%S').astimezone(pytz.timezone("UTC"))
    
    start_date = last.replace(tzinfo=timezone.utc).timestamp()
    print(int(start_date))
    e = dates[1]
    nw = dates[1].strip()
    end = nw.replace("/","-")
    # dre = datetime.strftime(nw, '%Y %b %d %H:%M:%S')
    now = datetime.strptime(end, '%Y %b %d %H:%M:%S').astimezone(pytz.timezone("UTC"))
    
    end_date = now.replace(tzinfo=timezone.utc).timestamp()
    print(int(end_date))
    
    cursor = connection.cursor()

    cursor.execute(f"SELECT * FROM {tblename} WHERE gfrid = {str(gfrid)} AND  (ts BETWEEN  {int(start_date)} AND {int(end_date)}) ORDER BY id")
    dbv = cursor.fetchall()
    
    values = ["" for i in range(len(dbv) +1)]

    x_ts = []
    n=1

    coord = []
    ts = []

    x_mnth = []
    x_year = []
    x_month = []
    x_hour = []

    histoSum = []

    distance = []
    y_dist = []
    m_dist = []
    h_dist = []

    yr1 = []
    yr_ll = []
    # global yr

    mn1 = []
    mn_ll = []
    # global mn

    hr1 = []
    hr_ll = []
    # global hr

    l_t_s = []
    yl_t_s = []
    hl_t_s = []

    for p in dbv:
        lat = p[3].rstrip()
        lat = lat[:-1]
        lon = p[4].rstrip()
        lon = lon[:-1]
        
        time = p[5]

        timestamp = datetime.fromtimestamp(int(time),tzz.tzutc()).strftime('%Y %b %d %H:%M:%S')

        x_ts.append(str(timestamp))
        tdet = tracker+" Reach here at " +(str(timestamp))
        values[j] = [lat, lon, tdet,timestamp]
        j =j+1

    # print(x_ts)
    
    for val in values[1:]:
        if val != "":
            coord.append((float(val[0]),float(val[1])))

            ts.append(val[3])
            n = n+1

    # print(coord)
    # print(len(coord))
############### Datetime_graph #################
    
    i = 0

    for i in range(len(coord)-1):
        
        distance.append(hs.haversine(coord[i],coord[i+1],unit = Unit.METERS))
        i = i+1
        histoSum.append(sum(distance))
    print(distance)
    # print(x_ts)
    # print(len(x_ts))

############### Year_graph #################
    ztc = zip(x_ts,coord)
    l_ztc = list(ztc)

    for tc in range(len(l_ztc)):
        if datetime.strptime(l_ztc[tc][0],'%Y %b %d %H:%M:%S').year not in yr1:
            yr1.append(datetime.strptime(l_ztc[tc][0],'%Y %b %d %H:%M:%S').year)
    
    yr = list(zip(*(iter(yr1),)))

    for yr_l in range(len(yr)):
        for y_tc in range(len(yr[yr_l])):
            for yy_tc in range(len(l_ztc)):
                if f"{datetime.strptime(l_ztc[yy_tc][0],'%Y %b %d %H:%M:%S').year}":
                    yr_ll.append(l_ztc[yy_tc][1])
        yr[yr_l] += tuple(yr_ll)
        yr_ll.clear()
    # print(yr)

    for j_y in range(len(yr)):
        yl_t_s.append(len(yr[j_y]))
        for j_yy in range(1,len(yr[j_y])-1,1):
            y_dist.append(hs.haversine(yr[j_y][j_yy],yr[j_y][j_yy+1]))
    Input_y = iter(y_dist)
    ylength_to_split = yl_t_s
    yOutput = [list(islice(Input_y, elem))for elem in ylength_to_split]

    yres = list(map(sum, yOutput))
    # print(yres)
    
    for y_l in range(len(yr)):
        x_year.append(yr[y_l][0])
    # print(x_year)

# ########### Month_graph ################

    for tc_m in range(len(l_ztc)):
        if f"{calendar.month_abbr[datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}" not in mn1:
            mn1.append(f"{calendar.month_abbr[datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}")
            
    mn = list(zip(*(iter(mn1),)))
    # print(mn)
    # print(len(l_ztc))

    for mn_l in range(len(mn)):
        for m_tc in range(len(mn[mn_l])):
            for mm_tc in range(len(l_ztc)):
                if f"{calendar.month_abbr[datetime.strptime(l_ztc[mm_tc][0],'%Y %b %d %H:%M:%S').month]} {datetime.strptime(l_ztc[tc_m][0],'%Y %b %d %H:%M:%S').year}" == mn[mn_l][m_tc]:
                    mn_ll.append(l_ztc[mm_tc][1])
        mn[mn_l] += tuple(mn_ll)
        mn_ll.clear()

    for j_m in range(len(mn)):
        l_t_s.append(len(mn[j_m]))
        for j_mm in range(1,len(mn[j_m])-1,1):
            m_dist.append(hs.haversine(mn[j_m][j_mm],mn[j_m][j_mm+1]))
    Input = iter(m_dist)
    length_to_split = l_t_s
    Output = [list(islice(Input, elem))for elem in length_to_split]

    print(l_t_s)
    # print(len(m_dist))
    # print(len(mn))
    # print(mn_ll)
    # print(mn)

    res = list(map(sum, Output))
    print(res)
    
    for m_l in range(len(mn)):
        x_month.append(mn[m_l][0])
    # print(x_month)
        
# ########### Hour_graph ################

    for tc_h in range(len(l_ztc)):
        if f"{datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00" not in hr1:
            hr1.append(f"{datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[tc_h][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00")
    # print(len(hr1))

    hr = list(zip(*(iter(hr1),)))

    for hr_l in range(len(hr)):
        # print(hr[hr_l])
        for h_tc in range(len(hr[hr_l])):
            for hh_tc in range(len(l_ztc)):
                if f"{datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%Y')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%b')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%d')} {datetime.strptime(l_ztc[hh_tc][0],'%Y %b %d %H:%M:%S').strftime('%H')}.00.00" == hr[hr_l][h_tc]:
                    hr_ll.append(l_ztc[hh_tc][1])

        hr[hr_l] += tuple(hr_ll)
        hr_ll.clear()

    # print(l_ztc)
    # print(len(hr))

    for j_h in range(len(hr)):
        hl_t_s.append(len(hr[j_h])-1)
        for j_hh in range(1,len(hr[j_h])-1,1):
            h_dist.append(hs.haversine(hr[j_h][j_hh],hr[j_h][j_hh+1],unit = Unit.METERS))
    hInput = iter(distance)
    hlength_to_split = hl_t_s
    hOutput = [list(islice(hInput, helem))for helem in hlength_to_split]

    hres = list(map(sum, hOutput))
    print(hres)
    
    for h_l in range(len(hr)):
        x_hour.append(hr[h_l][0])
    # print(x_hour)

######################################

    traceA = go.Bar(x=x_month,y=res,width=0.2)
    traceB = go.Bar(x=x_year,y=yres,visible=False,width=0.1)
    traceC = go.Bar(x=x_hour,y=hres,visible=False)
    traceD = go.Bar(x=x_ts,y=distance,visible=False)
    data = [traceA,traceB,traceC,traceD]

# Add dropdown
    updatemenus=list([
        dict(
            type = "buttons",
            direction = "down",
            yanchor = "bottom",
            buttons=list([dict(label='Month', method='update',
                                            args=[{'visible': [True,False,False,False]},
                                                {'title': 'Month chart', 'yaxis': {'title':'Distance(kilometer)'}}]), # add layout to args
                                    
                                    dict(label='Year', method='update',
                                            args=[{'visible': [False,True,False,False]},
                                                {'title': 'Year chart', 'yaxis': {'title':'Distance(kilometer)'}}]), # add layout to args

                                    dict(label='Hour', method='update',
                                            args=[{'visible': [False,False,True,False,]},
                                                {'title': 'Hour chart', 'yaxis': {'title':'Distance(meter)'}}]), # add layout to args
                                    
                                    dict(label='Datetime', method='update',
                                            args=[{'visible': [False,False,False,True]},
                                                {'title': 'Datetime chart', 'yaxis': {'title':'Distance(meter)'}}]), # add layout to args
                                    ]),)
    ])

    layout = dict(title='Month Chart', title_x=.5,
            yaxis_title='Distance(kilometer)', 
            updatemenus=updatemenus)
    if (len(x_ts)==0):
        tr = True
    else:
        tr = False

# # plot
    fig = dict(data=data,layout=layout)
    fig1 = go.Figure(fig)
    fig1.update_xaxes(tickangle=-90)  
    return render(request, "Staff/plotly.html",context={'empty': offline.plot((fig1),output_type='div'),'alert_flag': tr,'dt':f"Date Range Selected : {s} - {e}"})

#-----------------------------------------------------------------------------------------
def accPlotDetails(request, tracker, sensor, custBTID, custid, todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)
       
        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier
       

            
        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection
        #print("CONNECTION NAME******",conn1_new)
        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
         
            tbles = checkTableExists(conn1_new, tblename)
            if tbles == True:
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/accDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate,'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
#-------------------------------MPU Graphs----------------------------------------------------------
def accDetails(request, tracker, sensor, custBTID, custid, todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)
       
        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier
       

            
        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection
        
        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
         
            tbles = checkTableExists(conn1_new, tblename)
            if tbles == True:
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/accDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate,'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def bmpDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection
        print("CONNECTION NAME******",conn1_new)

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(conn1_new, tblename)
            if tbles == True:
                print("Trye")
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/bmpDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')
#---------------------------------------------------------------------------------------------------
def bmeDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(conn1_new, tblename)
            if tbles == True:
                print("Trye")
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/bmeDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def stcDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(connection, tblename)
            if tbles == True:
                print("Trye")
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/stcDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def vibDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(connection, tblename)
            if tbles == True:
                print("Trye")
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/vibDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def lcellliveDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier
        
        if todate == '0.0':
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz) + timedelta(days=1)

            last3days = datetime.now(tz) - timedelta(days=3)
            todate = str(int(now.timestamp()))
            fromdate =str(int(last3days.timestamp()))
            
            
        else :
            print("hello1")
            print("TOOOOOODATE: ",todate,type(todate))
            print("FROOOOOMDATE: ",fromdate)
            todates = todate.split(".")
            todate = todates[0]
            fromdates = fromdate.split(".")
            fromdate = fromdates[0]
         
	
        print("TODATTEEEE: ",todate)
        print("FROMMMMMMMMDATE: ",fromdate)

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode.lower()+"_"+sensor.lower()
            tblename = tblename
            print("tablename: ",tblename)

            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(connection, tblename)
            if tbles == True:
                print("Trye")
                lcelldet = "select id,gfrid,last_modified,perwght,avgwght,wghtlist,ts from "+tblename+" where gfrid="+str(gfrID)+" AND ts BETWEEN "+fromdate+" AND "+todate
        
              # selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(lcelldet)
                with conn1_new.cursor() as cursor:
                    if lcelldet:
                        cursor.execute(lcelldet)
                        results = cursor.fetchall()
                        xaxis = []
                        yaxis = []
                        for x in results:
                            print("printing value: ",x,x[6],x[3])
                            timestampss = datetime.fromtimestamp(int(x[6])).strftime('%Y-%m-%d %H:%M:%S %p')
			    
                            xaxis.append(timestampss)
                            if x[3] > 100:
                                yaxis.append(100)
                            elif x[3] < 0:
                                yaxis.append(0)
                            else:
                                yaxis.append(x[3])
                           
                         
                        conn1_new.commit()
                        conn1_new.close()

	

                        print("Final VAlues: ",xaxis,yaxis)
                        context = {
                                    "labels": xaxis,
                                    "values": yaxis,
                                    "gfrID":gfrID,
                                    "custBTID": custBTID, 
                                    "version":verCode, 
                                    "sensor":sensor, 
                                    "custid":custid, 
                                    "tracker":tracker, 
                                    "objects" :objects, 
                                    "batches":batches, 
                                    "todate":todate, 
                                    "fromdate":fromdate
                                }
                        return render(request,
                            'Staff/lcellliveDetails.html', context
                           )
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def lcellDetails(request, tracker, sensor, custBTID, custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        print("VERSIONCODE: ",verCode)

        objCust = Customers.objects.get(id = custid)
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            gfr = GFRID.objects.get(version_id= versionID,tracker = tracker )
            gfrID = gfr.id
            print("GFRID: ",gfrID)
            tblename = verCode+"_"+sensor
            tblename = tblename.lower()
            print("tablename: ",tblename)
            #MUST HAVE TO CHECK IF ANOTHER DB LATER
            tbles = checkTableExists(connection, tblename)
            if tbles == True:
                print("Trye")
                selt = "select * from "+tblename+" where gfrid = "+str(gfrID)
                print(selt)
                with conn1_new.cursor() as cursor:
                    if selt:
                        cursor.execute(selt)
                        results = cursor.fetchall()
                        for x in results:
                            print(x[3])
                        conn1_new.commit()
                        conn1_new.close()
                        return render(request,
                            'Staff/lcellDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':verCode, 'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })
            else:
                print("False")
                return redirect ('/Staff/trackers/')

        else:
                return redirect ('/Staff/trackers/')

#---------------------------------------------------------------------------------------------------
def AutoUpdate(request,gfrID,version,function,sensor,todate,fromdate):

    gfrid = GFRID.objects.get(id=gfrID)
    tracker = gfrid.tracker
    #customer
    cust = Cust_Batches_Trackers.objects.get(tracker=tracker)
    custid = cust.customerID
    custs = Customers.objects.get(id = custid)
    newdb = custs.new_database
    custcode = custs.Customer_Identifier
    if newdb == True:
        dcon = createNewDB(connection, custcode)
        #print("Test", dcon)
    else:
        dcon = connection
        #print("Check", dcon)
        
  #  datetimes = request.session['datetimes']
    if todate == '0.0':
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz)

            last3days = datetime.now(tz) - timedelta(days=3)
            todate = str(int(now.timestamp()))
            fromdate =str(int(last3days.timestamp()))
            print("TODATTEEEE: ",todate)
            print("FROMMMMMMMMDATE: ",fromdate)
            
    else :
            print("hello1")
            print("TOOOOOODATE: ",todate,type(todate))
            print("FROOOOOMDATE: ",fromdate)
            todates = todate.split(".")
            todate = todates[0]
            fromdates = fromdate.split(".")
            fromdate = fromdates[0]
         
    print("AUTOUPDATE")
    print("Sensor: ",sensor)
    print("VERS:",version)
    vertable = version.lower()+"_"+sensor.lower()
    print("table:::::::::::::::: ",vertable)
    if function == 'acc':
        qry = "select id,gfrid,acc_x,acc_y,acc_z,gyr_x,gyr_y,gyr_z,mag_x,mag_y,mag_z,temp,ts, last_modified from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
    elif function == 'bmp':
        qry = "select id,gfrid,temp,pres,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'    order by id"
    elif function == 'stc':
        qry = "select id,gfrid,voltage,current,temp,soc,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
    elif function == 'vibration':
        qry = "select id,gfrid,level,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
    elif function == 'bme':
        qry = "select id,gfrid,temp,pres,humid,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"' order by id"
    
    else:
        qry = "select id,gfrid,voltage,current,temp,soc,last_modified,ts from "+vertable+" where gfrid="+gfrID+" ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"        
    with dcon.cursor() as cursor:
            cursor.execute(qry)
            dcon.commit()
            objs = cursor.fetchall()
            json_data = []
            if function == 'acc':
                for obj in objs:
                    json_data.append({"id":obj[0], "gfrid":obj[1], "acc_x" : obj[2], "acc_y" : obj[3], "acc_z":obj[4], "gyr_x":obj[5], "gyr_y":obj[6], "gyr_z":obj[7], "mag_x":obj[8], "mag_y":obj[9], "mag_x":obj[10], "temp":obj[11], "ts":obj[12], "datetime":obj[13]})
                    print(obj[1],obj[0],obj[2],obj[3],obj[4])
            elif function == 'bmp':        
                for obj in objs:
                    json_data.append({"id":obj[0], "gfrid":obj[1], "temp" : obj[2], "pres" : obj[3], "datetime":obj[4], "ts":obj[5]})
                    print(obj[1],obj[0],obj[2],obj[3],obj[4])
            elif function == 'stc':
                for obj in objs:
                    json_data.append({"id":obj[0], "gfrid":obj[1], "voltage" : obj[2], "current" : obj[3],"temp":obj[4],"soc":obj[5], "datetime":obj[6], "ts":obj[7]})
                    print(obj[1],obj[0],obj[2],obj[3],obj[4])  
            elif function == 'vibration':
                for obj in objs:
                    json_data.append({"id":obj[0], "gfrid":obj[1], "vibration" : obj[2], "datetime":obj[3], "ts":obj[4]})
                    print(obj[1],obj[0],obj[2],obj[3],obj[4])  
            elif function == 'bme':        
                for obj in objs:
                    json_data.append({"id":obj[0], "gfrid":obj[1], "temp" : obj[2], "pres" : obj[3], "humidity" : obj[4], "datetime":obj[5], "ts":obj[6]})
                    print(obj[1],obj[0],obj[2],obj[3],obj[4],obj[5],obj[6])                
            else :    
                json_data = []       
    dcon.close()   
    return JsonResponse(json_data, safe=False)
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def multiline(request,function,gfrID,version,sensor,tracker,custBTID,custid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:

        print("GFRIDDDDD*****: ",gfrID)
        print("DATETIMERANGEEEEEEEE func: ",todate,fromdate)
        
        if function == 'acc':
            return render(request, 'Staff/multiline_acc.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'role':
            return render(request, 'Staff/rolegraph.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'pitch':
            return render(request, 'Staff/pitchgraph.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })                                
        elif function == 'gyr':
            return render(request, 'Staff/multiline_gyr.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'mag' :
            return render(request, 'Staff/multiline_mag.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'temp':
            return render(request, 'Staff/temp.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- MULTILINE: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/accDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })                 
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/accDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'datetimerange':datetimerange, 'todate':todate,'fromdate':fromdate
                            })                                                            
#-------------------------------------------------------------------------------------------------------------------------------------------------------

def bmeVal(request,function,gfrID,version,sensor,tracker,custBTID,custid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        if function == 'temp':
            return render(request, 'Staff/bme_temp.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'pres':
            return render(request, 'Staff/bme_pres.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'humidity':
            return render(request, 'Staff/bme_humidity.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- BME: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/bmeDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })                                   
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/bmeDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker,'accDet': results, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })                                                            
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def bmpVal(request,function,gfrID,version,sensor,tracker,custBTID,custid, todate, fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        if function == 'temp':
            return render(request, 'Staff/bmp_temp.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'pres':
            return render(request, 'Staff/bmp_pres.html',{
                            'gfrID':gfrID, 'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- BMP: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/bmpDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })                   
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/bmpDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'accDet': results, 'todate':todate, 'fromdate':fromdate
                            })                                                            
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def stcVal(request,function,gfrID,version,sensor,tracker,custBTID,custid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        if function == 'voltage':
            return render(request, 'Staff/stc_voltage.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'current':
            return render(request, 'Staff/stc_current.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'temp':
            return render(request, 'Staff/stc_temp.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })  
        elif function == 'soc':
            return render(request, 'Staff/stc_soc.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })                                  
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- STC: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/stcDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })   
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/stcDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'accDet': results, 'todate':todate, 'fromdate':fromdate
                            }) 
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def vibVal(request,function,gfrID,version,sensor,tracker,custBTID,custid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        if function == 'vibration':
            return render(request, 'Staff/vibration.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- VIBRATION: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/vibDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })                                 
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/vibDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })                                                            
#---------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def lcellVal(request,function,gfrID,version,sensor,tracker,custBTID,custid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        if function == 'lcell':
            return render(request, 'Staff/lcell.html',{
                            'gfrID':gfrID,'custBTID': custBTID,  'custid':custid, 'tracker':tracker,  'version' :version, 'sensor':sensor, 'todate':todate, 'fromdate':fromdate
                        })
        elif function == 'nosensor':
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            last3days = last3days.timestamp()
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            nowdate = nowdate.timestamp()
            print("type nowdate",type(nowdate),nowdate)
            print("type last3days",type(last3days),last3days)
            
            print("DATETIMERANGEEEEEE- VIBRATION: ",nowdate,last3days)
            
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/lcellDetails.html',
                            {
                                'gfrID':gfrID, 'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches,'todate':nowdate,'fromdate':last3days
                            })                                 
        else:
            objects = Cust_Batches_Trackers.objects.get(id = custBTID)
            batchID = objects.batchId
            batches = Batches_Trackers.objects.get(id = batchID)
            return render(request,
                            'Staff/lcellDetails.html',
                            {
                                'gfrID':gfrID,'custBTID': custBTID, 'version':version,'sensor':sensor, 'custid':custid, 'tracker':tracker, 'objects' :objects, 'batches':batches, 'todate':todate, 'fromdate':fromdate
                            })                                                            
#-------------------------------------------------------------------------------------------------------------------------------------------------------                                                   
#-------------------------------------------------------------------------------------------------------------------------------------------------------

@csrf_exempt
def filter_graphsview(request):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        sensorname = request.GET.get("sensor")
        version = request.GET.get("version")
        tblename = version.lower()+"_"+sensorname.lower()
        tracker = request.GET.get("tracker")
        gfr = GFRID.objects.get(tracker = tracker)
        gfrid = gfr.id  
        custBTID = request.GET.get('custBTID')
        custid =request.GET.get("custid")
        print("custBTID"+str(custBTID))
        print("DATE: ",request.GET.get('datetimerange'))
        datetimerange = request.GET.get('datetimerange')
        print(datetimerange)
       
        print(type(datetimerange))
        if datetimerange == "None":
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz)
            last3days = datetime.now(tz) - timedelta(days=3)
        else :
            print("hello1")
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            print ("NOW: ",nowdate)
        #print(type(last3days))
        print(last3days)
        objects1 = Cust_Batches_Trackers.objects.filter(staffID_id=request.session['staff_ID'])
        objCust = Customers.objects.get(id = request.session['custid'])
        custlat = objCust.Lat
        custlon = objCust.Lon
        j = 1

       
        new_DB = objCust.new_database
        custcode = objCust.Customer_Identifier

        conn1_new = ""
        if new_DB == True:    # CHECK IF ANOTHER DB
            conn = createNewDB(connection, custcode)
            conn1_new = conn
        else:
            conn1_new = connection
        print("CONNECTION NAME******",conn1_new)

        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            print("Table")
            objectsAll = GFRID.objects.filter(tracker = tracker)
            objects = objectsAll.last()
            vcode = objects.version.code
            objcount = objectsAll.count()
           
            j = 1
            print("objcount: ",objcount)
            if objcount > 0 :
                #customer lat long
                objCust = Customers.objects.get(id = custid)
                custlat = objCust.Lat
                custlon = objCust.Lon
                custTS = objCust.last_modified
                if "gps" in sensorname.lower():
                    selectqry = "SELECT * FROM "+tblename+" WHERE gfrid = "+str(gfrid)+" AND  last_modified BETWEEN  '"+ str(last3days) +"' AND '"+ str(nowdate)  +"' ORDER BY id"
                    selectFDqry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tblename+"'  and table_schema = 'public'"
                    dbf= GetFieldValues(conn1_new,selectFDqry, tblename)
                    dbv= selectDBFullValues(conn1_new,selectqry, tblename)
                    print("dBF: ",dbf)
                    print("dbv: ",dbv)

                    out_arr1 = np.asarray(dbf)
                    out_arr2 = np.asarray(dbv)
                    print("OUT_ARR1: ",out_arr1.size)
                    print("OUT_ARR2: ",out_arr2)
                    values = ["" for i in range(len(dbv) +1)]
                    #if dbv.size >0:
                    values[0] =  [custlat, custlon, "Location"]
                       
                    for p in dbv:
                            lat = p[3].rstrip()
                            lat = lat[:-1]
                            lon = p[4].rstrip()
                            lon = lon[:-1]
                            tdet = tracker+" Reach here at " +str(p[2])
                            timestamp = p[2]
                            values[j] = [lat, lon, tdet,timestamp]
                            j =j+1
                   



                   
   
                    print("VALUES:",values)
                    print("Count is :",j)
                    n=1
                    coord = []
                    ts = []
                    for val in values[1:]:
                       if val != "":
                            print("values>>>>: ",val)
                           
                            coord.append((float(val[0]),float(val[1])))
                             
                            ts.append(val[3])
                            n = n+1
                    print("Coord: ",coord)  
                    print("time: ",ts)
                    print("Coord length: ",len(coord))
                    i = 0
                    distance = []
                    for i in range(len(coord)-1):
                        print("coordinates: ",coord[i],coord[i+1])
                        distance.append(hs.haversine(coord[i],coord[i+1]))
                        print("DISTANCE: ",distance)
                        i = i+1
                    print(i)
                    print(distance)
                    histoSum = sum(distance)
                    print("SUMMM::",histoSum)
                    json_data = []
               
                    data = {
                    'distance': distance,
                    'timestamp':ts,
                                      
                    }
                    print("DATAAA: ",data)
                    result = [{'distance':c, 'timestamp':d} for c, d in zip(distance, ts)]
                    print("RESULTTTT : ",result)
                    print(type(result))
                    json_data.append(data)
                    print("Json",json_data)      
         
                return JsonResponse(result, safe=False)
#-----------------------------------------------------------------------------------------

def filter_mapsview(request):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        sensorname = request.GET.get("sensor")
        version = request.GET.get("version")
        tblename = version.lower()+"_"+sensorname.lower()
        tracker = request.GET.get("tracker")
        gfr = GFRID.objects.get(tracker = tracker)
        gfrid = gfr.id  
        custBTID = request.GET.get('custBTID')
        custid =request.GET.get("custid")
        print("custBTID"+str(custBTID))
        datetimerange = request.GET.get('datetimerange')
        print(datetimerange)
       
        print(type(datetimerange))
        if datetimerange == "None":
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz)
            last3days = datetime.now(tz) - timedelta(days=3)
            nowdate = now + timedelta(days=1)
        else :
            print("hello1")
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            last3days = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nw = dates[1].strip()
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            nowdate = now + timedelta(days=1)
            print ("NOW: ",nowdate)
        #print(type(last3days))
        print(last3days)
        objects1 = Cust_Batches_Trackers.objects.filter(staffID_id=request.session['staff_ID'])
        objCust = Customers.objects.get(id = request.session['custid'])
        custlat = objCust.Lat
        custlon = objCust.Lon
        j = 0


        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            print("Table")
            objectsAll = GFRID.objects.filter(tracker = tracker)
            objects = objectsAll.last()
            vcode = objects.version.code
            objcount = objectsAll.count()
           
            j = 0
            print("objcount: ",objcount)
            if objcount > 0 :
                objCust = Customers.objects.get(id = custid)
                new_DB = objCust.new_database
                custcode = objCust.Customer_Identifier

                conn1_new = ""
                if new_DB == True:    # CHECK IF ANOTHER DB
                    conn = createNewDB(connection, custcode)
                    conn1_new = conn
                else:
                    conn1_new = connection
                print("CONNECTION NAME******",conn1_new)    


                #customer lat long
                objCust = Customers.objects.get(id = custid)
                custlat = objCust.Lat
                custlon = objCust.Lon
                custTS = objCust.last_modified
                print("TABVLE:::::",tblename)
                if "gps" in sensorname.lower():
                    selectqry = "SELECT * FROM "+tblename+" WHERE gfrid = "+str(gfrid)+" AND  last_modified BETWEEN  '"+ str(last3days) +"' AND '"+ str(nowdate)  +"' ORDER BY id"
                    selectFDqry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tblename+"'  and table_schema = 'public'"
                    print(selectqry, selectFDqry)
                    dbf= GetFieldValues(conn1_new,selectFDqry, tblename)
                    dbv= selectDBFullValues(conn1_new,selectqry, tblename)
                    print("dBF: ",dbf)
                    print("dbv: ",dbv)
                    print("DBV SIZE: ",len(dbv))
                   
                    out_arr1 = np.asarray(dbf)
                    out_arr2 = np.asarray(dbv)
                    print("OUT_ARR1: ",out_arr1.size)
                    print("OUT_ARR2: ",out_arr2)
                    values = ["" for i in range(len(dbv))]
                    #if dbv.size >0:
                    values[0] =  [custlat, custlon, "Location"]
                       
                    for p in dbv:
                            lat = p[3].rstrip()
                            print(p[3])
                            lat = lat[:-1]
                            lon = p[4].rstrip()
                            lon = lon[:-1]
                            tdet = tracker+" Reach here at " +str(p[2])
                            timestamp = p[2]
                            values[j] = [lat, lon, tdet,timestamp]
                            j =j+1
                   



                   
   
                    print("VALUES:",values)  
       
        return JsonResponse(values, safe=False)

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
def createTable(dcon, qry, instqry):
    with dcon.cursor() as cursor:
        cursor.execute(qry)
        cursor.execute(instqry)    
        dcon.close()
        return True
                     
#-----------------------------------------------------------------------------------------
def insertDBTable(dcon, instqry, selqry):
    with dcon.cursor() as cursor:
        dbcur = cursor.execute(selqry)    
        ct = cursor.rowcount
        #print(ct)
        if ct == 0:
            cursor.execute(instqry)    

        dcon.close()
        return True
#----------------------------------------------------------------------------------------
def viewLog(request):
    filename = request.GET.get('f')
    response = HttpResponse(open("logs/"+filename, 'rb').read())
    response['Content-Type'] = 'text/plain'
   
    return response
         
#-----------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------
def configuration(request, tracker):
    custBatch = Cust_Batches_Trackers.objects.get(tracker = tracker)
    batch = Batches_Trackers.objects.get(id = custBatch.batchId)

    gfrid = GFRID.objects.get(tracker=tracker)
    trkName = gfrid.tracker_name
    gereq_flag = gfrid.gereq_flag
    js = gfrid.fullJSON
    if js:
        js = 1
    else:
        js = 0

    #HAT Config
    strng = batch.version.configtabs
    ctabList1 = strng.strip("]").strip("[").split(",")

    ctabList = ["" for k in range(len(ctabList1))] 
    lstarr = ["" for k in range(len(ctabList1))]
    
    k = 0
    for item in ctabList1:
        print(gfrid.id, item.lstrip())
        #checking JSON
        cfobj = ConfigTab.objects.get(id = item)
        tble =  cfobj.configtab
        print("ConfigTab", cfobj.configtab)
        vcod = gfrid.version.code
        #customer
        cust = Cust_Batches_Trackers.objects.get(tracker=tracker)
        custid = cust.customerID
        custs = Customers.objects.get(id = custid)
        newdb = custs.new_database
        custcode = custs.Customer_Identifier
        if newdb == True:
            dcon = createNewDB(connection, custcode)
            #print("Test", dcon)
        else:
            dcon = connection
            #print("Check", dcon)
        tablename = vcod.lower()+"_"+tble.lower()
        with dcon.cursor() as cursor:
            qry1 = "Select jsonfield from "+tablename+" where gfrid = "+str(gfrid.id)
            cursor.execute(qry1)
            print( cursor.rowcount)
            vals = cursor.fetchone()
            if cursor.rowcount > 0:
                data = vals[0]
                if data:
                    print("YES")
                    lstarr[k] = "1"
                else:
                    lstarr[k] = "0"
            else:
               lstarr[k] = "0"
        confper = Config_Permission.objects.filter(configtab_ID = item.lstrip(), GFRID = gfrid.id).exclude(tester_permission= "")
        
        print(confper.count())
        if confper.count() > 0:
            print("Yes")
            ctabList[k] = item.lstrip()
            k= k+1
        
    print(ctabList)
    if "1" in lstarr:
        dt = 1
    else:
        dt = 0
    #Status Data
    sensorslist = batch.version.sensors
    sensorList = sensorslist.strip("]").strip("[").split(",")
    print(sensorList)
   
    return render(request, 
                        'Staff/configuration.html',
                        {
                             'sensorlist':sensorList, 'tablist':ctabList, 'vcod':batch.version.code, 'tracker':tracker, 'custid':custBatch.customerID, 'batchId': custBatch.batchId, 'gereq_flag':gereq_flag, 'js':js, 'custBTID':custBatch.id, 'dt':dt, 'trkName':trkName
                        })         
#-----------------------------------------------------------------------------------------
def selectDBValues(connection,selectqry, tblenames):    
    print(connection)  
    print("QRYLLLLL",selectqry)
    chk =  checkTableExists(connection, tblenames)
    print(chk)
    if chk == True:
        with connection.cursor() as cursor:
            cursor.execute(selectqry)
            exists = cursor.fetchone()
            print("exists", exists)
        #connection.close()
    else:
        exists ="None"
    return exists
#--------------------------------------------------------------------------------------------
def selectDBFullValues(connection,selectqry, tblenames):      
    chk =  checkTableExists(connection, tblenames)
    if chk == True:
        with connection.cursor() as cursor:
            cursor.execute(selectqry)
            exists = cursor.fetchall()
        connection.close()
    else:
        exists ="None"
    return exists    
#--------------------------------------------------------------------------------------------
def GetFieldValues(dcon,selectqry, tblenames):  
    print(selectqry)
    chk =  checkTableExists(dcon, tblenames)
    print("Checl", chk)
    if chk == True:
        dbcur = dcon.cursor()
        dbcur.execute(selectqry)
        LIST = dbcur.fetchall()
       
        LIST =  " ".join( repr(e) for e in LIST  )
        LIST = LIST.replace("(","")
        LIST = LIST.replace(")","")
        LIST = LIST.replace("'","")
        LIST = LIST.rstrip(LIST[-1])
        LIST = LIST.split(",")
   
        #dcon.close()
    else:
        LIST ="None"
    return LIST

#---------------------------------------------------------------------------------------------------------------------------------------------
def get_details(request, sensorid):
    sens = Sensors.objects.get(id=sensorid)
    return render(request,
                            'Staff/viewDetails.html',
                            {
                                'sen':sens
                            })

#--------------------------------------------------------------------------------------------------------------------------------------------------
def mpuVals(values, mod_axis):
    print("mod_axis", mod_axis)
    if "acc" in mod_axis.lower():
        Acc_S = 16384.0
        ACCVAL= float(values)
        #print("ACC : ",ACCVAL)

        return ACCVAL

    elif "gyr" in mod_axis.lower():
        Gyro_S = 131.0
        GYROVAL= float(values)
        #print("GYRO :",GYROVAL)

        return GYROVAL

    elif "mag" in mod_axis.lower():
        ASAX = 177
        ASAY = 178
        ASAZ = 167
        Mag_S = 0.6
        if "x" in mod_axis.lower():
            MAGVAL=float(values)
        elif "y" in mod_axis.lower():
            MAGVAL=float(values)
        else:
            MAGVAL=float(values)
            #print("MAG: ",MAGVAL)    

        return MAGVAL    
       
    elif "temp" in mod_axis.lower():
        temp_S = 340.0
        temp_off = 28.0
        temp_c = 21.0
        TEMP = float(values)
        print("TEMP: ",TEMP)

        return TEMP

#-----------------------------------------------------------------------------------------
def getfilename(request, tracker):

    f = []
    t = []

    path = Path("logs/")
    for x in path.iterdir():
        if x.name.startswith((str(tracker))) == True or x.name.startswith(str('SAMlog')):
            print(x.name)
            f.append(x.name)
            ts = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(os.path.getmtime("{}".format(str(x)))))            
            t.append(ts)
        
    fusion = zip(f, t)

    print(len(f))
    #print(f)
    return render(request, 'Staff/files.html',{'files': fusion
    })

#-----------------------------------------------------------------------------------------

import urllib.request
import requests

def getfiles(request):
	filename = request.GET.get('f')
	fname = filename.split(".")
	md = request.GET.get('md')
	if md=="r":
		response = HttpResponse(open("logs/"+filename, 'rb').read())
		response['Content-Type'] = 'text/plain'
		response['Content-Disposition'] = 'attachment; filename='+filename
		return response
	elif md =="d":
		path ="logs/"+filename
	if os.path.isfile(path):
		os.remove(path)
	return redirect ('/Staff/getfilename/'+str(fname[0])+'/')

#-----------------------------------------------------------------------------------------

def getConfigVal(request, vcod, tble, tracker, custid, batchId):
    my_records = GFRID.objects.get(tracker=tracker)
    gfrid = my_records.id

    custs = Customers.objects.get(id = custid)
    newdb = custs.new_database
    custcode = custs.Customer_Identifier
    if newdb == True:
        dcon = createNewDB(connection, custcode)
        #print("Test", dcon)
    else:
        dcon = connection
        #print("Check", dcon)

    conf = ConfigTab.objects.get(configtab = tble)
    tid = conf.id
   
    vobj = Version.objects.get(code = vcod)
    tablename = vcod.lower()+"_"+tble.lower()
    fieldarr = getTableFields(dcon, tablename)
    #print(fieldarr, "FIELDS")
    if 'id' in fieldarr :
        fieldarr.remove('id')
    
    if 'gfrid' in fieldarr :
        fieldarr.remove('gfrid')
    
    if 'last_modified' in fieldarr :
        fieldarr.remove('last_modified')

    if 'jsonfield' in fieldarr :
        fieldarr.remove('jsonfield')

    fieldS, valueS = [], []

    tz = pytz.timezone('Asia/Calcutta')
    timestamps = datetime.now(tz)
   

    with dcon.cursor() as cursor:
        for fieldname in fieldarr:
            
            val = request.POST.get(fieldname)
            des = request.POST.get(str(fieldname)+"_des")
            dis = request.POST.get(str(fieldname)+"_dis")
            #print('values', val)
            uts = request.POST.get(str(fieldname)+"_units")
            if val == "None":
                val = ""
            

            if val :
                #print("VAl", tablename)
                qry2 = "Select * from "+tablename+" where gfrid = "+str(gfrid)+" and "+fieldname+" = '"+str(val)+"'"
                #print(qry2)
                cursor.execute(qry2)
                countRow = cursor.rowcount
                if countRow == 0:
                    fieldS.append(fieldname)
                    valueS.append(val)

        if len(valueS) > 0:
            score_titles = [{ t : s for t, s in zip(fieldS, valueS)}]
            #print (score_titles, "Length", len(fieldS), len(valueS))
            # Printing in JSON format
            jsonArr = json.dumps(score_titles)
            # Values of Config tabs
            qry1 = "Select jsonfield from "+tablename+" where gfrid = "+str(gfrid)
            #print(qry1)
            cursor.execute(qry1)
            res = cursor.fetchone()
            dcon.commit()
            if cursor.rowcount > 0:
                if res[0]!=None:
                    json_old = str(res[0])
                    print("yahfdfmsdfds ",json_old)
                    json_old1 = json_old.replace("[","")
                    json_old1 = json_old1.replace("]","")
                    json_old1 = json_old1.replace("{","")
                    json_old1 = json_old1.replace("}","")
                    json_old1 = json_old1.replace("'","\"")
                    # new data
                    jsonArr1 = jsonArr.replace("[","")
                    jsonArr1 = jsonArr1.replace("]","")
                    jsonArr1 = jsonArr1.replace("{","")
                    jsonArr1 = jsonArr1.replace("}","")

                    Full = "{"+str(json_old1)+", "+str(jsonArr1)+"}"
                    print(jsonArr1, json_old1, Full)
                    a = ast.literal_eval(json.dumps(Full))
                    print(a)
                    json_obj = json.loads(a)
                    print("Unique Key in a JSON object:")
                    json_obj = json.dumps(json_obj)
                    print(json_obj) 

                    #json_old.append(a)
                    qry = "UPDATE "+tablename+" SET jsonfield = '"+str(json_obj)+"', last_modified = '"+str(timestamps)+"' WHERE gfrid = "+str(gfrid)  
                else:
                    qry = "UPDATE "+tablename+" SET jsonfield = '"+str(jsonArr)+"', last_modified = '"+str(timestamps)+"' WHERE gfrid = "+str(gfrid)           
                print(qry)
                cursor.execute(qry)
                dcon.commit()
            else:
                qry = "insert into "+tablename+"(gfrid, last_modified, jsonfield) values("+str(gfrid)+",'"+str(timestamps)+"','"+str(jsonArr)+"')"
                print(qry)
                cursor.execute(qry)  
                dcon.commit() 
                
        ctabList = ["" for k in range(len(fieldarr))] 
        k =0
        for f in fieldarr:
            confper = Config_Permission.objects.filter(field_name = f, GFRID = gfrid).exclude(tester_permission="")
            for p in confper:
                #print(p.GFRID)
                per = p.tester_permission
                if per:
                    ctabList[k]=f
                    #print(per)
                    k =k+1
        #return render(request, 'Staff/config_DBform.html',{'obj':ctabList, 'vcod':vcod, 'versionName':vobj.version, 'custid':custid, 'tracker':tracker, 'batchId':batchId, 'tble':tble, 'tid':str(tid)})
        return redirect ('/Staff/configuration/'+str(tracker)+'/')

        
    
#----------------------------------------------------------------------------------------------------------------------------------
def getSenConfigVal(request, vcod, tble, tracker, custid, batchId):
    my_records = GFRID.objects.get(tracker=tracker)
    gfrid = my_records.id

    conf = ConfigTab.objects.get(configtab = tble)
    tid = conf.id

    vobj = Version.objects.get(code = vcod)
    sen = eval(vobj.sensors)
    tablename = vcod.lower()+"_"+tble.lower()
    fieldarr = getTableFields(connection, tablename)

    if 'id' in fieldarr :
        fieldarr.remove('id')
    
    if 'gfrid' in fieldarr :
        fieldarr.remove('gfrid')

    if 'sensorid' in fieldarr :
            fieldarr.remove('sensorid')

    if 'last_modified' in fieldarr :
        fieldarr.remove('last_modified')

    if 'jsonfield' in fieldarr :
        fieldarr.remove('jsonfield')

    tz = pytz.timezone('Asia/Calcutta')
    timestamps = datetime.now(tz)

    custs = Customers.objects.get(id = custid)
    newdb = custs.new_database
    custcode = custs.Customer_Identifier
    if newdb == True:
        dcon = createNewDB(connection, custcode)
        #print("Test", dcon)
    else:
        dcon = connection
        #print("Check", dcon)

    with dcon.cursor() as cursor:
        qryS = "Select sensorid from "+tablename+" where gfrid = "+str(gfrid)
        cursor.execute(qryS)
        dcon.commit()
        fet = cursor.fetchall()
        LIST =  " ".join( repr(e) for e in fet  )
        LIST = LIST.replace("(","")
        LIST = LIST.replace(")","")
        LIST = LIST.replace("'","")
        LIST = LIST.replace(" ","")
        LIST = LIST.rstrip(LIST[-1])
        LIS = LIST.split(",")
        for ids in sen: 
            fieldS, valueS, senss = [], [], []              

            for fieldname in fieldarr:
            
                #print(fieldname)
                # Description of Config tabs
                des = request.POST.get(str(fieldname)+"_des")
                dis = request.POST.get(str(fieldname)+"_dis")
                uts = ""
                
                if "sensor" not in fieldname:
                    getname = fieldname+"_"+str(ids)
                    val = request.POST.get(getname)
                    if val == " " or val=="None":
                        val = ""    
                    
                   
                if val :
                    qry2 = "Select * from "+tablename+" where gfrid = "+str(gfrid)+" and "+fieldname+" = '"+str(val)+"' and sensorid = "+str(ids)
                    #print(qry2)
                    cursor.execute(qry2)
                    countRow = cursor.rowcount
                    if countRow == 0:
                        fieldS.append(fieldname)
                        valueS.append(val)
                        senss.append(ids)

            #print("fields", fieldS)
            #print("values", valueS)

            if len(valueS) > 0:   
                score_titles = [{t : s for t, s in zip(fieldS, valueS)}]                         
                #print (score_titles, "Length", len(fieldS), len(valueS))
                # Printing in JSON format
                jsonArr = json.dumps(score_titles)
                qry1 = "Select * from "+tablename+" where gfrid = "+str(gfrid)+" and sensorid = "+str(ids) 
                #print(qry1)
                cursor.execute(qry1)
                dcon.commit()
                #print(cursor.rowcount, "countsssssss")
                if cursor.rowcount > 0:
                    qry2 = "Select jsonfield from "+tablename+" where gfrid = "+str(gfrid)+" and sensorid = "+str(ids) 
                    cursor.execute(qry2)
                    #print(qry2)
                    res = cursor.fetchone()
                    print("json", res[0])
                    if res[0]!=None:
                        json_old = str(res[0])
                        json_old1 = json_old.replace("[","")
                        json_old1 = json_old1.replace("]","")
                        json_old1 = json_old1.replace("{","")
                        json_old1 = json_old1.replace("}","")
                        json_old1 = json_old1.replace("'","\"")
                        # new data
                        jsonArr1 = jsonArr.replace("[","")
                        jsonArr1 = jsonArr1.replace("]","")
                        jsonArr1 = jsonArr1.replace("{","")
                        jsonArr1 = jsonArr1.replace("}","")

                        Full = "{"+str(json_old1)+", "+str(jsonArr1)+"}"
                        #print(jsonArr1, json_old1, Full)
                        a = ast.literal_eval(json.dumps(Full))

                        json_obj = json.loads(a)
                        #print("Unique Key in a JSON object:")
                        json_obj = json.dumps(json_obj)
                        #print(json_obj) 

                        #json_old.append(a)
                        qry = "UPDATE "+tablename+" SET jsonfield = '"+str(json_obj)+"', last_modified = '"+str(timestamps)+"' WHERE gfrid = "+str(gfrid)+" and sensorid = "+str(ids)   
                    else:
                        qry = "UPDATE "+tablename+" SET jsonfield = '"+str(jsonArr)+"' , last_modified = '"+str(timestamps)+"' WHERE gfrid = "+str(gfrid)+" and sensorid = "+str(ids)          

                    print(qry)   
                    cursor.execute(qry)
                    dcon.commit()

                    
                       
    obdata = Version.objects.get(code=vcod)
    sensorlist = eval(obdata.sensors)
    

    k = 0
    
    
    sensorlist1 = ["" for k in range(len(LIS))] 
    sensorName = ["" for k in range(len(LIS))]  


    print(sensorlist, "TRRRR", LIS)
    k = 0
    for item in LIS:
        sensName = Sensors.objects.get(id=item)
        if sensName.code:
            sensorName[k] = sensName.code
            sensorlist1[k] = item
        k = k+1
    
    
    gfrid = GFRID.objects.get(tracker=tracker)
    ctabList = ["" for l in range(len(fieldarr))] 
    l =0
    print(type(ctabList))
    for f in fieldarr:
        confper = Config_Permission.objects.filter(field_name = f, GFRID=gfrid.id).exclude(tester_permission="")
        if confper.count()> 0:
            for p in confper:
                per = p.tester_permission
                print("Per", l)
                if per:
                    ctabList[l]=f
                    l =l+1
    #return render(request, 'Staff/sensor_configDB.html',{'obj':ctabList, 'vcod':vcod, 'versionName':vobj.version, 'custid':custid, 'tracker':tracker, 'batchId':batchId, 'tble':tble, 'tid':str(tid), 'senlist':sensorlist1,'range':range(len(sensorlist1)), 'sensorName':sensorName})
    return redirect ('/Staff/configuration/'+str(tracker)+'/')

        
    #-----------------------------------------------------------------------------------------
        
    #-----------------------------------------------------------------------------------------
#for get request
import http.client
import ast

@csrf_exempt
def getData(request):
   
    HATid=request.GET["Id"]
    gfrid = GFRID.objects.get(hardware_Id=HATid)
    gfridid = gfrid.id
    code = gfrid.Code
    tracker = gfrid.tracker
    fulljson = gfrid.fullJSON
    fulljson = json.dumps(fulljson)
    gereq_flag = gfrid.gereq_flag
    dttime = datetime.now(tz)
           
    path = "logs/"+tracker+".log"
    isFile = os.path.isfile(path)
    #print("File exitsts", isFile)
    setup_logger(request, "1", path)
    logger('Get Data '+" ("+str(request)+")", 'info', str(1))
    
    #customer
    myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
    MyCustomer = Customers.objects.get(id= myCust.customerID)
    new_DB = MyCustomer.new_database
    custcode = MyCustomer.Customer_Identifier
    #version
    bv = Batches_Trackers.objects.get(id = myCust.batchId)
    vid = bv.version.id
    ver = Version.objects.get(id = vid)
    vcod = ver.code
    configtablst = ver.configtabs
    slst = ver.sensors

    conn1_new = ""
    if new_DB == True:
        conn = createNewDB(connection, custcode)
        conn1_new = conn
    else:
        conn1_new = connection

    senDetail = Sensors.objects.all()
    for s in senDetail:
        if str(s.id) in slst:
            if "SIM" in s.code:
                SIMTB = vcod+"_"+s.code
                with conn1_new.cursor() as cursor:
                    sel = "select * from "+SIMTB.lower()+" where gfrid = "+str(gfridid)
                    cursor.execute(sel)  
                    conn1_new.commit()

                    if cursor.rowcount>0:
                        qry ="UPDATE "+SIMTB.lower()+" SET  last_modified = '"+str(datetime.now(tz))+"' where gfrid = "+str(gfridid) 
                        #print(qry)
                        cursor.execute(qry)   
                        conn1_new.commit()
                    else:
                        inst1 = "insert into "+SIMTB.lower()+"(gfrid, last_modified) values("+str(gfridid)+",'"+str(datetime.now(tz))+"')"
                        #print(inst1)
                        cursor.execute(inst1) 
                        conn1_new.commit()   
    
    if str(fulljson) == "null":
        
        objTC = TrackerCommands.objects.filter(tracker=tracker, tester_flag=1).order_by('tester_cmdOrder')
        firsts = objTC.first()

        objTC1 = TrackerCommands.objects.filter(tracker=tracker, flag=1).order_by('cmdOrder')
        firsts1 = objTC1.first()

        objTC2 = TrackerCommands.objects.filter(tracker=tracker, cust_flag=1).order_by('cust_cmdOrder')
        firsts2 = objTC2.first()

        
        
        if firsts:
            cmd = TestCommands.objects.get(id = firsts.commandID)
            data= cmd.cmdjson
            cap = data['CMD'].upper()
            data['ID'] = HATid
            data['CMD'] = cap

            code = data['CODE']
            if code:
                coddata = {"CODE":code}
                data = dict(list(coddata.items()) + list(data.items()))
            print(data)
            
            TrackerCommands.objects.filter(tracker=tracker, process_flag = 1).update(process_flag = 0)
            TrackerCommands.objects.filter(id = firsts.id).update(last_modified = dttime, tester_flag=2, process_flag = 1, process_time = dttime)


            logger('Sending Selected Commands '+" ("+str(data)+")", 'info', str(1))
            return JsonResponse(data, safe=False)     

        elif firsts1:
            cmd = TestCommands.objects.get(id = firsts1.commandID)
            data= cmd.cmdjson
            cap = data['CMD'].upper()
            data['ID'] = HATid
            data['CMD'] = cap

            code = data['CODE']
            if code:
                coddata = {"CODE":code}
                data = dict(list(coddata.items()) + list(data.items()))
            print(data)
            
            TrackerCommands.objects.filter(tracker=tracker, process_flag = 1).update(process_flag = 0)
            TrackerCommands.objects.filter(id = firsts1.id).update(last_modified = dttime, flag=2, process_flag = 1, process_time = dttime)


            logger('Sending Selected Commands '+" ("+str(data)+")", 'info', str(1))
            return JsonResponse(data, safe=False)  
            
        elif firsts2:
            cmd = TestCommands.objects.get(id = firsts2.commandID)
            data= cmd.cmdjson
            cap = data['CMD'].upper()
            data['ID'] = HATid
            data['CMD'] = cap

            code = data['CODE']
            if code:
                coddata = {"CODE":code}
                data = dict(list(coddata.items()) + list(data.items()))
            print(data)
            
            TrackerCommands.objects.filter(tracker=tracker, process_flag = 1).update(process_flag = 0)
            TrackerCommands.objects.filter(id = firsts2.id).update(last_modified = dttime, cust_flag=2, process_flag = 1, process_time = dttime)


            logger('Sending Selected Commands '+" ("+str(data)+")", 'info', str(1))
            return JsonResponse(data, safe=False)
             
        else:
            nodata = { "CODE": "GFR", "ID": HATid, "CMD":"NONE",}
            logger('Sending Nodata command '+" ("+str(nodata)+")", 'info', str(1))
            return JsonResponse(nodata, safe=False)     
    else:
        dttime = datetime.now(tz)
        GFRID.objects.filter(id = gfridid).update(gereq_flag = 1, last_modified = dttime)
        res = ast.literal_eval(fulljson)
        logger('Sending changed configuration'+" ("+str(res)+")", 'info', str(1))
        return JsonResponse(res, safe=False)
#---------------------------------------------------------------------------------------------------        

def flushUpdates(con, tble, GFRIDID):
    jsfield = "null"
    with con.cursor() as cursor:
        qry = "UPDATE "+tble+" SET jsonfield = "+str(jsfield)+" WHERE gfrid = "+str(GFRIDID)         
        cursor.execute(qry)
        con.commit()
        return True

def flushDatas(con, tble, GFRIDID):
     with con.cursor() as cursor:
        qry = "DELETE FROM public."+tble+" where gfrid = "+str(GFRIDID)
        print("DELETE", qry)
        cursor.execute(qry)
        con.commit()
        return True


#-----------------------------------------------------------------------------------------
def alerts(request, tracker):
    print("Hello")     
#----------------------------------------------------------------------------------------------------------------------------------------------------
def mapzoom(request,version,tracker,custBTID,custid,sensorname,sensorid):
   print("mapzoom!!!!!!!!!!",version,tracker,custBTID,custid,sensorname,sensorid)
   return render(request,
                            'Staff/mapzoom.html',
                            {
                                'custBTID': custBTID, 'version':version, 'custid':custid, 'tracker':tracker, 'sensorname':sensorname, 'sensorid':sensorid
                            })

#--------------------------------------------------------------------
#--------------------------------------------------------------------
def getSenCommands(request, tracker):
    GFRIDV = GFRID.objects.get(tracker=tracker)
    verID = GFRIDV.version.id
    trkName = GFRIDV.tracker_name
    
    
    custBatch = Cust_Batches_Trackers.objects.get(tracker = tracker)
    batch = Batches_Trackers.objects.get(id = custBatch.batchId)
   
    #general commands
    Tstcommds = TestCommands.objects.filter(sensorID = 0) 
                                
    for i in Tstcommds:
        SC = SensorCommands.objects.filter(commandID = i.id)
        if SC.count() == 0:
            SensorCommands.objects.create(versionID=verID, sensorID = 0, commandID=i.id)
       
    Commds = SensorCommands.objects.filter(versionID = verID)
    for com in Commds:
        cmdval = 0
        TC = TrackerCommands.objects.filter(tracker=tracker)
        TCct = TC.count()
        flter = TC.filter(commandID=com.commandID)
        flct = flter.count()
        if(flct == 0 ):
            TrackerCommands.objects.create(tracker=tracker, commandID= com.commandID, flag=cmdval, cmdOrder = TCct+1)

    Commds1 = TrackerCommands.objects.filter(tracker = tracker).order_by("cmdOrder")
    AllCommds = TestCommands.objects.all()

    Commds23 = TrackerCommands.objects.filter(commandID__in=TestCommands.objects.filter().exclude(user_permission= ""), tracker = tracker, tester_cmdOrder = 1)
    if Commds23.count() == 0:
        Commds22 = TrackerCommands.objects.filter(commandID__in=TestCommands.objects.filter().exclude(user_permission= ""), tracker = tracker).order_by("cmdOrder")
        y = 1
        for i in Commds22:
            TrackerCommands.objects.filter(id = i.id).update(tester_cmdOrder = y)
            y = y + 1
    Commds2 = TrackerCommands.objects.filter(commandID__in=TestCommands.objects.filter().exclude(tester_permission= ""), tracker = tracker).order_by("tester_cmdOrder")

    return render(request,
                            'Staff/commands.html',
                            {
                                'objCT':Commds2, 'allcmds':AllCommds, 'tracker':tracker, 'custid':custBatch.customerID, 'batchId': custBatch.batchId, 'custBTID':custBatch.id, 'trkName':trkName
                            })                                                        
            
#-------------------------------------------------------------------------------------------------------------------


def CMDMode(request, tracker, tab):
    GFRIDV = GFRID.objects.get(tracker=tracker)
    verID = GFRIDV.version.id
    Commds = SensorCommands.objects.filter(versionID = verID)
    custBatch = Cust_Batches_Trackers.objects.get(tracker = tracker)
    batch = Batches_Trackers.objects.get(id = custBatch.batchId)

    for i in Commds:
        commandID =  i.commandID
        modes = "mode"+str(commandID)
        types = "typ"+str(commandID)
        chkmode = request.POST.get(modes)
        typ = request.POST.get(types)
        if chkmode == "on":
            cmdval = 5
          
        else:
            cmdval = 0

        dttime = datetime.now(tz)

        trcComm = TrackerCommands.objects.filter(tracker=tracker, commandID=commandID)
       
            
        if trcComm.count() == 0:
            if typ == "admin":
                TrackerCommands.objects.create(tracker=tracker, commandID= commandID, flag=cmdval)
            elif typ == "tester":
                TrackerCommands.objects.create(tracker=tracker, commandID= commandID, tester_flag=cmdval)
            if typ == "cust":
                TrackerCommands.objects.create(tracker=tracker, commandID= commandID, cust_flag=cmdval)
        else: 
            trkCMD1 = TrackerCommands.objects.filter(tracker = tracker, tester_flag = 1)
            trkCMD2 = TrackerCommands.objects.filter(tracker = tracker, cust_flag = 1)
            trkCMD3 = TrackerCommands.objects.filter(tracker = tracker, flag = 1)   
                 	
            if typ == "admin":
                if trkCMD1.count() == 0 and trkCMD2.count() == 0 and trkCMD3.count() == 0:
                    TrackerCommands.objects.filter(tracker=tracker, commandID= commandID).update(flag = cmdval, last_modified = dttime)
            elif typ == "tester":
                if trkCMD1.count() == 0 and trkCMD2.count() == 0 and trkCMD3.count() == 0:
                    TrackerCommands.objects.filter(tracker=tracker, commandID= commandID).update(tester_flag = cmdval, last_modified = dttime)
            if typ == "cust":
                if trkCMD1.count() == 0 and trkCMD2.count() == 0 and trkCMD3.count() == 0:
                    TrackerCommands.objects.filter(tracker=tracker, commandID= commandID).update(cust_flag = cmdval, last_modified = dttime)
                    
    if tab =="D":
        return redirect ('/SuperAdmin/config/'+str(tracker)+'/'+str(custBatch.customerID)+'/D/'+str(custBatch.batchId))
    elif tab == "C":
        return redirect ('/Customer/getSenCommands/'+str(tracker)) 
    else:
        return redirect ('/Staff/getSenCommands/'+str(tracker))   
        
#-------------------------------------------------------------------------------------------------------------------   

#showing the changed configuration list------------------------------------------------
def changedConfig(request, tracker):
    GFRIDV = GFRID.objects.get(tracker=tracker)
    verID = GFRIDV.version.id
    vcod = GFRIDV.version.code
    configlst = GFRIDV.version.configtabs
    gfridid = GFRIDV.id


    myCust = Cust_Batches_Trackers.objects.get(tracker=tracker)
    MyCustomer = Customers.objects.get(id= myCust.customerID)
    new_DB = MyCustomer.new_database
    custcode = MyCustomer.Customer_Identifier
    if new_DB == True:
        dcon = createNewDB(connection, custcode)
        #print("Test", dcon)
    else:
        dcon = connection

    
    ctabList1 = configlst.strip("]").strip("[").split(",")

    return render(request, 
                        'Staff/changeConfig.html',
                        {
                            'tablist':ctabList1, 'vcod':vcod, 'tracker':tracker, 'custid':myCust.customerID, 'batchId': myCust.batchId
                        })    
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def alertsgraphdata(request, tracker,todate,fromdate):
    print("welcome graph data")
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id    
    
    if todate == '0':
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz)
            last3days = datetime.now(tz) - timedelta(days=3)
            todate = str(int(now.timestamp()))
            fromdate =str(int(last3days.timestamp()))          
            
            
    else :
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")            
            t =  dates[0].strip()
            fdate = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            fromdate = int(fdate.timestamp())
            #fromdate1 = fdate - timedelta(hours=5,minutes=30)
            #fromdate = int(fromdate1.timestamp())
            tdate = dates[1].strip()
            now = datetime.strptime(tdate, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            todate = int(now.timestamp())
            #nowdate = now - timedelta(hours=5,minutes=30)
            #todate = int(nowdate.timestamp())
            
                   
    return render(request, 
                        'Staff/alerts.html',
                        {
                             'tracker':tracker,'gfrid':gfrid,'todate':todate,'fromdate':fromdate
                        })         
#-------------------------------------------------------------------------------  
def showalertsdata(request, tracker,alertinfo):
    print("welcome graph data")
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id
    HAT = AlertNotification.objects.get(alert_notice = alertinfo)
    HAT_id = HAT.id
    print("helloooo---",HAT_id)    
    obj = Alerts.objects.filter(alertNotify_id = HAT_id)             
    return render(request, 
                        'Staff/alertsinfo.html',
                        {
                             'alertobjs' : obj,'tracker':tracker,'gfrid':gfrid,'alertinfo':alertinfo
                        })         
#--------------------------------------------------------------------------------------        
def alertVal(request,gfrID,tracker,filename,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        return render(request, 'Staff/alerts_'+filename+'.html',{
                            'gfrID':gfrID, 'tracker':tracker, 'todate':todate, 'fromdate':fromdate
                        })       
#--------------------------------------------------------------------------------------
  
def alertsjson(request,gfrID,alertsid,todate,fromdate):
    tz = pytz.timezone("Asia/Calcutta")
    data=[]
    data1=[]    
    combine_data = []    
    alerts = Alerts.objects.filter(GFRID = gfrID, alertNotify_id=alertsid,TS_BigInt__range=[fromdate,todate])
    alerts_count = alerts.count()  
    print("ALERTSCOUNT",alerts_count)
    close_old_connections() 
    alerts_info = AlertNotification.objects.get(id = alertsid)
    fromdate_int=int(fromdate) 
    todate_int = int(todate)    
    if alerts_count == 0:
        for x in range(6):
            print("No alert")
            fromdate_int1 =fromdate_int+2            
            data.append(0)
            data1.append(fromdate_int1) 
        data.append(0)
        data1.append(todate_int)        
       
    else:
        
        for obj in alerts:      
            fromdate_db=obj.TS_BigInt
            my_time = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(fromdate_db))
            utc_dt = datetime.utcfromtimestamp(fromdate_db)
            fromdate_local = utc_dt - timedelta(hours=5,minutes=30)            
            TS_ON = int(fromdate_local.timestamp())            
            if obj.status==0: 
                for x in range(6):
                    fromdate_int1 =fromdate_int+2                    
                    data.append(0)
                    data1.append(fromdate_int1) 
                data.append(1)
                data1.append(TS_ON)                
                if int(obj.TS_OFF_BigInt) <= int(todate):
                    todate_db=obj.TS_OFF_BigInt
                    utc_dt = datetime.utcfromtimestamp(todate_db)                    
                    todate_local = utc_dt - timedelta(hours=5,minutes=30)                                     
                    TS_OFF = int(todate_local.timestamp())
                    data.append(0)
                    data1.append(TS_OFF)                             
            else:
                for x in range(6):
                    print("No alert")
                    fromdate_int1 =fromdate_int+2                    
                    data.append(0)
                    data1.append(fromdate_int1)
                data.append(1)
                data1.append(TS_ON)        
    combine_data = [{alerts_info.alert_notice: t, "datetime": s} for t, s in zip(data, data1)]     
    connection.close()     
    combine_data.sort(key=lambda item:item['datetime'], reverse=False)    
    return JsonResponse(combine_data, safe=False)
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def alerts(request, tracker):
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id
    alerts = Alerts.objects.filter(GFRID = gfrid,status = 1)
    alertObj = alerts.count()
    print("AlertsCount: ",alertObj)
    return render(request, 
                        'Staff/alertSnapshot.html',
                        {
                             'alerts' : alerts,'tracker':tracker, 'alertObj':alertObj
                        })  

#-----------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------
def alertsPlotlygraphdata(request, tracker,todate,fromdate):
    global t
    global tdate
    global t_d
    global tdate_d
    print("welcome graph data")
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id  
    
    if todate == '0':

        tdate = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y %b %d %H:%M:%S')
        t = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=1)).strftime('%Y %b %d %H:%M:%S')
        
        dt_e = datetime.strptime(t, '%Y %b %d %H:%M:%S')
        dt_s = datetime.strptime(tdate, '%Y %b %d %H:%M:%S')

        todate = round(dt_s.timestamp())
        fromdate = round(dt_e.timestamp())
           
    else :
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")            
            t =  dates[0].strip()
            
            # t_d = datetime.strptime(t,'%d-%b %H:%M:%S').astimezone(pytz.timezone("Asia/Calcutta"))
            fdate = datetime.strptime(t, '%Y %b %d %H:%M:%S').astimezone(pytz.timezone("Asia/Calcutta"))
            fromdate = int(fdate.timestamp())
            # fromdate = int(fdate.timestamp())
        
            tdate = dates[1].strip()

            # tdate_d = datetime.strptime(tdate_d,'%d-%b %H:%M:%S').astimezone(pytz.timezone("Asia/Calcutta"))
            now = datetime.strptime(tdate, '%Y %b %d %H:%M:%S').astimezone(pytz.timezone("Asia/Calcutta"))
            # todate = int(now.timestamp())
            todate = int(now.timestamp())
            
    alerts_info = AlertNotification.objects.all()
    alertslen = alerts_info.count()  
    print("ALERTS LENGTHHHH: ",alertslen) 
    alertslength = range(1,alertslen)            
    return render(request, 
                        'Staff/alertsPlotly.html',
                        {
                             'tracker':tracker,'gfrid':gfrid,'todate':todate,'fromdate':fromdate, 'alertslen':alertslength
                        })         
    
#--------------------------------------------------------------------------------------  
#--------------------------------------------------------------------------------------        
def alertPlotlyVal(request,gfrID,tracker,alertsid,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        print("Alerts: ", alertsid)
        alerts_info = AlertNotification.objects.get(id = alertsid)
        filename = alerts_info.alert_notice 
        print("alertFile:: ", filename)
        if filename != 'UNKNOWN':
            return render(request, 'Staff/alerts_'+filename+'.html',{
                            'gfrID':gfrID, 'tracker':tracker, 'todate':todate, 'fromdate':fromdate, 'alertsid':alertsid
                        })       
#--------------------------------------------------------------------------------------  
#                               
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def alertGraphsUpdate(request,tracker,gfrID,alertsid,todate,fromdate):
    data=[]
    data1=[]

    dt = []
    dt_ts = []

    combine_data = []
    dt1 = []
    st1 = []
    st2 =[]

    dbv = Alerts.objects.filter(GFRID=gfrID,TS_BigInt__range=[fromdate,todate]).values('id')

##############Getting all alert id by using gfrid######################
    # an_id = AlertNotification.objects.all()
    an = connection.cursor()
    an.execute(f'''SELECT * FROM "SuperAdmin_alertnotification"''')
    an_id = an.fetchall()

    g_id = []
    g_tt = []
    g_to = []
    g_ts = []
    for ln in range(len(an_id)):
        g_id.append([])
        g_tt.append([])
        g_to.append([])
        g_ts.append([])

    ##############Getting all alert timestamp by using gfrid######################
    for ln in range(len(an_id)):
        g = Alerts.objects.filter(GFRID=gfrID,alertNotify_id=an_id[ln][0]).values('TS_BigInt')

        for g_t in g:
            g_ts[ln].append(g_t['TS_BigInt'])

    #############################################################################
    
    for ln in range(len(an_id)):
        gg = Alerts.objects.filter(GFRID=gfrID,alertNotify_id=an_id[ln][0],id__gte=10000).values('id')
        gt = Alerts.objects.filter(GFRID=gfrID,alertNotify_id=an_id[ln][0],id__gte=10000,TS_BigInt__lte=fromdate).values('TS_BigInt')
        goff = Alerts.objects.filter(GFRID=gfrID,alertNotify_id=an_id[ln][0],id__gte=10000,TS_BigInt__lte=fromdate).values('TS_OFF_BigInt')

        for g_i in gg:
            g_id[ln].append(g_i['id'])
        
        for g_t in gt:
            g_tt[ln].append(g_t['TS_BigInt'])

        for g_o in goff:
            if g_o['TS_OFF_BigInt']:
                g_to[ln].append(g_o['TS_OFF_BigInt'])

    for ln in range(len(an_id)):
        for a in range(len(g_to[ln])):
            g_tt[ln].append(g_to[ln][a])
    
    g_tt = [sorted(item) for item in g_tt]
            
    for l in range(len(dbv)):
        for ln in range(len(an_id)):
            ts_t = connection.cursor()
            ts_t.execute(f'''SELECT "TS_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
            Ts_t = ts_t.fetchall()[0][0]
            if (Ts_t not in dt_ts) and (an_id[ln][0] != 12) and (an_id[ln][0] != 16):
                dt_ts.append(Ts_t)
                ts_to = connection.cursor()
                ts_to.execute(f'''SELECT "TS_OFF_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
                Ts_to = ts_to.fetchall()[0][0]
                if Ts_to:
                    dt_ts.append(Ts_to)
            
        for ln in range(len(an_id)):
            ts_d = connection.cursor()
            ts_d.execute(f'''SELECT "TS_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
            Ts_d = ts_d.fetchall()[0][0]

            al_h = connection.cursor()
            al_h.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
            Al_h = json.loads(al_h.fetchall()[0][0])
            Al_h = Al_h['ALERT']
            # ss.append(Al_h)

            al_n = connection.cursor()
            al_n.execute(f'''SELECT "hexVal" FROM "SuperAdmin_alertnotification" WHERE id = {an_id[ln][0]}''')
            Al_n = al_n.fetchall()[0][0]
            # ss.append(Al_n)

            if (datetime.fromtimestamp(Ts_d).strftime('%d-%b %H:%M:%S') not in dt) and (Al_h==Al_n) and (an_id[ln][0] != 12) and (an_id[ln][0] != 16):
                dt.append(datetime.fromtimestamp(Ts_d).strftime('%d-%b %H:%M:%S'))
                ts_do = connection.cursor()
                ts_do.execute(f'''SELECT "TS_OFF_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
                Ts_do = ts_do.fetchall()[0][0]
                if Ts_do and (int(Ts_do) <= int(todate)):
                    dt.append(datetime.fromtimestamp(Ts_do).strftime('%d-%b %H:%M:%S'))

    dt_ts = sorted(dt_ts)
    dt = sorted(dt)

#####################Getting serperate Alert datetime#################################
    for ln in range(len(an_id)):
        dt1.append([])

    for l in range(len(dbv)):
        for ln in range(len(an_id)):
            ts_t = connection.cursor()
            ts_t.execute(f'''SELECT "TS_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
            Ts_t = ts_t.fetchall()[0][0]

            al_h = connection.cursor()
            al_h.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
            Al_h = json.loads(al_h.fetchall()[0][0])
            Al_h = Al_h['ALERT']

            al_n = connection.cursor()
            al_n.execute(f'''SELECT "hexVal" FROM "SuperAdmin_alertnotification" WHERE id = {an_id[ln][0]}''')
            Al_n = al_n.fetchall()[0][0]
            if(Al_h==Al_n):    
                dt1[an_id[ln][0]-1].append(Ts_t)
                ts_to = connection.cursor()
                ts_to.execute(f'''SELECT "TS_OFF_BigInt" FROM "SuperAdmin_alerts" WHERE id = {int(dbv[l]['id'])}''')
                Ts_to = ts_to.fetchall()[0][0]
                if Ts_to and (int(Ts_to) <= int(todate)):
                    dt1[an_id[ln][0]-1].append(Ts_to)
            dt1[an_id[ln][0]-1] = sorted(dt1[an_id[ln][0]-1])

# #########No Alert in between a filter date#############
    if len(dt) == 0:
        t_d = parse(t).strftime('%d-%b %H:%M:%S')
        tdate_d = parse(tdate).strftime('%d-%b %H:%M:%S')
        dt.append(t_d)
        dt.append(tdate_d)

        for ln in range(len(an_id)):
            st1.append([])
        
        for ln in range(len(an_id)):
            if len(g_tt[ln]) ==  0:
                st1[ln].append(0)
                st1[ln].append(0)
            else:
                if g_tt[ln][-1] in g_ts[ln]:
                    print(g_tt[ln][-1])
                    print(g_ts[2])
                    stat_s = connection.cursor()
                    stat_s.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" where "GFRID" = {gfrID} AND "alertNotify_id" = {an_id[ln][0]} AND "TS_BigInt" = {g_tt[ln][-1]}''')
                    Stat_s = json.loads(stat_s.fetchall()[0][0])
                    Stat_s = Stat_s['STATUS']

                    stat_o = connection.cursor()
                    stat_o.execute(f'''SELECT "TS_OFF_BigInt" FROM "SuperAdmin_alerts" where "GFRID" = {gfrID} AND "alertNotify_id" = {an_id[ln][0]} AND "TS_BigInt" = {g_tt[ln][-1]}''')
                    Stat_o = stat_o.fetchall()[0][0]
                    # print(Stat_off)

                    st1[ln].append(Stat_s)
                    st1[ln].append(Stat_s)
    
                else:
                    st1[ln].append(0)
                    st1[ln].append(0)
                    # st1[an_id[ln][0]-1].append(Alerts.objects.filter(GFRID=gfrID).filter(alertNotify=an_id[ln][0]).filter(TS_BigInt=temp[-1]).values('status')[0]['status'])
    
        alertbefore = Alerts.objects.filter(GFRID = gfrID, alertNotify_id=alertsid, status = 1, TS_BigInt__lte=fromdate)
        alerts = Alerts.objects.filter(GFRID = gfrID, alertNotify_id=alertsid,TS_BigInt__range=[fromdate,todate])
        alerts_count = alerts.count()  
        # print("ALERTSCOUNT",alerts_count)
        close_old_connections() 
        alerts_info = AlertNotification.objects.get(id = alertsid)
        fromdate_int=int(fromdate) 
        todate_int = int(todate)    

        if alerts_count == 0:
            if alertbefore.count() > 0:
                # print("Previous Alerts !")
                data.append(1)
                data1.append(fromdate_int)    
                data.append(1)
                data1.append(todate_int) 
            else:         
                # print("No Alerts !")
                data.append(0)
                data1.append(fromdate_int)    
                data.append(0)
                data1.append(todate_int)   
        else:
            # print("Alerts found!")
            # print("ALERTCOUNTTTT: ",alerts_count)
            if alertbefore.count() > 0:
                data.append(1)
                data1.append(fromdate_int) 
                
            else:
                data.append(0)
                data1.append(fromdate_int)
                # print("0",fromdate_int)
            for obj in alerts:      
                fromdate_db=obj.TS_BigInt
                TS_ON = int(fromdate_db)
                # print("TSONNNNN: ",TS_ON)     
                if obj.TS_OFF_BigInt:
                    todate_db = obj.TS_OFF_BigInt
                    TS_OFFZ = int(todate_db)  
                else:
                    TS_OFFZ = 0    
                # print("TS_OFFZZZZZZZZZZ",TS_OFFZ)

                if obj.status == 0:
                    # print("Status of the alert: ",obj.status)
                                    
                    data.append(0)
                    data1.append(TS_ON)
                    
                    data.append(1)
                    data1.append(TS_ON)
                    
                    data.append(1)
                    data1.append(TS_OFFZ)
                
                    data.append(0)
                    data1.append(TS_OFFZ)
                        
                else:
                    # print("Status of the alert: ",obj.status)
                    data.append(1)
                    data1.append(TS_ON)

        combine_data = [{alerts_info.alert_notice: t, "datetime": s} for t, s in zip(data, data1)]     
        connection.close()  
        # print("DATA:::",combine_data)   
        combine_data.sort(key=lambda item:item['datetime'], reverse=False) 
        datalist = []
        timelist = []        
        for i in combine_data:
            
            key_list = [x for x in i]
            
            value_list = i.get(key_list[0])
            value_list1 = i.get(key_list[1])
            
            datalist.append(value_list)
            timestampss = datetime.fromtimestamp(int(value_list1)).strftime('%Y-%m-%d %H:%M:%S') 
            #timelist.append(timestampss)
            timelist.append(value_list1)

        alertrs = System_Health.objects.filter(GFRID = gfrID, system_offtime__range=[fromdate,todate])
        if alertrs.count() > 0:
            for i in alertrs:
                timestampss = datetime.fromtimestamp(int(i.system_offtime)).strftime('%Y-%m-%d %H:%M:%S') 
                timelist.append(i.system_offtime)
                datalist.append(None)    
        zipped_pairs = zip(datalist, timelist)
        if zipped_pairs:
            ziped = sorted(list(zipped_pairs), key = lambda x: x[1])
        else:
            ziped = []    
        # print("Zipped PAir: ",ziped)
        # print(type(ziped))
        STATUS = []
        TIMES = []
        TIM = []
        if ziped:
            for x,y in zip(ziped, itertools.islice(ziped, 1, None)):
                if y[0] == None:
                    # print("values: ",x[1],y[1])
                    ziped.append((x[0],y[1]))
            zipedagain = sorted(list(ziped), key = lambda x: x[1])
            # print("Zipped PAir: ",zipedagain)    
            for x,y in zip(zipedagain, itertools.islice(zipedagain, 1, None)):   
                # print("iterating: ",x[0],x[1],y[0],y[1])
                if x[1] == y[1]:
                    # print("iterating ts equals: ",x[0],x[1],y[0],y[1])
                    if x[1] == None:
                        tempo = x[0]
                        x[0]= y[0]
                        y[0]= tempo
            # print("ZippedAgainnnnn: ",zipedagain)
            for i in zipedagain:
                STATUS.append(i[0])
                TIM.append(i[1])
                
            for i in TIM:
                timestampss = datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S') 
                # print(i,timestampss)
                TIMES.append(timestampss)
            # print("AlertIdependentGraph List:",timelist, datalist)
            # print("Final List:",STATUS,TIMES)
        
        if "HAT_GSM_LOW_SIGNAL" in key_list:
            
            trace = {
                        "x": dt,
                        "y": st1[0],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GSM LOW SIGNAL',
                        "type": 'scatter',
                        "line_color":'#FF0000',
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            gsmfigure = go.Figure(data = data3, layout = layout3)
            gsmfigure.update_yaxes(fixedrange=True,automargin=True)
            # gsmfigure.update_xaxes(tickangle=90)
            gsmfigure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    

            msg2 = "GSM LOW SIGNAL"
            if timelist:
                plot_div = opy.plot(gsmfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2,'dt':f"Date Range Selected : {t} - {tdate}"})    
        elif "HAT_GPS_NO_SIGNAL" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[1],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS NO SIGNAL',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )        

            msg2 = "GPS NO SIGNAL"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2})

        elif "HAT_GPS_OUT_OF_WRK_GEOFENCE" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[2],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS OUT OF WRK',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True) 
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "GPS OUT OF WRK"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GPS_OUT_OF_STR_GEOFENCE" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[3],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS OUT OF STR',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)    
            msg2 = "GPS OUT OF STR"
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg, 'msg2':msg2})
        elif "HAT_GG_FULL_CHARGE_ALERT" in key_list:
            print("TIMELIST, DATALIST: ",timelist, datalist)
            trace = {
                        "x": dt,
                        "y": st1[14],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'FULL CHARGE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            msg2 = "GG FULL CHARGE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_HIGH_VOLTAGE_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[4],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG HIGH VOLTAGE THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "GG HIGH VOLTAGE THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_VOLTAGE_BELOW_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[5],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG VOLTAGE BELOW THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True) 
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "VOLTAGE BELOW THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_SOC_BELOW_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[6],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG SOC BELOW THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            msg2 = "SOC BELOW THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_HIGH_TEMPERATURE" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[7],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG HIGH TEMPERATURE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "HIGH TEMPERATURE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_BME_HIGH_TEMPERATURE" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[8],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'BME HIGH TEMPERATURE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "BME HIGH TEMPERATURE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_HIGH_VIBRATION" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[9],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'HIGH VIBRATION',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )        
            msg2 = "HIGH VIBRATION"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_MOTION_DETECTED" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[10],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'MOTION DETECTED',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            ) 

            msg2 = "MOTION DETECTED"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_SYSTEM_RESTART" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[13],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'SYSTEM RESTART',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "SYSTEM RESTART"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
                                                                            
        elif "HAT_SYSTEM_ON_POWER_LINE" in key_list:
            trace = {
                        "x": dt,
                        "y": st1[12],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'SYSTEM ON POWER LINE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )

            msg2 = "POWERLINE"
            
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2})

# # ############################

# # ###########X-axis#############
    else:
        # an_id = AlertNotification.objects.all()
        an = connection.cursor()
        an.execute(f'''SELECT * FROM "SuperAdmin_alertnotification"''')
        an_id = an.fetchall()

        for ln in range(len(an_id)):
            st2.append([])

        for ln in range(len(an_id)):
            # for ll in range(len(dt_ts)): 
            # for ln in range(len(an_id)):
            if len(dt1[ln]) == 0:
                if (len(g_tt[ln])==0):
                    st2[ln].append(0)

                elif(len(g_tt[ln])>0):
                    if g_tt[ln][-1] not in g_ts[ln]:
                        st2[ln].append(0)

                    elif g_tt[ln][-1] in g_ts[ln]:
                        stat_s = connection.cursor()
                        stat_s.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" where "GFRID" = {gfrID} AND "alertNotify_id" = {an_id[ln][0]} AND "TS_BigInt" = {g_tt[ln][-1]}''')
                        Stat_s = json.loads(stat_s.fetchall()[0][0])
                        Stat_s = Stat_s['STATUS']

                        st2[ln].append(Stat_s)

            for ll in range(len(dt_ts)):        
                if(dt_ts[ll] in dt1[ln]):
                    if dt_ts[ll] not in g_ts[ln]:
                        st2[ln].append(0)

                    elif(dt_ts[ll] in g_ts[ln]):                        
                        stat_s = connection.cursor()
                        print("last",dt_ts[ll])
                        stat_s.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" where "GFRID" = {gfrID} AND "alertNotify_id" = {an_id[ln][0]} AND "TS_BigInt" = {dt_ts[ll]}''')
                        Stat_s = json.loads(stat_s.fetchall()[0][0])
                        Stat_s = Stat_s['STATUS']

                        if int(Stat_s) == 1:
                            st2[ln].append(1)
                        elif(int(Stat_s)==0):
                            st2[ln].append(0)
                    
                elif(dt_ts[ll] not in dt1[ln])and(ll-1<0):                
                    if len(g_tt[ln])==0:
                        st2[ln].append(0)
                    elif(g_tt[ln][0]==dt_ts[ll]):
                        st2[ln].append(0)
                    else:
                        if g_tt[ln][-1] not in g_ts[ln]:
                            st2[ln].append(0)
                        elif(g_tt[ln][-1] in g_ts[ln]):
                            stat_s = connection.cursor()
                            stat_s.execute(f'''SELECT "jsonFile" FROM "SuperAdmin_alerts" where "GFRID" = {gfrID} AND "alertNotify_id" = {an_id[ln][0]} AND "TS_BigInt" = {g_tt[ln][-1]}''')
                            Stat_s = json.loads(stat_s.fetchall()[0][0])
                            Stat_s = Stat_s['STATUS']

                            if(int(Stat_s)==1):
                                st2[ln].append(1)
                            elif(int(Stat_s)==0):
                                st2[ln].append(0)

                elif(dt_ts[ll] not in dt1[ln]) and (ll-1>=0):
                    # print("sssssssss",st2[ln][-1])
                    st2[ln].append(st2[ln][-1])

        # return HttpResponse(st2)

    #########################

        alertbefore = Alerts.objects.filter(GFRID = gfrID, alertNotify_id=alertsid, status = 1, TS_BigInt__lte=fromdate)
        alerts = Alerts.objects.filter(GFRID = gfrID, alertNotify_id=alertsid,TS_BigInt__range=[fromdate,todate])
        alerts_count = alerts.count()  
        # print("ALERTSCOUNT",alerts_count)
        close_old_connections() 
        alerts_info = AlertNotification.objects.get(id = alertsid)
        fromdate_int=int(fromdate) 
        todate_int = int(todate)    

        if alerts_count == 0:
            if alertbefore.count() > 0:
                # print("Previous Alerts !")
                data.append(1)
                data1.append(fromdate_int)    
                data.append(1)
                data1.append(todate_int) 
            else:         
                # print("No Alerts !")
                data.append(0)
                data1.append(fromdate_int)    
                data.append(0)
                data1.append(todate_int)   
        else:
            # print("Alerts found!")
            # print("ALERTCOUNTTTT: ",alerts_count)
            if alertbefore.count() > 0:
                data.append(1)
                data1.append(fromdate_int) 
                
            else:
                data.append(0)
                data1.append(fromdate_int)
                # print("0",fromdate_int)
            for obj in alerts:      
                fromdate_db=obj.TS_BigInt
                TS_ON = int(fromdate_db)
                # print("TSONNNNN: ",TS_ON)     
                if obj.TS_OFF_BigInt:
                    todate_db = obj.TS_OFF_BigInt
                    TS_OFFZ = int(todate_db)  
                else:
                    TS_OFFZ = 0    
                # print("TS_OFFZZZZZZZZZZ",TS_OFFZ)

                if obj.status == 0:
                    # print("Status of the alert: ",obj.status)
                                    
                    data.append(0)
                    data1.append(TS_ON)
                    
                    data.append(1)
                    data1.append(TS_ON)
                    
                    data.append(1)
                    data1.append(TS_OFFZ)
                
                    data.append(0)
                    data1.append(TS_OFFZ)
                        
                else:
                    # print("Status of the alert: ",obj.status)
                    data.append(1)
                    data1.append(TS_ON)

        combine_data = [{alerts_info.alert_notice: t, "datetime": s} for t, s in zip(data, data1)]     
        connection.close()  
        # print("DATA:::",combine_data)   
        combine_data.sort(key=lambda item:item['datetime'], reverse=False) 
        datalist = []
        timelist = []        
        for i in combine_data:
            
            key_list = [x for x in i]
            
            value_list = i.get(key_list[0])
            value_list1 = i.get(key_list[1])
            
            datalist.append(value_list)
            timestampss = datetime.fromtimestamp(int(value_list1)).strftime('%Y-%m-%d %H:%M:%S') 
            #timelist.append(timestampss)
            timelist.append(value_list1)

        alertrs = System_Health.objects.filter(GFRID = gfrID, system_offtime__range=[fromdate,todate])
        if alertrs.count() > 0:
            for i in alertrs:
                timestampss = datetime.fromtimestamp(int(i.system_offtime)).strftime('%Y-%m-%d %H:%M:%S') 
                timelist.append(i.system_offtime)
                datalist.append(None)    
        zipped_pairs = zip(datalist, timelist)
        if zipped_pairs:
            ziped = sorted(list(zipped_pairs), key = lambda x: x[1])
        else:
            ziped = []    
        # print("Zipped PAir: ",ziped)
        # print(type(ziped))
        STATUS = []
        TIMES = []
        TIM = []
        if ziped:
            for x,y in zip(ziped, itertools.islice(ziped, 1, None)):
                if y[0] == None:
                    print("values: ",x[1],y[1])
                    ziped.append((x[0],y[1]))
            zipedagain = sorted(list(ziped), key = lambda x: x[1])
            # print("Zipped PAir: ",zipedagain)    
            for x,y in zip(zipedagain, itertools.islice(zipedagain, 1, None)):   
                # print("iterating: ",x[0],x[1],y[0],y[1])
                if x[1] == y[1]:
                    # print("iterating ts equals: ",x[0],x[1],y[0],y[1])
                    if x[1] == None:
                        tempo = x[0]
                        x[0]= y[0]
                        y[0]= tempo
            # print("ZippedAgainnnnn: ",zipedagain)
            for i in zipedagain:
                STATUS.append(i[0])
                TIM.append(i[1])
                
            for i in TIM:
                timestampss = datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S') 
                # print(i,timestampss)
                TIMES.append(timestampss)
            # print("AlertIdependentGraph List:",timelist, datalist)
            # print("Final List:",STATUS,TIMES)
        
        if "HAT_GSM_LOW_SIGNAL" in key_list:
            
            trace = {
                        "x": dt,
                        "y": st2[0],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GSM LOW SIGNAL',
                        "type": 'scatter',
                        "line_color":'#FF0000',
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            gsmfigure = go.Figure(data = data3, layout = layout3)
            gsmfigure.update_yaxes(fixedrange=True,automargin=True)
            # gsmfigure.update_xaxes(tickangle=90)
            gsmfigure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    

            msg2 = "GSM LOW SIGNAL"
            if timelist:
                plot_div = opy.plot(gsmfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2,'dt':f"Date Range Selected : {t} - {tdate}"})    
        elif "HAT_GPS_NO_SIGNAL" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[1],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS NO SIGNAL',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )        

            msg2 = "GPS NO SIGNAL"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2})

        elif "HAT_GPS_OUT_OF_WRK_GEOFENCE" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[2],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS OUT OF WRK',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            
            data3 = [trace]
            layout3 = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True) 
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "GPS OUT OF WRK"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GPS_OUT_OF_STR_GEOFENCE" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[3],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GPS OUT OF STR',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)    
            msg2 = "GPS OUT OF STR"
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg, 'msg2':msg2})
        elif "HAT_GG_FULL_CHARGE_ALERT" in key_list:
            print("TIMELIST, DATALIST: ",timelist, datalist)
            trace = {
                        "x": dt,
                        "y": st2[14],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'FULL CHARGE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            msg2 = "GG FULL CHARGE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_HIGH_VOLTAGE_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[4],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG HIGH VOLTAGE THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "GG HIGH VOLTAGE THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_VOLTAGE_BELOW_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[5],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG VOLTAGE BELOW THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True) 
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "VOLTAGE BELOW THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_SOC_BELOW_THRESHOLD" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[6],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG SOC BELOW THRESHOLD',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )    
            msg2 = "SOC BELOW THRESHOLD"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_GG_HIGH_TEMPERATURE" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[7],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'GG HIGH TEMPERATURE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "HIGH TEMPERATURE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_BME_HIGH_TEMPERATURE" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[8],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'BME HIGH TEMPERATURE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "BME HIGH TEMPERATURE"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_HIGH_VIBRATION" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[9],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'HIGH VIBRATION',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )        
            msg2 = "HIGH VIBRATION"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_MOTION_DETECTED" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[10],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'MOTION DETECTED',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180,margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )        
            msg2 = "MOTION DETECTED"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
        elif "HAT_SYSTEM_RESTART" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[13],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'SYSTEM RESTART',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            datas = [trace]
            layouts = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = datas, layout = layouts)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )       
            msg2 = "SYSTEM RESTART"
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg = ""
            else:
                plot_div = ""
                msg = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg,'msg2':msg2})
                                                                            

        elif "HAT_SYSTEM_ON_POWER_LINE" in key_list:
            trace = {
                        "x": dt,
                        "y": st2[12],
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'SYSTEM ON POWER LINE',
                        "type": 'scatter',
                        "line_color":'#FF0000', 
                        "line_width": 1
                        }
            data3 = [trace]
            layout3 = go.Layout(height= 180, margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
            figure = go.Figure(data = data3, layout = layout3)
            figure.update_yaxes(fixedrange=True,automargin=True)
            figure.update_layout(
                yaxis = dict(
                    tickmode = 'linear',
                    tick0 = 1,
                    dtick = 1
                )
            )

            msg2 = "POWERLINE"
            
            if timelist:
                plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                msg1 = ""
            else:
                plot_div = ""
                msg1 = "No data available in the selected range"
            return render(request, "Staff/alertsPlot.html", context={'plot_div': plot_div,'msg1':msg1,'msg2':msg2})
             
                         
   
                            
   
#-------------------------------------------------------------------------------------------------------------------------------------------------------
                            
   
   
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def systemhealthPlotly(request, tracker,todate,fromdate):
    print("welcome graph data")
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id    
    
    if todate == '0':
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz) + timedelta(days=1)
            last3days = datetime.now(tz) - timedelta(days=3)
            todate = int(now.timestamp())
            fromdate =int(last3days.timestamp())          
           
    else :
            datetimerange = request.POST.get('datetimes')
            print("DATETIME: ",datetimerange)
            print("DATETIME TYPE",type(datetimerange))
            dates = datetimerange.split("-")            
            t =  dates[0].strip()
            fdate = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            fromdate = int(fdate.timestamp())
            #fromdate1 = fdate - timedelta(hours=5,minutes=30)
            #fromdate = int(fromdate1.timestamp())
            tdate = dates[1].strip()
            now = datetime.strptime(tdate, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            todate = int(now.timestamp())
            #nowdate = now - timedelta(hours=5,minutes=30)
            #todate = int(nowdate.timestamp())
            
    alerts_info = AlertNotification.objects.all()
    alertslen = alerts_info.count()  
    print("ALERTS LENGTHHHH: ",alertslen) 
    alertslength = range(1,alertslen)            
    return render(request, 
                        'Staff/systemHealth.html',
                        {
                             'tracker':tracker,'gfrid':gfrid,'todate':todate,'fromdate':fromdate, 'alertslen':alertslength
                        })         
    
#--------------------------------------------------------------------------------------  


def sysHealthPlotly(request,gfrID,tracker,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
         #System Health
        dated =[]
        status = []
        tz = pytz.timezone("Asia/Calcutta")
        #now = datetime.now(tz) + timedelta(hours=5,minutes=30)
        now = datetime.now(tz) + timedelta(days=1)
        tt = str(now.timestamp())
        y = tt.split(".")
        last7days = datetime.now(tz) - timedelta(days=7)
        t = str(last7days.timestamp())
        x = t.split(".")
        
 
        ret = SystemHealth(request, gfrID)
        print("REST", ret)
        json_body = json.dumps(ret)
        data = json.loads(json_body)
        for i in range(0, len(data)):
            timestampss = datetime.fromtimestamp(int(data[i]['date'])).strftime('%Y-%m-%d %H:%M:%S %p') 
            dated.append(timestampss)
            status.append(data[i]['status'])
	
        print(dated, status)
        print("YESTEST")
        trace1 = {
                        "x": dated,
                        "y": status,
                        "line": {"shape": 'hv'},
                        "mode": 'lines',
                        "name": 'value',
                        "type": 'scatter',
                        "line_color":'#008000', 
                        "line_width": 5
                        }

        data1 = [trace1]
        #data=go.Data([trace1])
        layout1=go.Layout(title="<b>System Health</b>")
        figure1=go.Figure(data=data1,layout=layout1)
        figure1.update_xaxes(tickangle = 90)    
        figure1.update_yaxes(fixedrange=True)     
        figure1.update_layout( yaxis = dict( tickmode = 'array', tickvals = [0,1]))
        figure1.update_layout(
        boxmode='group', 
        boxgap=0.25,
        boxgroupgap=0.25,
        height=300, 
        xaxis=dict(gridcolor='#fff')
        )
        
        if dated:
            plot_System = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
            msgSystem = ""
        else:
            plot_System = ""
            msgSystem = "No data available in the selected range"


        # ALERTSSSS
        print("TIME RANGE: ",x[0],y[0])

        alert = Alerts.objects.filter(GFRID = gfrID, status = 1, TS_BigInt__range=[x[0],y[0]])
        alertbefore = Alerts.objects.filter(GFRID = gfrID, status = 1, TS_BigInt__lte=int(x[0]))
        
        dated1 =[]
        status1 = []
        print(alert.count(), "Alert at ",x[0],y[0])
        if alert.count() > 0:
            for i in alert:
                print("AlertNotification ID: ",i.alertNotify_id)
                timestampss = datetime.fromtimestamp(int(i.TS_BigInt)).strftime('%Y-%m-%d %H:%M:%S %p') 
                dated1.append(int(i.TS_BigInt))
                status1.append(1)
                dated1.append(int(y[0]))
                status1.append(1) 
        else:
            if alertbefore.count() > 0:
                for i in alertbefore:
                   print("AlertNotification ID: ",i.alertNotify_id)
                   print("Doubt parttttt")  
                   dated1.append(int(x[0]))
                   status1.append(1)         
                   dated1.append(int(y[0]))
                   status1.append(1)
            else:
                dated1.append(int(x[0]))
                status1.append(0)         
                dated1.append(int(y[0]))
                status1.append(0) 
        alertrs = System_Health.objects.filter(GFRID = gfrID, system_offtime__range=[x[0],y[0]])
        if alertrs.count() > 0:
            for i in alertrs:
                timestampss = datetime.fromtimestamp(int(i.system_offtime)).strftime('%Y-%m-%d %H:%M:%S %p') 
                dated1.append(i.system_offtime)
                status1.append(None)    
        zipped_pairs = zip(status1, dated1)
        ziped = sorted(list(zipped_pairs), key = lambda x: x[1])
        print("Zipped PAir: ",ziped)
        print(type(ziped))
        STATUS = []
        TIMES = []
        TIM = []
        for i in ziped:
            STATUS.append(i[0])
            TIM.append(i[1])
        
        for i in TIM:
            timestampss = datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S %p') 
            print(i,timestampss)
            TIMES.append(timestampss)
        print("AlertGraph List:",dated1, status1)
        print("Final List:",STATUS,TIMES)
        #consolidated alerts
        trace2 = {
                    "x": TIMES,
                    "y": STATUS,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter',
                    "line_color":'#FF0000', 
                    "line_width": 3
                    }

        data2 = [trace2]
        #data=go.Data([trace1])
        layout2=go.Layout(title="<b>Alerts Summary</b>")
        figure2=go.Figure(data=data2,layout=layout2)
        figure2.update_xaxes(tickangle = 90)
        figure2.update_yaxes(fixedrange=True)  
        figure2.update_traces(connectgaps=False)    
        figure2.update_layout( yaxis = dict( tickmode = 'array', tickvals = [0,1]))  
        figure2.update_layout(
        boxmode='group', 
        boxgap=0.25,
        boxgroupgap=0.25,
        height=300, 
        xaxis=dict(gridcolor='#FFF'))                
        if dated1:
    	    plot_alert = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
    	    msgAlert = ""
        else:
            plot_alert = ""
            msgAlert = "No data available in the selected range"

        #Generate Alerts List
        alert = Alerts.objects.filter(GFRID=gfrID,status=1).distinct('alertNotify')
        alertname = []
        times = []
        alertList = []
        for i in alert:
            print("Alert NAME: ",i.alertNotify_id, i.alert)
            alertName = AlertNotification.objects.get(id=i.alertNotify_id)
            alertname.append(alertName.alert_notice)
            timestampss = datetime.fromtimestamp(int(i.TS_BigInt)).strftime('%Y-%m-%d %H:%M:%S %p')
            times.append(timestampss)
        alertList = [{'alertname': a, 'times': t} for a, t in zip(alertname, times)]    
        print("AlertLIST names: ",alertList)
        startdate = datetime.fromtimestamp(int(x[0])).strftime('%Y-%m-%d %H:%M:%S %p') 
        enddate = datetime.fromtimestamp(int(y[0])).strftime('%Y-%m-%d %H:%M:%S %p') 
        print(startdate,enddate)

        return render(request, 'Staff/sysHealthPlot.html',{
                            'gfrID':gfrID, 'tracker':tracker, 'startdate':startdate, 'enddate':enddate, 'todate':todate, 'fromdate':fromdate, 'plot_System':plot_System, 'msgSystem':msgSystem, 'plot_alert':plot_alert, 'msgAlert':msgAlert, 'alertList':alertList
                        })        
#--------------------------------------------------------------------------------------        

   
   
#---------------------------------------------------------------------------------------------------
def graphsUpdate(request,gfrID,version,function,sensor,todate,fromdate):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        gfrid = GFRID.objects.get(id=gfrID)
        tracker = gfrid.tracker
        #customer
        cust = Cust_Batches_Trackers.objects.get(tracker=tracker)
        custid = cust.customerID
        custs = Customers.objects.get(id = custid)
        newdb = custs.new_database
        custcode = custs.Customer_Identifier
        if newdb == True:
            dcon = createNewDB(connection, custcode)
            #print("Test", dcon)
        else:
            dcon = connection
            #print("Check", dcon)
    
        if todate == '0.0':
                print("hello")
                tz = pytz.timezone("Asia/Calcutta")
                now = datetime.now(tz)

                last3days = datetime.now(tz) - timedelta(days=3)
                nextday = datetime.now(tz) + timedelta(days=1)
                todate = str(int(nextday.timestamp()))
                fromdate =str(int(last3days.timestamp()))
                print("TODATTEEEE: ",todate)
                print("FROMMMMMMMMDATE: ",fromdate)
                
        else :
                print("hello1")
                print("TOOOOOODATE: ",todate,type(todate))
                print("FROOOOOMDATE: ",fromdate)
                todates = todate.split(".")
                todate = todates[0]
                fromdates = fromdate.split(".")
                fromdate = fromdates[0]
            
        print("GRAPHSUPDATE")
        print("Sensor: ",sensor)
        print("VERS:",version)
        vertable = version.lower()+"_"+sensor.lower()
        print("table:::::::::::::::: ",vertable)
        if function == 'accPlot':
            qry = "select id,gfrid,acc_x,acc_y,acc_z,gyr_x,gyr_y,gyr_z,mag_x,mag_y,mag_z,temp,ts,last_modified,roll,pitch from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
        elif function == 'bmp':
            qry = "select id,gfrid,temp,pres,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'    order by id"
        elif function == 'stc':
            qry = "select id,gfrid,voltage,current,temp,soc,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
        elif function == 'vibration':
            qry = "select id,gfrid,level,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
        elif function == 'lcell':
            qry = "select id,gfrid,last_modified,perwght,avgwght,wghtlist,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"
        elif function == 'bme':
            qry = "select id,gfrid,temp,pres,humid,last_modified,ts from "+vertable+" where gfrid="+gfrID+" AND ts BETWEEN '"+fromdate+"' AND '"+todate+"' order by id"
        elif function == 'ltc':
            qry = "select id,gfrid,voltage,current,temp,soc,last_modified,ts from "+vertable+" where gfrid="+gfrID+" and ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"        
        else:
            qry = "select id,gfrid,voltage,current,temp,soc,last_modified,ts from "+vertable+" where gfrid="+gfrID+" and ts BETWEEN '"+fromdate+"' AND '"+todate+"'  order by id"            
        tz = pytz.timezone("Asia/Calcutta")
        with dcon.cursor() as cursor:
                cursor.execute(qry)
                dcon.commit()
                objs = cursor.fetchall()
                json_data = []
                xaxis = []
                yaxix = []
                accx_yaxis,accy_yaxis,accz_yaxis,gyrx_yaxis,gyry_yaxis,gyrz_yaxis,magx_yaxis,magy_yaxis,magz_yaxis,temp_yaxis,roll_yaxis,pitch_yaxis = ([] for i in range(12))
                if function == 'accPlot':
                    for obj in objs:
                        timestampss = datetime.fromtimestamp(int(obj[12])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        accx_yaxis.append(obj[2])
                        accy_yaxis.append(obj[3])
                        accz_yaxis.append(obj[4])
                        gyrx_yaxis.append(obj[5])
                        gyry_yaxis.append(obj[6])
                        gyrz_yaxis.append(obj[7])
                        magx_yaxis.append(obj[8])
                        magy_yaxis.append(obj[9])
                        magz_yaxis.append(obj[10])
                        temp_yaxis.append(obj[11])
                        roll_yaxis.append(obj[14])
                        pitch_yaxis.append(obj[15])
                    print("XAXISSS: ",xaxis)    
                    trace1 = go.Scatter(x = xaxis, y = accx_yaxis, mode = 'lines', name= 'AccX')
                    trace2 = go.Scatter(x = xaxis, y = accy_yaxis, mode = 'lines', name= 'AccY')
                    trace3 = go.Scatter(x = xaxis, y = accz_yaxis, mode = 'lines', name= 'AccZ')
                    data = [trace1,trace2,trace3]
                    layout = go.Layout(yaxis_title="Accelerometer", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=25), font=dict(size=10))
                    accfigure = go.Figure(data = data, layout = layout)
                    accfigure.update_yaxes(fixedrange=True)
                                        
                    trace4 = go.Scatter(x = xaxis, y = gyrx_yaxis, mode = 'lines', name= 'GyrX')
                    trace5 = go.Scatter(x = xaxis, y = gyry_yaxis, mode = 'lines', name= 'GyrY')
                    trace6 = go.Scatter(x = xaxis, y = gyrz_yaxis, mode = 'lines', name= 'GyrZ')
                    data1 = [trace4,trace5,trace6]
                    layout1 = go.Layout(yaxis_title="Gyroscope", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=25), font=dict(size=10))
                    gyrfigure = go.Figure(data = data1, layout = layout1)
                    gyrfigure.update_yaxes(fixedrange=True)
                    
                    trace7 = go.Scatter(x = xaxis, y = magx_yaxis, mode = 'lines', name= 'MagX')
                    trace8 = go.Scatter(x = xaxis, y = magy_yaxis, mode = 'lines', name= 'MagY')
                    trace9 = go.Scatter(x = xaxis, y = magz_yaxis, mode = 'lines', name= 'MagZ')
                    data2 = [trace7,trace8,trace9]
                    layout2 = go.Layout(yaxis_title="Magnetometer", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=25), font=dict(size=10))
                    magfigure = go.Figure(data = data2, layout = layout2)
                    magfigure.update_yaxes(fixedrange=True)                    

                    trace10 = go.Scatter(x = xaxis, y = temp_yaxis, mode = 'lines', name= 'Temp')
                    data3 = [trace10]
                    layout3 = go.Layout(yaxis_title="Temperature", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
                    tempfigure = go.Figure(data = data3, layout = layout3)
                    tempfigure.update_yaxes(fixedrange=True)                                        
                    
                    trace11 = go.Scatter(x = xaxis, y = roll_yaxis, mode = 'lines', name= 'Roll')
                    data4 = [trace11]
                    layout4 = go.Layout(yaxis_title="Roll", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
                    rollfigure = go.Figure(data = data4, layout = layout4)
                    rollfigure.update_yaxes(fixedrange=True)                                        
                                        
                    trace12 = go.Scatter(x = xaxis, y = pitch_yaxis, mode = 'lines', name= 'Pitch')
                    data5 = [trace12]
                    layout5 = go.Layout(yaxis_title="Pitch", height= 180, width= 960, paper_bgcolor='rgb(233,233,233)', margin=go.layout.Margin(l=0, r=0, b=0, t=20), font=dict(size=10))
                    pitchfigure = go.Figure(data = data5, layout = layout5)
                    pitchfigure.update_yaxes(fixedrange=True)                                        
                                        
                    if xaxis:
                        plot_div_acc = opy.plot(accfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg1 = ""
                    else:
                        plot_div_acc = ""
                        msg1 = "No data available in the selected range"
                        
                    if xaxis:
                        plot_div_gyr = opy.plot(gyrfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg2 = ""
                    else:
                        plot_div_gyr = ""
                        msg2 = "No data available in the selected range"

                    if xaxis:
                        plot_div_mag = opy.plot(magfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg3 = ""
                    else:
                        plot_div_mag = ""
                        msg3 = "No data available in the selected range" 

                    if xaxis:
                        plot_div_temp = opy.plot(tempfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg4 = ""
                    else:
                        plot_div_temp = ""
                        msg4 = "No data available in the selected range"
                        
                    if xaxis:
                        plot_div_roll = opy.plot(rollfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg5 = ""
                    else:
                        plot_div_roll = ""
                        msg5 = "No data available in the selected range"
                    
                    if xaxis:
                        plot_div_pitch = opy.plot(pitchfigure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg6 = ""
                    else:
                        plot_div_pitch = ""
                        msg6 = "No data available in the selected range"
		            
                    
                    return render(request, "Staff/multiline_accPlot.html", context={'plot_div_acc': plot_div_acc,'plot_div_gyr': plot_div_gyr,'plot_div_mag': plot_div_mag,'plot_div_temp': plot_div_temp,'plot_div_roll': plot_div_roll,'plot_div_pitch': plot_div_pitch,'msg1':msg1,'msg2':msg2,'msg3':msg3,'msg4':msg4,'msg5':msg5,'msg6':msg6})
                    
                    
                elif function == 'vibration':   

                    for obj in objs:
                        print(obj, "OBJ")
                        timestampss = datetime.fromtimestamp(int(obj[4])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        yaxix.append(obj[2])


                    trace1 = {
                    "x": xaxis,
                    "y": yaxix,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data = [trace1]
                    #data=go.Data([trace1])
                    layout=go.Layout(xaxis={'title':'timestamp'}, yaxis={'title':'level'})
                    figure=go.Figure(data=data,layout=layout)
                    figure.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg = ""
                    else:
                        div = ""
                        msg = "No data available in the selected range"
                    return render(request, "Staff/vibPlotDetails.html", context={'plot_div': div, "msg":msg})

                elif function == 'lcell':   
                   
                    for obj in objs:
                        print(obj, "OBJ")
                        timestampss = datetime.fromtimestamp(int(obj[6])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        
                        if obj[3] > 100: 
                          yaxix.append(100)
                        elif obj[3] < 0:
                          yaxix.append(0)
                        else:
                          yaxix.append(obj[3])


                    trace1 = {
                    "x": xaxis,
                    "y": yaxix,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data = [trace1]
                    #data=go.Data([trace1])
                    layout=go.Layout(xaxis={'title':'timestamp'}, yaxis={'title':'Percentage Weight'})
                    figure=go.Figure(data=data,layout=layout)
                    figure.update_yaxes(fixedrange=False)                    
                    if xaxis:
                        div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg = ""
                    else:
                        div = ""
                        msg = "No data available in the selected range"
                    return render(request, "Staff/lcellPlotDetails.html", context={'plot_div': div, "msg":msg})
                        
                elif function == 'bmp':  
                    yaxix1_temp = []
                    yaxix2_pres = [] 
                    for obj in objs:
                        timestampss = datetime.fromtimestamp(int(obj[5])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        yaxix1_temp.append(obj[2])
                        yaxix2_pres.append(obj[3])
                    
                    trace1 = {
                    "x": xaxis,
                    "y": yaxix1_temp,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data1 = [trace1]
                    #data=go.Data([trace1])
                    layout1=go.Layout( xaxis={'title':'timestamp'}, yaxis={'title':'temperature'})
                    figure1=go.Figure(data=data1,layout=layout1)
                    figure1.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_temp = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg1 = ""
                    else:
                        div_temp = ""
                        msg1 = "No data available in the selected range"



                    trace2 = {
                    "x": xaxis,
                    "y": yaxix2_pres,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data2 = [trace2]
                    #data=go.Data([trace1])
                    layout2=go.Layout( xaxis={'title':'timestamp'}, yaxis={'title':'pressure'})
                    figure2=go.Figure(data=data2,layout=layout2)
                    figure2.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_pres = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg2 = ""
                    else:
                        div_pres = ""
                        msg2 = "No data available in the selected range"

                    return render(request, "Staff/bmpPlotDetails.html", context={'plot_temp': div_temp, "msg1":msg1,'plot_pres': div_pres, "msg2":msg2,"fromdate":fromdate, "todate":todate})  
      
                elif function == 'bme':  
                    yaxix1_temp = []
                    yaxix2_pres = [] 
                    yaxix3_humid =[]
                    for obj in objs:
                        timestampss = datetime.fromtimestamp(int(obj[6])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        yaxix1_temp.append(obj[2])
                        yaxix2_pres.append(obj[3])
                        yaxix3_humid.append(obj[4])
                    trace1 = {
                    "x": xaxis,
                    "y": yaxix1_temp,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data1 = [trace1]
                    #data=go.Data([trace1])
                    layout1=go.Layout(title="Temperature", xaxis={'title':'timestamp'}, yaxis={'title':'temp'})
                    figure1=go.Figure(data=data1,layout=layout1)
                    figure1.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_temp = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg1 = ""
                    else:
                        div_temp = ""
                        msg1 = "No data available in the selected range"



                    trace2 = {
                    "x": xaxis,
                    "y": yaxix2_pres,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data2 = [trace2]
                    #data=go.Data([trace1])
                    layout2=go.Layout(title="Pressure", xaxis={'title':'timestamp'}, yaxis={'title':'temp'})
                    figure2=go.Figure(data=data2,layout=layout2)
                    figure2.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_pres = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg2 = ""
                    else:
                        div_pres = ""
                        msg2 = "No data available in the selected range"

                    
                    trace3 = {
                    "x": xaxis,
                    "y": yaxix3_humid,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data3 = [trace3]
                    #data=go.Data([trace1])
                    layout3=go.Layout(title="Humid", xaxis={'title':'timestamp'}, yaxis={'title':'Humid'})
                    figure3=go.Figure(data=data3,layout=layout3)
                    figure3.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_humid = opy.plot(figure3, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg3 = ""
                    else:
                        div_humid = ""
                        msg3 = "No data available in the selected range"

                    return render(request, "Staff/bmePlotDetails.html", context={'plot_temp': div_temp, "msg1":msg1,'plot_pres': div_pres, "msg2":msg2,'plot_humid': div_humid, "msg3":msg3,"fromdate":fromdate, "todate":todate})                    

                elif function == 'ltc':  

                    yaxix1_voltage = []
                    yaxix2_current = [] 
                    yaxix3_temp = []
                    yaxix3_soc = []

                    for obj in objs:
                        timestampss = datetime.fromtimestamp(int(obj[7])).strftime('%Y-%m-%d %H:%M:%S %p') 
                        xaxis.append(timestampss)
                        yaxix1_voltage.append(obj[2])
                        yaxix2_current.append(obj[3])
                        yaxix3_temp.append(obj[4])
                        yaxix3_soc.append(obj[5])

                    trace1 = {
                    "x": xaxis,
                    "y": yaxix1_voltage,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data1 = [trace1]
                    #data=go.Data([trace1])
                    layout1=go.Layout(title="Voltage", xaxis={'title':'timestamp'}, yaxis={'title':'voltage'})
                    figure1=go.Figure(data=data1,layout=layout1)
                    figure1.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_vol = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg1 = ""
                    else:
                        div_vol = ""
                        msg1 = "No data available in the selected range"



                    trace2 = {
                    "x": xaxis,
                    "y": yaxix2_current,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data2 = [trace2]
                    #data=go.Data([trace1])
                    layout2=go.Layout(title="Current", xaxis={'title':'timestamp'}, yaxis={'title':'current'})
                    figure2=go.Figure(data=data2,layout=layout2)
                    figure2.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_cur = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg2 = ""
                    else:
                        div_cur = ""
                        msg2 = "No data available in the selected range"

                    
                    trace3 = {
                    "x": xaxis,
                    "y": yaxix3_temp,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data3 = [trace3]
                    #data=go.Data([trace1])
                    layout3=go.Layout(title="Temperature", xaxis={'title':'timestamp'}, yaxis={'title':'temp'})
                    figure3=go.Figure(data=data3,layout=layout3)
                    figure3.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_temp = opy.plot(figure3, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg3 = ""
                    else:
                        div_temp = ""
                        msg3 = "No data available in the selected range"



                    trace4 = {
                    "x": xaxis,
                    "y": yaxix3_soc,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter'
                    }

                    data4 = [trace4]
                    #data=go.Data([trace1])
                    layout4=go.Layout(title="SOC", xaxis={'title':'timestamp'}, yaxis={'title':'soc'})
                    figure4=go.Figure(data=data4,layout=layout4)
                    figure4.update_yaxes(fixedrange=True)                    
                    if xaxis:
                        div_soc = opy.plot(figure4, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                        msg4 = ""
                    else:
                        div_soc = ""
                        msg4 = "No data available in the selected range"
                        

                    return render(request, "Staff/socPlotDetails.html", context={'plot_vol': div_vol, "msg1":msg1,'plot_cur': div_cur, "msg2":msg2,'plot_temp': div_temp, "msg3":msg3,'plot_soc': div_soc, "msg4":msg4,"fromdate":fromdate, "todate":todate})                    

		                    
                                      
        dcon.close()  
        
        return JsonResponse(json_data, safe=False)         
    
    
#-------------------------------------------------------------------------------------------------------------------------------------------------------
def sysHealth(request, tracker):
    gfr = GFRID.objects.get(tracker = tracker)
    gfrid = gfr.id
    dated =[]
    status = []
    tz = pytz.timezone("Asia/Calcutta")
    now = datetime.now(tz)
    tt = str(now.timestamp())
    y = tt.split(".")
    last7days = datetime.now(tz) - timedelta(days=7)  
    t = str(last7days.timestamp())
    x = t.split(".")
    
    ret = SystemHealth(request, gfrid)
    json_body = json.dumps(ret)
    data = json.loads(json_body)

    alert = Alerts.objects.filter(GFRID = gfrid, status = 1, TS_BigInt__range=[x[0],y[0]])

    print(len(data))
    dated1 =[]
    status1 = []
    
    for i in range(0, len(data)):
        timestampss = datetime.fromtimestamp(int(data[i]['date'])).strftime('%Y-%m-%d %H:%M:%S %p') 
        dated.append(timestampss)
        status.append(data[i]['status'])
    
    print(alert.count(), "Alert")
    for i in alert:
    	timestampss = datetime.fromtimestamp(int(i.TS_BigInt)).strftime('%Y-%m-%d %H:%M:%S %p') 
    	dated1.append(timestampss)
    	status1.append(1)
    
    print(dated, status)
    trace1 = {
                    "x": dated,
                    "y": status,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter',
                    "line_color":'#008000', 
                    "line_width": 5
                    }

    data1 = [trace1]
    #data=go.Data([trace1])
    layout1=go.Layout(title="System State", xaxis={'title':'timestamp'}, yaxis={'title':'status'})
    figure1=go.Figure(data=data1,layout=layout1)
    figure1.update_xaxes(tickangle = 90, dtick = 60*60*1000)    
    figure1.update_yaxes(fixedrange=True)     
    figure1.update_layout( yaxis = dict( tickmode = 'array', tickvals = [0,1]))
    figure1.update_layout(
    boxmode='group', 
    boxgap=0.25,
    boxgroupgap=0.25,
    height=400, 
    xaxis=dict(gridcolor='#fff')
    

)
    
    if dated:
        plot_div = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
        msg1 = ""
    else:
        plot_div = ""
        msg1 = "No data available in the selected range"
    
    #consolidated alerts
    trace2 = {
                    "x": dated1,
                    "y": status1,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter',
                    "line_color":'#FF0000', 
                    "line_width": 5
                    }

    data2 = [trace2]
    #data=go.Data([trace1])
    layout2=go.Layout(title="Alerts", xaxis={'title':'timestamp'}, yaxis={'title':'status'})
    figure2=go.Figure(data=data2,layout=layout2)
    figure2.update_xaxes(tickangle = 90)
    figure2.update_yaxes(fixedrange=True)      
    figure2.update_layout( yaxis = dict( tickmode = 'array', tickvals = [0,1]))  
    figure2.update_layout(
    boxmode='group', 
    boxgap=0.25,
    boxgroupgap=0.25,
    height=400, 
    xaxis=dict(gridcolor='#FF0000'))                
    if dated1:
        plot_alert = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
        msg2 = ""
    else:
        plot_alert = ""
        msg2 = "No data available in the selected range"
    
    return render(request, "Staff/sysHealthSnapshot.html", context={'plot_div': plot_div, "msg":msg1, 'plot_alert': plot_alert, "msg1":msg2})  
    
                      
                  
                                      
    dcon.close()   
    return JsonResponse(json_data, safe=False)        
#-------------------------------------------------------------------------------------------------------------------------------------------------------

def systemHealth(request,tracker):
    if 'staff_username' not in request.session:    
        return redirect('/Staff/login_auth/')
    else:        
	    custBatch = Cust_Batches_Trackers.objects.get(tracker = tracker)
	    batch = Batches_Trackers.objects.get(id = custBatch.batchId)

	    gfrid = GFRID.objects.get(tracker=tracker)

	    return render(request, 
		                'Staff/systemHealth.html',
                        {
                             'tracker':tracker,'gfrid':gfrid, 'custid':custBatch.customerID,'custBTID':custBatch.id
                        }) 
        
#-----------------------------------------------------------------------------------------                      

def SystemHealth(request, gfrID):
    print(gfrID, "gfrID")
    tz = pytz.timezone("Asia/Calcutta")
    now = datetime.now(tz)
    tt = str(now.timestamp())
    y = tt.split(".")
    last7days = datetime.now(tz) - timedelta(days=7)  
    t = str(last7days.timestamp())
    x = t.split(".")
    
            
    arr = []
    timearr = []
    statarr = []
    
    for i in range(6, 0, -1):
        last7days = datetime.now(tz) - timedelta(days=i)  
        t1 = str(last7days.timestamp())
        x1 = t1.split(".")
        my_time = time.strftime('%Y-%m-%d', time.localtime(int(x1[0])))
            
        arr.append(my_time)

    y_time = time.strftime('%Y-%m-%d', time.localtime(int(y[0])))
    arr.append(y_time)
            
    
    SHT = System_Health.objects.filter(GFRID = gfrID, system_ontime__range=[x[0],y[0]]) | System_Health.objects.filter(GFRID = gfrID, system_offtime__range=[x[0],y[0]])
    SH = SHT.order_by('system_ontime')
    for x in SH:
        if x.system_offtime:
            offtime = time.strftime('%Y-%m-%d', time.localtime(x.system_offtime))
            if offtime in arr:
                timearr.append(x.system_offtime)
                statarr.append(0)
            
        if x.system_ontime:
            ontime = time.strftime('%Y-%m-%d', time.localtime(x.system_ontime))
            if ontime in arr:
                timearr.append(x.system_ontime)
                statarr.append(1)
            

    score_titles = [{'date': a, 'status': t} for a, t in zip(timearr, statarr)]
    # Printing in JSON format
    jsonArr =json.dumps(score_titles)

    #------------------------------------------------------#

    jsonObject = json.loads(jsonArr)
    darr = []
    sarr = []

    for i in range(len(jsonObject)):
        jdat = jsonObject[i]['date']
        js_time = time.strftime('%Y-%m-%d', time.localtime(jdat))
        darr.append(js_time)
        sarr.append(jsonObject[i]['status'])
        #print(js_time, "js_time")
    
    print(darr, "DATE")
    print(sarr, "STATUS")

   
    for s in arr:
        if s in darr:
            print("YESY", s)
        else:
            datetime_object = datetime.strptime(s, '%Y-%m-%d')
            pre = datetime_object - timedelta(1)
            d1 = str(pre.timestamp())
            d2 = d1.split(".")

            #for old data
            pred2= datetime_object - timedelta(2)
            dd1 = str(pred2.timestamp())
            dd2 = dd1.split(".")

            d3 = str(datetime_object.timestamp()).split(".")

            jj_time = time.strftime('%Y-%m-%d', time.localtime(int(d2[0])))


            if jj_time in darr:
                res = darr.index(jj_time)
                # finding the last occurrence
                if darr.count(jj_time)>1:
                    res = max(index for index, item in enumerate(darr) if item == jj_time)
                else:
                    res = darr.index(jj_time)
                
                
                
                if str(res).isdigit():
                    vaa= sarr[res]
                    score_titles.append(({"date":int(d3[0]), "status":vaa}))
                else:
                    score_titles.append(({"date":int(d3[0]), "status":0}))
            else:

                SHT = System_Health.objects.filter(GFRID = gfrID, system_ontime__lte=int(d3[0])) | System_Health.objects.filter(GFRID = gfrID, system_offtime__lte=int(d3[0]))
                SHTT = SHT.order_by('system_ontime')
                
                SHFst = SHTT.first()
                if SHFst:
                    ontime  = SHFst.system_ontime
                    offtine  = SHFst.system_offtime
                    if ontime:
                        stst = 1
                    else:
                        stst = 0
                else:
                    stst = 0
                
                score_titles.append(({"date":int(d3[0]), "status":stst}))

    print("score_titlesscore_titles", score_titles)
    score_titles.sort(key=lambda x: x["date"])
    #return JsonResponse(score_titles, safe=False)
    return score_titles

from django.contrib.staticfiles.storage import staticfiles_storage
#---------------------------------------------------------------------------------------------------------------------
def filter_map_folium(request, tracker, sensor, custBTID, custid):
    if 'staff_username' not in request.session:
        return redirect('/Staff/login_auth/')
    else:
        datetimerange = request.POST.get('datetimes')
        # print(datetimerange, "DATE RAN")
        objects = Cust_Batches_Trackers.objects.get(id = custBTID)
        batchID = objects.batchId

        batches = Batches_Trackers.objects.get(id = batchID)
        # print("VERSION:::::",batches.version_id)
        versionID = batches.version_id
        verCode = Version.objects.get(id = versionID).code
        # print("VERSIONCODE: ",verCode, sensor)    
        
        tblename = verCode.lower()+"_"+sensor.lower()
        if datetimerange == None:
            print("hello")
            tz = pytz.timezone("Asia/Calcutta")
            now = datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')
            last3day = (datetime.now(tz) - timedelta(days=2)).strftime('%Y-%m-%d %H:%M:%S')

            last3day = datetime.strptime(last3day, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))
            now = datetime.strptime(now, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

            last3days1 = last3day.timestamp()
    
            nowdate1 = now.timestamp()

            last3days =  str(round(last3days1))
            nowdate = str(round(nowdate1))
        
        else :
            print("hello1")
            dates = datetimerange.split("-")
            print("DATERANGE:: ",dates)
            t =  dates[0].strip()
            t = parse(t).strftime('%Y/%m/%d %H:%M:%S')
            last3day = datetime.strptime(t, '%Y/%m/%d %H:%M:%S').astimezone(pytz.timezone("UTC"))
            last3days1 = last3day.timestamp()
            # y = last3days1.split(".")
            last3days = str(round(last3days1))
            nw = dates[1].strip()
            nw = parse(nw).strftime('%Y/%m/%d %H:%M:%S')
            now = datetime.strptime(nw, '%Y/%m/%d %H:%M:%S').astimezone(pytz.timezone("UTC"))
            nowdate1 = now.timestamp()
            # x = nowdate1.split(".")
            nowdate = str(round(nowdate1))
            # print ("NOW: ",nowdate)
        #print(type(last3days))
        # print(last3days)
        # print("lllll",last3days)
        # print("nnnn",nowdate)
        objCust = Customers.objects.get(id = custid)
        custlat = objCust.Lat
        custlon = objCust.Lon
        j = 0


        tble = checkTableExists(connection, "SuperAdmin_gfrid")
        if tble == True:
            # print("Table")
            objectsAll = GFRID.objects.filter(tracker = tracker)
            objects = objectsAll.last()
            vcode = objects.version.code
            objcount = objectsAll.count()
            gfrid=objects.id
            j = 0
            # print("objcount: ",objcount)
            if objcount > 0 :
                objCust = Customers.objects.get(id = custid)
                new_DB = objCust.new_database
                custcode = objCust.Customer_Identifier

                conn1_new = ""
                if new_DB == True:    # CHECK IF ANOTHER DB
                    conn = createNewDB(connection, custcode)
                    conn1_new = conn
                else:
                    conn1_new = connection
                # print("CONNECTION NAME******",conn1_new)    


                if "gps" in sensor.lower():
                    selectqry = "SELECT * FROM "+tblename+" WHERE gfrid = "+str(gfrid)+" AND  ts BETWEEN  '"+ last3days +"' AND '"+ nowdate  +"' ORDER BY id"
                    # print(selectqry)
                    selectFDqry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tblename+"'  and table_schema = 'public'"
                    dbf= GetFieldValues(conn1_new,selectFDqry, tblename)
                    dbv= selectDBFullValues(conn1_new,selectqry, tblename)
                    # print(len(dbv), dbv)

		
                    dictionary = []
                    lat_Arr = []
                    lon_Arr = []
                    tss = []
                    if len(dbv) >0 :
                        for i in range(len(dbv)):
                            dictionary.append(dict(zip(dbf, dbv[i])))
                        
                        for j in range(len(dictionary)):
                            dicts = dictionary[j]

                            lat1 = str(dicts[' lat']).strip()
                            lat = lat1[:-1]

                            lon1 = str(dicts[' lon']).strip()
                            lon = lon1[:-1]
                            # print(lon, type(lon))


                            tts = str(dicts[' ts']).strip()
                            

                            lat_Arr.append(float(lat))
                            lon_Arr.append(float(lon))
                            tss.append(int(tts))
                                               		
	    
            # Generate map
            print(tss)

            #place_lat = [37.4601928, 37.4593108, 37.4641108, 37.4611508]
            #place_lng = [126.4406957, 126.4432957, 126.4476917, 126.4423957]
            points = []

            if len(lat_Arr) > 0:
                m = folium.Map(location=[lat_Arr[0],lon_Arr[0]], width="%100", height="%100",zoom_start=12, scrollWheelZoom=False, tiles="OpenStreetMap")
                for i in range(len(lat_Arr)):
                    points.append([lat_Arr[i], lon_Arr[i]])
                    
                for index,lat in enumerate(lat_Arr):
                    # print(tss[index], "aapend")
                    sts = datetime.fromtimestamp(int(tss[index])).strftime("%B %d, %Y, %H:%M:%S %p")                
                    iframe = folium.IFrame("Lat : "+str(lat) + '<br>' + "Lon : "+str(lon_Arr[index])+ "<br>TS :"+str(sts))
                    popup = folium.Popup(iframe, min_width=300, max_width=300,  max_height=80)
                    
                    icon_url = "http://164.52.200.223:8004/static/SuperAdmin/dist/img/marker-icon-2x.png"  
                    iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
                    iconurl = str(iicon) +"SuperAdmin/dist/img/marker-icon-2x.png"               		
                    icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
                    
                    if (index == 0) or (len(lat_Arr)-1 == index):
                        folium.Marker([lat, lon_Arr[index]], popup=popup, icon = folium.Icon(color='blue', icon="info-sign")).add_to(m)
                    else:
                        folium.Marker([lat, lon_Arr[index]], popup=popup, icon = icon).add_to(m)            		
                    folium.PolyLine(points, color='green', dash_array='10').add_to(m)
                ms=m._repr_html_()
            else:
            	ms ="No data"
            return render(request,
                                                'Staff/filter_map_folium.html', {'my_map':ms, "tracker":tracker, "sensor":sensor, "custBTID":custBTID, "custid":custid,  'daterange':datetimerange}
                                            )
        else:
            return render(request,
                                                'Staff/filter_map_folium.html', {'my_map':"","tracker":tracker, "sensor":sensor, "custBTID":custBTID, "custid":custid,  'daterange':datetimerange}
                                            )
    


#-------------------------------------------------------------------------------------------------------------------------------------------------
def gsmsqgraph(request, gfrid):
    if 'staff_username' not in request.session:    
        return redirect('/Staff/login_auth/')
    else:     
        datetimerange = request.POST.get('datetimes')
        tz = pytz.timezone("Asia/Calcutta")
        #now = datetime.now(tz)
        print(datetimerange)
        if datetimerange == None:
            
            now = datetime.now(tz) + timedelta(days=1)  
            tt = str(now.timestamp())
            y = tt.split(".")
            todate = y[0]

            last7days = datetime.now(tz) - timedelta(days=7)  
            t = str(last7days.timestamp())
            x = t.split(".")
            fromdate = x[0]
        else:
            dates = datetimerange.split("-")            
            t =  dates[0].strip()
            fdate = datetime.strptime(t, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))
            fromdate = int(fdate.timestamp())
            #fromdate1 = fdate - timedelta(hours=5,minutes=30)
            #fromdate = int(fromdate1.timestamp())
            tdate = dates[1].strip()
            now = datetime.strptime(tdate, '%Y/%m/%d %H:%M:%S.%f').astimezone(pytz.timezone("Asia/Calcutta"))+ timedelta(days=1)  
            todate = int(now.timestamp())


        dateSQ = []
        valSQ = []
        
        #LRT checking
        offTS = []
        offVal = []


        offtm = System_Health.objects.filter(GFRID=gfrid, system_offtime__gte=fromdate, system_offtime__lte=todate)
        if offtm.count()>0:
            for ot in offtm:
                offTS.append(ot.system_offtime)
                offVal.append(None)

        sq = GSMSQL.objects.filter(GFRID=gfrid, TS__gte=fromdate, TS__lte=todate)
        #sqTS = list(sq.values('TS'))
        if sq.count()>0:
            for g in sq:
                dateSQ.append(g.TS)
                valSQ.append(g.SQValue)

            dat = dateSQ + offTS
            vval = valSQ + offVal
        
            zipped_pairs = zip(vval, dat)
            ziped = sorted(list(zipped_pairs), key = lambda x: x[1])

            valueSQS = []
            TIMES = []
            TIM = []
            for i in ziped:
                valueSQS.append(i[0])
                TIM.append(i[1])

            for i in TIM:
                timestampss = datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S %p') 
                print(i,timestampss)
                TIMES.append(timestampss)
            
            print("Final List:",valueSQS,TIMES)

            if  dat : 
                trace1 = {
                    "x": TIMES,
                    "y": valueSQS,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'value',
                    "type": 'scatter',
                    "line_color":'#008000', 
                    "line_width": 3
                    }

                data1 = [trace1]
                #data=go.Data([trace1])
                layout1=go.Layout(title="GSM Signal Quality", xaxis={'title':'timestamp'}, yaxis={'title':'value'})
                figure1=go.Figure(data=data1,layout=layout1, layout_yaxis_range=[1,30])
                
                figure1.update_traces(connectgaps=False)
                if dat:
                    plot_div = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
                    msg = ""
                else:
                    plot_div = ""
                    msg = "No data available in the selected range"
                    
        else:
                    plot_div = ""
                    msg = "No data available in the selected range"
           
        return render(request, 
		                'Staff/gsmsq.html',
                        {
                             'sq':sq,'gfrid':gfrid,'plot_div':plot_div,'msg':msg,'todate':int(todate),'fromdate':int(fromdate)
                        }) 
#--------------------------------------------------------------------------------------------------------------------------------------
def calibrationList(request, tracker):
    gfridtble = GFRID.objects.get(tracker = tracker)
    ver = gfridtble.version
    sensorslist = ver.sensors
    sensorList = sensorslist.strip("]").strip("[").split(",")

    return render(request, 'Staff/generalSettings.html', {"tracker":tracker, 'sensorlist':sensorList}
                                            )
                                            
#-----------------------------------------------------------------------------------------------------------------------------------------
def UpdateTrackerName(request, tracker):
    if 'staff_username' not in request.session:    
        return redirect('/Staff/login_auth/')
    else:     
        trkname = request.POST.get('trackername')
        GFRID.objects.filter(tracker = tracker).update(tracker_name=trkname)
        return redirect ('/Staff/onshow/')            
#-----------------------------------------------------------------------------------------------------------------------------------------                                        
