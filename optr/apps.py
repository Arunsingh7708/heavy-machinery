from django.apps import AppConfig


class OptrConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'optr'
