from django.urls import path,re_path
from . import views

urlpatterns = [
    path('',views.home,name='shome'),
    path('login_auth/',views.login_auth,name='slogin_auth'),
    path('tracker/',views.login,name='slogin'),
    path('logout/',views.logout,name='slogout'),

    # path('viewlive/<tracker>',views.ViewLive,name='viewlive'),
    re_path(r'^viewdetail/(?P<tracker>\w+)',views.ViewLive,name='sviewdetail'),
    # re_path(r'^viewhistory/(?P<tracker>\w+)',views.ViewHist,name='viewhist'),
    re_path(r'^Location/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewGps,name='slocation'),
    re_path(r'^Histo/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewHisto,name='shisto'),
    re_path(r'^Statuson/(?P<tracker>\w+)/(?P<start>\w+)/(?P<end>\w+)',views.ViewOn,name='sstatuson'),
    # re_path(r'^viewjson/(?P<tracker>\w+)',views.ViewJson.as_view(),name='viewjson'),
    # re_path(r'^viewlive/(?P<tracker>\w+)/pastdata',views.ViewHist,name='viewhist')

]