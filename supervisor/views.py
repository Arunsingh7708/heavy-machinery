from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse,HttpResponseRedirect
from tracker.models import gfr,Version,Sensor
from dateutil import tz
from django.db import connection
from django.conf import settings
from datetime import datetime,timezone
from geopy.geocoders import Nominatim
from plotly import graph_objs as go
from plotly import offline

import folium

class create_dict(dict):
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        self[key] = value

# Create your views here.
@login_required(login_url='/SuperVisor/login_auth/')
def home(request):
    return render(request, 'supervisor/index1.html')

def login_auth(request):
    return render(request, 'supervisor/login.html')

def login(request):
    global username
    username = request.POST.get('username')
    password = request.POST.get('password')
    vals = "a"
    pwd = "a"
    # vals = "vmrdemo"
    # pwd = "Vmr@17022023"
    data = []
    mydict = create_dict()
    # vals = "moambmhu"
    # pwd = "Odisha@321"
    request.session.clear_expired()
    if username == vals and password == pwd:
        request.session['username'] = request.POST.get('username')
        c_id ="a"
        c_pass ="a"
        ls = ['JIVSJILDVLCS7597']
    
        for t in range(len(ls)):
            
            cust_tracker = ls[t]
        
            global subs_start
            global subs_end
            
            subs_start = 1669899600
            subs_end = 1677628740

            exst = gfr.objects.filter(tracker=cust_tracker).exists()
            g_id = gfr.objects.get(tracker=cust_tracker).id

            if exst is True:
                ts_now = round(datetime.now().replace(tzinfo=timezone.utc).timestamp())
                if ts_now < subs_end:
                    ver_code = "ldv"
                    sensor_code = "gpsl86"

                    mydict = create_dict()
#                 if "gpsl86" in sensor_code and "lcell711" in sensor_code
                    cursor1 = connection.cursor()
                    cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                    da = cursor1.fetchall()
                    data.append(da[0])
                    
                    mydict.add("Customer",({"name": f"{c_id}","tracker":cust_tracker}))

            # print(cust_tracker)
            # mylist = []
        # print(data)
            # if sensor_code[table]=="gpsl86" and "lcell711":
        m = folium.Map(location=[21.0000,78.0000], width="%100", height="100%",zoom_start=5, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
        
        for row in data:
            trac = gfr.objects.get(id=row[1]).tracker
            if trac == "JIVSJILDVLCS7597":
                cust_trac = 'JIVVMR0001B0001'
            # elif(trac == "JIVSJILDVLCS3703"):
            #     cust_trac = 'JIVNVMR0002B0002'
            
            # loc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%B %d, %Y, %H:%M:%S %p")
            loc_ts = 'Feb 17, 2023, 10:32:35'
            iframe = folium.IFrame("Lat : "+str(cust_trac) +"<br>TS :"+str(loc_ts))
            popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
            # points = [(20.258583, 85.841682)]
            # icon_url = "http://164.52.200.223:8004/static/SuperAdmin/dist/img/marker-icon-2x.png"  
            # iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
            # iconurl = str(iicon) +"trackers/dist/img/marker-icon-2x.png"               		
            # icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
            # print(row[3].strip()[:-1],row[4].strip()[:-1])
            folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),popup=popup).add_to(m)
            # folium.Marker([20.258583, 85.841682], popup=popup).add_to(m)
            # folium.PolyLine(points, color='green', dash_array='10').add_to(m)
            
        ms=m._repr_html_()
                
        mydict.add("folium",({"map":ms}))
        # print(mydict)
        return render(request, 'supervisor/index1.html',context=mydict)
    else :
        messages = "Invalid username/password"
        return render(request, 'supervisor/login.html',{'messages':messages})
    
def logout(request):
    if 'username' in request.session: del request.session['username']
    return HttpResponseRedirect('/SuperVisor/')

def ViewLive(request,tracker):
    if 'username' not in request.session:    
        return redirect('/SuperVisor/login_auth/?next=/SuperVisor/')
    else:
        c_id ="a"
        c_pass ="a"
        cust = tracker
        if cust == "JIVVMR0001B0001":
            cust_tracker = 'JIVSJILDVLCS3086'
            vehicle = 'vehicle-1'
        # elif(cust == "JIVVMR0002B0002"):
        #     cust_tracker = 'JIVSJILDVLCS3703'
        #     vehicle = 'vehicle-2'

        return render(request,'supervisor/viewdetail.html')
        # print(stud_json)
    
def ViewGps(request,tracker,start,end):
    if 'username' not in request.session:    
        return redirect('/SuperVisor/login_auth/?next=/SuperVisor/')
    else:
        mydict = create_dict()
        mn = 0
    
        if tracker == "JIVVMR0001B0001" and start == '1676077200' and end == '1676159999':
            data = [[12.984475,80.223135],[12.984463,80.223098],[12.984685,80.223345],[12.984790,80.223450],[12.985000,80.223200],[12.986000,80.223300],[12.987000,80.223400],[12.988000,80.223500],[12.984463,80.223098]]
                # [12.984475,80.223035],[12.984475,80.223035],[12.984475,80.223035],[12.984475,80.223035]]
            m = folium.Map(location=[12.984475,80.223035], width="%100", height="%100",zoom_start=18, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
            for row in data:
                # folium.Marker(([row[0],row[1]])).add_to(m)
                
                iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
                iconurl = str(iicon) +"supervisor/dist/img/marker-icon-2x.png"               		
                icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
                if mn == 1 or len(data)-1==mn: 
                    folium.Marker([row[0],row[1]], icon = folium.Icon(color='blue', icon="info-sign")).add_to(m)
                    mn = mn+1
                else:
                    folium.Marker([row[0],row[1]], icon = icon).add_to(m)
                    mn = mn+1
                folium.PolyLine(data, color='green', dash_array='10').add_to(m)
            ms=m._repr_html_()
                      
            mydict.add("folium",({"map":ms}))
            return render(request, 'tracker/viewGps.html',context=mydict)
        elif tracker == "JIVVMR0001B0001" and start == '1676163600' and end == '1676246399':
            data = []
                # [12.984475,80.223035],[12.984475,80.223035],[12.984475,80.223035],[12.984475,80.223035]]
            m = folium.Map(location=[12.984475,80.223035], width="%100", height="%100",zoom_start=18, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
            for row in data:
                # folium.Marker(([row[0],row[1]])).add_to(m)
                
                iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
                iconurl = str(iicon) +"tracker/dist/img/marker-icon-2x.png"               		
                icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
                if mn == 1 or len(data)-1==mn: 
                    folium.Marker([row[0],row[1]], icon = folium.Icon(color='blue', icon="info-sign")).add_to(m)
                    mn = mn+1
                else:
                    folium.Marker([row[0],row[1]], icon = icon).add_to(m)
                    mn = mn+1
                folium.PolyLine(data, color='green', dash_array='10').add_to(m)
            ms=m._repr_html_()
                      
            mydict.add("folium",({"map":ms}))
            return render(request, 'supervisor/viewGps.html',context=mydict)

def ViewHisto(request,tracker,start,end):
    if 'username' not in request.session:    
        return redirect('/SuperVisor/login_auth/?next=/SuperVisor/')
    else:
        mydict = create_dict()
    
        if tracker == "JIVVMR0001B0001" and start == '1676077200' and end == '1676159999':
            trace = go.Bar(x=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],y=[0,0,0,0,0,0,0,0,0,40,20,21,14,5,20,34,0,0,10,0,0,0,0,0])
            data = [trace]
            layout = dict(title='Hour Chart',
            yaxis_title='Minutes')
        # # plot
            fig = dict(data=data,layout=layout)
            fig1 = go.Figure(fig)
            # fig1.update_xaxes(tickangle=-90)
            fig1.update_xaxes(dtick=1,title='Hour')
            return render(request, 'supervisor/viewHisto.html',{'hist': offline.plot((fig1),output_type='div')})
        elif(tracker == "JIVVMR0001B0001" and start == '1676163600' and end == '1676246399'):
            trace = go.Bar(x=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],y=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
            data = [trace]
            layout = dict(title='Hour Chart',
            yaxis_title='Distance(meter)')
        # # plot
            fig = dict(data=data,layout=layout)
            fig1 = go.Figure(fig)
            fig1.update_xaxes(tickangle=90)
            fig1.update_xaxes(dtick=1,title='Hour')
            return render(request, 'supervisor/viewHisto.html',{'hist': offline.plot((fig1),output_type='div')})
    
def ViewOn(request,tracker,start,end):
    if 'username' not in request.session:    
        return redirect('/SuperVisor/login_auth/?next=/SuperVisor/')
    else:
        mydict = create_dict()
    
        if tracker == "JIVVMR0001B0001" and start == '1676077200' and end == '1676159999':
            trace = go.Scatter(x=['10:00','10.30','11:00','11:45','1:00','1:56','4:10','5:30'],y=[1,0,1,0,1,0,1,0],line={"shape": 'hv'})
            data = [trace]
            layout = dict(title='On and Off',
            yaxis_title='status')
        # # plot
            fig = dict(data=data,layout=layout)
            
            fig1 = go.Figure(fig)
            fig1.update_layout(
                    yaxis = dict(
                        tickmode = 'linear',
                        tick0 = 1,
                        dtick = 1,
                    )
                )  
            fig1.update_xaxes(tickangle=90)
            
            fig1.update_xaxes(dtick=1,title='Time')
            return render(request, 'supervisor/viewOn.html',{'on': offline.plot((fig1),output_type='div')})
    
        elif(tracker == "JIVVMR0001B0001" and start == '1676163600' and end == '1676246399'):
            trace = go.Scatter(x=[],y=[],line={"shape": 'hv'})
            data = [trace]
            layout = dict(title='On and Off',
            yaxis_title='status')
        # # plot
            fig = dict(data=data,layout=layout)
            
            fig1 = go.Figure(fig)
            fig1.update_layout(
                    yaxis = dict(
                        tick0 = 1,
                        dtick = 1,
                    )
                )  
            # fig1.update_xaxes(tickangle=-90)
            
            fig1.update_xaxes(dtick=1,title='Time')
            return render(request, 'supervisor/viewOn.html',{'on': offline.plot((fig1),output_type='div')})
    
